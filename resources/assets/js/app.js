
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./DataTablesBS4.js');
require('./jquery.rut.js');
window.Vue = require('vue'); 
import store from '../../../Modules/UniTv/Resources/assets/js/store/store.js';

import { VTooltip, VPopover, VClosePopover } from 'v-tooltip';
import CxltToastr from 'cxlt-vue2-toastr'; //TOASTR
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.directive('tooltip', VTooltip);
Vue.directive('close-popover', VClosePopover);
Vue.component('v-popover', VPopover);

//Componentes generales
Vue.component('general-sidebar', require('./components/AllSidebar.vue'));
Vue.component('list-direct', require('./components/ListContacts.vue'));

Vue.component('loginunico', require('./components/Login.vue'));
Vue.component('portal-clientes', require('./components/Login2.vue'));
Vue.component('top-menu', require('../../../Modules/UniTv/Resources/assets/js/components/top-header.vue'));
//UnitV

Vue.component('left-sidebar', require('../../../Modules/UniTv/Resources/assets/js/components/Sidebar.vue'));
Vue.component('left-sidebar2', require('../../../Modules/HelpDesk/Resources/assets/js/components/Sidebar.vue')); //menu de usuarios normales
Vue.component('unico-footer', require('../../../Modules/UniTv/Resources/assets/js/components/Footer.vue'));
//helpdesk
Vue.component('left-sidebar-helpdesk', require('../../../Modules/HelpDesk/Resources/assets/js/components/Sidebar.vue'));

////COMPONENTES DE UNITV
Vue.component('list-supermarket', require('../../../Modules/UniTv/Resources/assets/js/views/maintainers/supermarket/List.vue'));
Vue.component('list-employees', require('../../../Modules/UniTv/Resources/assets/js/views/maintainers/employees/List.vue'));
Vue.component('list-home', require('../../../Modules/UniTv/Resources/assets/js/views/maintainers/home/List.vue'));
Vue.component('list-product', require('../../../Modules/UniTv/Resources/assets/js/views/maintainers/product/List.vue'));
Vue.component('list-slider', require('../../../Modules/UniTv/Resources/assets/js/views/maintainers/slider/List.vue'));
Vue.component('list-video', require('../../../Modules/UniTv/Resources/assets/js/views/maintainers/video/List.vue'));
Vue.component('dashbrd', require('../../../Modules/UniTv/Resources/assets/js/views/Dashboard.vue'));
//COMPONENTES DE HELPDESK
Vue.component('boards', require('../../../Modules/HelpDesk/Resources/assets/js/views/maintainers/board/boards.vue'));
Vue.component('list-board', require('../../../Modules/HelpDesk/Resources/assets/js/views/maintainers/board/List.vue'));
Vue.component('list-type-problem', require('../../../Modules/HelpDesk/Resources/assets/js/views/maintainers/type_problem/List.vue'));
Vue.component('list-problem', require('../../../Modules/HelpDesk/Resources/assets/js/views/maintainers/problem/List.vue'));

Vue.component('menu-home', require('../../../Modules/UniTv/Resources/assets/js/views/pages/Menu_home.vue'));
Vue.component('board-user', require('../../../Modules/HelpDesk/Resources/assets/js/views/maintainers/board/boardUser.vue'));

Vue.component('list-survey', require('../../../Modules/Surveys/Resources/assets/js/components/survey/List.vue'));
Vue.component('list-survey-companies', require('../../../Modules/Surveys/Resources/assets/js/components/companies/List.vue'));
Vue.component('list-survey-branch', require('../../../Modules/Surveys/Resources/assets/js/components/branch/List.vue'));
Vue.component('list-survey-pollster', require('../../../Modules/Surveys/Resources/assets/js/components/pollster/List.vue'));
Vue.component('reports-product', require('../../../Modules/Surveys/Resources/assets/js/components/report/List.vue'));

//components administration
Vue.component('list-permission', require('../../../Modules/Administration/Resources/assets/js/components/permission/List.vue'));
Vue.component('list-users-create', require('../../../Modules/Administration/Resources/assets/js/components/users/List.vue'));

//components simpleCredit
Vue.component('list-contracttype', require('../../../Modules/SimpleCredit/Resources/assets/js/components/contract_type/List.vue'));
Vue.component('list-articles', require('../../../Modules/SimpleCredit/Resources/assets/js/components/articles/List.vue'));
Vue.component('list-contract', require('../../../Modules/SimpleCredit/Resources/assets/js/components/contract/List.vue'));
Vue.component('list-clients', require('../../../Modules/SimpleCredit/Resources/assets/js/components/clients/List.vue'));
Vue.component('list-responsables', require('../../../Modules/SimpleCredit/Resources/assets/js/components/responsibles/List.vue'));
Vue.component('list-employees-credit', require('../../../Modules/SimpleCredit/Resources/assets/js/components/employees/List.vue'));
Vue.component('list-banks', require('../../../Modules/SimpleCredit/Resources/assets/js/components/bank/List.vue'));
Vue.component('list-account-type', require('../../../Modules/SimpleCredit/Resources/assets/js/components/account_type/List.vue'));
//components Flow
Vue.component('form-magic', require('../../../Modules/Flow/Resources/assets/js/components/cases/formMagic.vue'));
Vue.component('list-cases', require('../../../Modules/Flow/Resources/assets/js/components/cases/List.vue'));
Vue.component('list-type-flow', require('../../../Modules/Flow/Resources/assets/js/components/type_flow/List.vue'));

//Vue.component('list-clients', require('../../../Modules/SimpleCredit/Resources/assets/js/components/clients/List.vue'));

Vue.component('test', require('../../../Modules/UniTv/Resources/assets/js/views/components/Forms.vue'));

let toastrConfigs = {
   position: 'bottom center',
   showDuration: 400,
   hideDuration: 400,
   delay: 0,
   timeOut: 4000,
   progressBar: true,
   hideMethod: 'slideOutDown',
   showMethod: 'slideInUp'
}



Vue.use(CxltToastr, toastrConfigs, VTooltip)

const app = new Vue({
    el: '#app',
    store,
});
