<?php
use Illuminate\Support\Str;
use App\Notifications\SendEmailForPassword;
use App\AccessControl;
//use Hash;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('login');

Route::post('/testing', function () {
    return 'hola';
});
Route::get('/portal',function(){
return view('auth.Portal');
});

/*Route::get('/cosa/{id}', 'AccessControlController@sortedArray');*/

Route::get('/pass', function(){


	//Password

	//$a = Hash::make('123456');
	$a = 'XAkZkvVhd8wDu86hFC+L8kf6FyakuEThxmOqhVEiE=';
	//dd(Hash::make('123456'));
	$base = hash_hmac('sha256', Str::random(40), $a);
	$c = Hash::make($base);
	//dd($c, $base);
	//$base = '167ef804543f5d56d500aea37c784f5055fb42edf122251eb306de21568cf2a0';
	//$c = '$2y$10$8J9cQBXQZSFTbSByjbnVmeKz2.WlKZIlMYpxl1HhBRUEpTk0p6nCC';
	//dd($c);
	//$value = Hash::check($base, $c);
	//dd($value);
	//dd($b);

	// Email
	
	Notification::send(AccessControl::find(1), new SendEmailForPassword($base));

});

/*Auth::routes();*/

Route::get('/dashboard', 'HomeController@index')->name('home');
