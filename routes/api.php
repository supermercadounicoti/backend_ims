<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware(['auth:api', 'cors'])->post('/update-purchase-order', '\Modules\Reception\Http\Controllers\PurchaseOrderHeaderController@update');
Route::middleware(['auth:api', 'cors'])->get('/local-purchase-order', '\Modules\Reception\Http\Controllers\LocalPurchaseOrderController@getLocals');
Route::middleware(['auth:api', 'cors'])->get('/list-purchase-order/{local}/{purchaseOrder}', '\Modules\Reception\Http\Controllers\PurchaseOrderHeaderController@listDetails');
Route::middleware(['auth:api', 'cors'])->get('/find-purchase-order/{local}/{purchaseOrder}', '\Modules\Reception\Http\Controllers\PurchaseOrderHeaderController@findByPurchaseOrder');

/*Route::post('/update-purchase-order', 'Modules\Http\Controller\PurchaseOrderHeaderController@update');*/