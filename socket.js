var fs = require('fs');

var https = require('https');

var server = https.createServer({
    key:  fs.readFileSync('/etc/ssl/private/nginx-selfsigned.key'),
  	cert: fs.readFileSync('/etc/ssl/certs/nginx-selfsigned.crt')
});


var io = require('socket.io')(server);

var Redis = require('ioredis');

var redis = new Redis();


redis.subscribe('sendPackage', function(err, count) {
});
redis.on('message', function(channel, message) {
	console.log('Message Received: ' + message);
	message = JSON.parse(message);
	console.log(message.event);
	io.emit(channel + ':' + message.event, message.data);
});
server.listen(3000, function(){
	console.log('Listening on Port 3000');
});
io.on('connection', function(client){

    client.on('client_event1', function(name){
			console.log(name);
        io.emit('server_emit1', name);
    });

    client.on('client_event2', function(name){
        io.emit('server_emit2', name);
    });

});


// var server = require('http').Server();

// var io = require('socket.io')(server);

// var Redis = require('ioredis');

// var redis = new Redis();

// redis.subscribe('sendPackage', function(err, count) {
// });
// redis.on('message', function(channel, message) {
// 	console.log('Message Received: ' + message);
// 	message = JSON.parse(message);
// 	console.log(message.event);
// 	io.emit(channel + ':' + message.event, message.data);
// });
// server.listen(3000, function(){
// 	console.log('Listening on Port 3000');
// });
// io.on('connection', function(client){

//     client.on('client_event1', function(name){
// 			console.log(name);
//         io.emit('server_emit1', name);
//     });

//     client.on('client_event2', function(name){
//         io.emit('server_emit2', name);
//     });

// });