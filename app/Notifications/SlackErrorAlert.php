<?php

namespace App\Notifications;

Use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class SlackErrorAlert extends Notification
{
    use Queueable;

    protected $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //return ['mail'];
        return ['slack'];
    }

    public function routeNotificationForSlack()
    {
        return 'https://hooks.slack.com/services/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';
    }

    /**
     * Send message to slack channel
     * @param  $notifiable
     * @return Message to SLACK
     */
    public function toSlack($notifiable)
    {
        return (new SlackMessage)
            ->from('' . env('APP_NAME') . '')
            ->content('Information 
                IP: '. \Request::ip() .'
                Message: ' . $this->message . '')
            ->attachment(function ($attachment) {
                $attachment->title(''/*, url('http://google.cl')*/)
                    ->color('#36a64f')
                    ->footer('Equipo TI - Supermercado Unico')
                    ->footerIcon('http://www.urrutialogistica.cl/Sitio/images/img/unico.jpg');
            });
    }
}
