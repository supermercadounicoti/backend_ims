<?php
namespace App\Traits;

use Redis;
use Module;
use Storage;
use AdminNotify;
use App\Notifications\SlackErrorAlert;

trait UtilsFromTraits
{
	/**
	 * Show the application's login form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public static function responseMessage($status, $message = null)
	{
		if ($status < 1) {
			self::sendMessageSlack($message);
		}

		return response()->json([
			'status'=> $status,
			'message' => $message
		]);

	}

	/**
	 * [sendMessageSlack description]
	 * @param  [type] $message [description]
	 * @return [type]          [description]
	 */
	public static function sendMessageSlack($message)
	{
		$notify = AdminNotify::send(new SlackErrorAlert($message));

		return $notify;
	}

	/**
	 * Send package to differents TV
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public static function sendPackageNode($request, $disabled = 0)
	{
		$path = $request->path;
		$type_id = $request->type_id;
		$informative_id = $request->id;
		foreach ($request->supermarkets as $supermarketId) {
			$data = self::formatOfPackage($supermarketId->id, $informative_id, $path, $type_id, $disabled);
			Redis::publish('sendPackage', json_encode($data));
		}
	}	

	public static function formatOfPackage($supermarketId, $informative_id, $path, $type_id, $disabled)
	{
		$data = [
    		'event' => 'SendPackageEvent',
    		'data' => [
    			'supermarket_id' => $supermarketId,
    			'informative_id' => $informative_id,
    			'path' => $path,
    			'type_id' => $type_id,
    			'disabled' => $disabled
    		]
    	];

    	return $data;
	}

	public static function upload($module, $request)
	{
        try {
            $img = $request->file('file'); 
            $request->description = $img->getClientOriginalName();
            $request->{'path'} = Storage::disk('public')->put($module, $img);
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }

        return $request;
	}

}