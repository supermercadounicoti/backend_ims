<?php

namespace Modules\HelpDesk\Entities;

use DB;
use Storage;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class AttachedFile extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'case_id',
        'description',
        'path'
    ];
    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'attached_files';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Create
     * @param  [type] $request 
     * @return [type] JSON and Slack message
     */
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $query = new AttachedFile;
                $query->description = $request->attachedDescription;
                $query->path = $request->attachedPath;
                $query->case_id = $request->case_id;

                $query->push();

                DB::commit();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Update
     * @param  [Object] $model
     * @param  [Request] $request
     * @return [Json] Message
     */
    public static function actualize($model, $request)
    {
        try {
            $result = DB::transaction(function () use ($model, $request) {
                $query = AttachedFile::find($model->id);
                $query->description = $request->description;
                $query->path = $request->path;
                $query->case_id = $request->case_id;

                $query->push();

                DB::commit();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1, $result);
    }

    /**
     * Destroy
     * @param  [Object] $model
     * @return [Json] Message
     */
    public static function remove($model)
    {
        try {
            $result = DB::transaction(function () use ($model) {
                $query = AttachedFile::destroy($model->id);

                DB::commit();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1, $result);
    }

    /**
     * Change status
     * @param  [int] $id
     * @return [Json] Message
     */
    public static function change($id)
    {
        try {
            $result = DB::transaction(function () use ($id) {
                $query = AttachedFile::find($id);
                $query->status = ($query->status == 1)? 0:1;

                $query->push();

                DB::commit();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1, $result);
    }
}
