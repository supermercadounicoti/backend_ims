<?php

namespace Modules\HelpDesk\Entities;

use DB;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class Responsible extends Model
{
  use UtilsFromTraits;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'description',
    'status'
  ];
  
  protected $dateFormat = 'Y-m-d H:i:00';

  public function boards()
	{
			return $this->belongsToMany(
							'Modules\HelpDesk\Models\Board',
							'board_has_responsibles',
							'board_id',
              'responsible_id'

        );
	}
  public function user()
  {
      return $this->belongsTo('App\User');
  }
  /**
   * Database connection
   * @var string
   */
  protected $connection = 'sgi';

  /**
   * Table name
   * @var string
   */
  protected $table = 'responsibles';

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  /**
	 * List All Boards
	 * @return [Json] Message
	 */
	public static function listallresponsibles($idBoard)
	{
		try {
			$result = DB::transaction(function () use ($idBoard) {

			$result = Responsible::leftjoin('access_controls', 'access_controls.id', '=', 'responsibles.user_id')
          ->leftjoin('users_data', 'users_data.id', '=', 'access_controls.user_data_id')
          ->select("responsibles.*", DB::raw("users_data.ud_first_name + ' ' +users_data.ud_father_surname AS name"))
      ->where('responsibles.status',1)

					->get();
			return $result;

		});
		return $result;
	} catch (Exception $e) {
		DB::rollback();
		return self::responseMessage(0, $e->getMessage());
	}
  }
}
