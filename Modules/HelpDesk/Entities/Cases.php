<?php

namespace Modules\HelpDesk\Entities;

use DB;
use Storage;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;
use Modules\HelpDesk\Entities\Comment;
use Modules\HelpDesk\Entities\CaseHasFlow;
use Modules\HelpDesk\Entities\AttachedFile;

class Cases extends Model
{
  use UtilsFromTraits;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
	'name',
	'init_date',
	'end_date',
	'estimated_date',
	'supermarket_id',
	'responsible_id',
	'problem_id',
	'user_id',
	'status'

  ];

  /**
   * Database connection
   * @var string
   */
  protected $connection = 'sgi';

  protected $dateFormat = 'Y-m-d H:i:00';
  /**
   * Table name
   * @var string
   */
  protected $table = 'cases';

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  public function flow()
	{
			return $this->belongsToMany(
							'Modules\HelpDesk\Entities\Flow',
							'case_has_flows',
							'case_id',
			  'flow_id'

		);
	}

  public function supermarket(){
		return $this->belongsTo('Modules\UniTv\Entities\Supermarket');
	}
  public function problem(){
		return $this->belongsTo('Modules\HelpDesk\Entities\Problem');
	}
  public function comment(){
		return $this->hasMany('Modules\HelpDesk\Entities\Comment', 'case_id', 'id');
	}
	public function attached(){
		return $this->hasMany('Modules\HelpDesk\Entities\AttachedFile', 'case_id', 'id');
	}




	public static function createCases($request)
	{ 
	  	try {
		  $result = DB::transaction(function () use ($request) {
		  $cases = new Cases;
		  $cases->name = $request->name;
		  $cases->init_date = $request->init_date;
		  $cases->supermarket_id = $request->supermarket_id;
		  $cases->problem_id = $request->problem_id;
		  $cases->user_id = $request->user_id;
		  $cases->status = true;

		  $cases->push();

		  DB::commit();
		  CaseHasFlow::createCasesHasFlow2($cases->id);
		  $request->{"case_id"} = $cases->id;
		  $request->{"description"} = ($request->username .' ha ingresado un nuevo ticket a Por hacer');
		  Comment::store($request);
			if (!is_null($request->file('file'))) {
				foreach ($request->file('file') as $file) {
					$request->{'attachedDescription'} = $file->getClientOriginalName();
					$request->{'attachedPath'} = Storage::disk('public')->put('HelpDesk', $file);
					AttachedFile::store($request);
	   			}
			}

		});
		return self::responseMessage(1, $result);
	  } catch (Exception $e) {
		DB::rollback();
		return self::responseMessage(0, $e->getMessage());
	  }
	}

	public static function detailsTicket($id){
	  try {

			$result = DB::transaction(function () use ($id) {
		  // $result = Cases::with('comment')->get();
				$result = Cases::with([

			'problem' => function ($query) {
										$query
										->leftjoin('priority', 'priority.id', '=', 'problems.priority_id')
										->leftjoin('type_problems', 'type_problems.id', '=', 'problems.type_id')
										->select('problems.*', 'priority.name as prioridad' ,'type_problems.name as tipo_problema');
									},
			'flow' => function ($query) {
								$query ->where('case_has_flows.status', '=', '1');
							},

					'supermarket',
					'comment',
					'attached'

				  ])
		  ->leftjoin('access_controls', 'access_controls.id', '=', 'cases.user_id')

		  ->leftjoin('users_data', 'users_data.id', '=', 'access_controls.user_data_id')
		  
		  ->leftjoin('responsibles', 'cases.responsible_id', '=', 'responsibles.id')
		  
		  ->leftjoin('access_controls as user_responsible_data', 'user_responsible_data.id', '=', 'responsibles.user_id')
		  ->leftjoin('users_data as user_data_responsible', 'user_data_responsible.id', '=', 'user_responsible_data.user_data_id')
		  ->select('cases.*',
		  		DB::raw("users_data.ud_first_name + ' ' + users_data.ud_father_surname AS nombre_user"),
		  		'access_controls.email as Email',
		  		DB::raw("user_data_responsible.ud_first_name+ ' ' +user_data_responsible.ud_father_surname AS user_responsible"))
		  ->where('cases.id','=',$id)
		  ->get()[0];


			return $result;

		});
				return $result;
			} catch (Exception $e) {
				DB::rollback();
				return self::responseMessage(0, $e->getMessage());
			}
	}

	public static function actualizeDateEstimated($model, $request)
	{

		try {
			$result = DB::transaction(function () use ($model, $request) {
				$query = Cases::find($model);
				$query->estimated_date = $request->estimated_date;

				$query->push();

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1, $result);
	}

	public static function actualizeResponsible($model, $request)
	{

	  try {
		$result = DB::transaction(function () use ($model, $request) {
		  $query = Cases::find($model);
		  $query->responsible_id = $request->responsable_id;

		  $query->push();

		  DB::commit();

		  return $query;
		});
	  } catch (Exception $e) {
		DB::rollback();
		return self::responseMessage(0, $e->getMessage());
	  }
	  return self::responseMessage(1, $result);
	}

}
