<?php

namespace Modules\HelpDesk\Entities;

use DB;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	use UtilsFromTraits;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'case_id',
		'description',
	];
  protected $dateFormat = 'Y-m-d H:i:00';
	/**
	 * Database connection
	 * @var string
	 */
	protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'comments';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;

	/**
	 * Create
	 * @param  [type] $request 
	 * @return [type] JSON and Slack message
	 */
	public static function store($request)
	{
		try {
			$result = DB::transaction(function () use ($request) {
				$query = new Comment;
				$query->case_id = $request->case_id;
				$query->description = $request->description;

				$query->push();

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Update
	 * @param  [Object] $model
	 * @param  [Request] $request
	 * @return [Json] Message
	 */
	public static function actualize($model, $request)
	{
		try {
			$result = DB::transaction(function () use ($model, $request) {
				$query = Comment::find($model->id);
				$query->case_id = $request->case_id;
				$query->description = $request->description;

				$query->push();

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1, $result);
	}

    /**
     * Destroy
     * @param  [Object] $model
     * @return [Json] Message
     */
    public static function remove($model)
    {
    	try {
			$result = DB::transaction(function () use ($model) {
				$query = Comment::destroy($model->id);

                DB::commit();

                return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
        return self::responseMessage(1, $result);
    }

    /**
     * Change status
     * @param  [int] $id
     * @return [Json] Message
     */
    public static function change($id)
    {
    	try {
			$result = DB::transaction(function () use ($id) {
				$query = Comment::find($id);
				$query->status = ($query->status == 1)? 0:1;

				$query->push();

                DB::commit();

                return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
        return self::responseMessage(1, $result);
    }
}
