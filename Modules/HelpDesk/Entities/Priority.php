<?php

namespace Modules\HelpDesk\Entities;

use DB;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
  use UtilsFromTraits;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'description',
		'status'
	];
  protected $dateFormat = 'Y-m-d H:i:00';
	/**
	 * Database connection
	 * @var string
	 */
	protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'priority';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;


	
	/**
	 * List All Boards
	 * @return [Json] Message
	 */
	public static function listPriorityActive()
	{
		try {
			$result = DB::transaction(function () {
			$result = Priority::where('status', true)
					->get();
			return $result;

		});
			return $result;
	} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
	}
}
