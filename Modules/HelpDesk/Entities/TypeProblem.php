<?php

namespace Modules\HelpDesk\Entities;

use DB;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class TypeProblem extends Model
{
	use UtilsFromTraits;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'description',
		'status'
	];
  protected $dateFormat = 'Y-m-d H:i:00';
	/**
	 * Database connection
	 * @var string
	 */
	protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'type_problems';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;

	/**
	 * Create
	 * @param  [type] $request
	 * @return [type] JSON and Slack message
	 */
	public static function store($request)
	{
		try {
			$result = DB::transaction(function () use ($request) {
				$query = new TypeProblem;
				$query->name = $request->name;
				$query->description = $request->description;

				$query->push();

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Update
	 * @param  [Object] $model
	 * @param  [Request] $request
	 * @return [Json] Message
	 */
	public static function actualize($model, $request)
	{
		try {
			$result = DB::transaction(function () use ($model, $request) {
				$query = TypeProblem::find($model->id);
				$query->name = $request->name;
				$query->description = $request->description;

				$query->push();

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1, $result);
	}

    /**
     * Destroy
     * @param  [Object] $model
     * @return [Json] Message
     */
    public static function remove($model)
    {
    	try {
			$result = DB::transaction(function () use ($model) {
				$query = TypeProblem::destroy($model->id);

                DB::commit();

                return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
        return self::responseMessage(1, $result);
    }

    /**
     * Change status
     * @param  [int] $id
     * @return [Json] Message
     */
    public static function change($id)
    {
    	try {
			$result = DB::transaction(function () use ($id) {
				$query = TypeProblem::find($id);
				$query->status = ($query->status == 1)? 0:1;

				$query->push();

                DB::commit();

                return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
        return self::responseMessage(1, $result);
    }

	/**
	 * List All Boards
	 * @return [Json] Message
	 */
	public static function listAllTypeActive()
	{
		try {
			$result = DB::transaction(function () {
			$result = TypeProblem::where('status', true)
					->get();
			return $result;

		});
			return $result;
	} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
	}

	/**
	 * List All Boards
	 * @return [Json] Message
	 */
	public static function listAllType()
	{
		try {
			$result = DB::transaction(function () {
			$result = TypeProblem::all();
			return $result;

		});
			return $result;
	} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
	}

	public static function destroytype($typeProblem)
 	{
 		try {
 		$result = DB::transaction(function () use ($typeProblem) {
 			$typeProblem = TypeProblem::find($typeProblem->id);
 							$typeProblem->status = ($typeProblem->status == 1)? 0:1;
 							$typeProblem->push();

 							DB::commit();
 		});
 		return self::responseMessage(1, $result);
 	} catch (Exception $e) {
 		DB::rollback();
 		return self::responseMessage(0, $e->getMessage());
 	}
 	}

 	/**
	 * List Only Offer by id
	 * @param  [Object] $offer
	 * @return [Json] Message
	 */
	public static function listType($typeProblem)
	{
		try {
			$result = DB::transaction(function () use ($typeProblem) {
			$result = TypeProblem::where('id',$typeProblem->id)
				->get()[0];

			return $result;

		});
		return $result;
	} catch (Exception $e) {
		DB::rollback();
		return self::responseMessage(0, $e->getMessage());
	}
	}
}
