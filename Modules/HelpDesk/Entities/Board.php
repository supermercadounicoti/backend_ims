<?php

namespace Modules\HelpDesk\Entities;

use DB;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
	use UtilsFromTraits;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'description',
		'status'
	];

	protected $dateFormat = 'Y-m-d H:i:00';
	/**
	 * Database connection
	 * @var string
	 */
	protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'boards';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;

	/**
	 * Create
	 * @param  [type] $request
	 * @return [type] JSON and Slack message
	 */
	public static function store($request)
	{
		try {
			$result = DB::transaction(function () use ($request) {
				$query = new Board;
				$query->name = $request->name;
				$query->description = $request->description;
				$query->status = 1;
				$query->push();

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Update
	 * @param  [Object] $model
	 * @param  [Request] $request
	 * @return [Json] Message
	 */
	public static function actualize($model, $request)
	{
		try {
			$result = DB::transaction(function () use ($model, $request) {
				$query = Board::find($model->id);
				$query->name = $request->name;
				$query->description = $request->description;

				$query->push();

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1, $result);
	}

	/**
	 * Destroy
	 * @param  [Object] $model
	 * @return [Json] Message
	 */
	 public static function destroyboard($board)
 	{
 		try {
 		$result = DB::transaction(function () use ($board) {
 			$board = Board::find($board->id);
 							$board->status = ($board->status == 1)? 0:1;
 							$board->push();

 							DB::commit();
 		});
 		return self::responseMessage(1, $result);
 	} catch (Exception $e) {
 		DB::rollback();
 		return self::responseMessage(0, $e->getMessage());
 	}
 	}

	/**
	 * Change status
	 * @param  [int] $id
	 * @return [Json] Message
	 */
	public static function change($id)
	{
		try {
			$result = DB::transaction(function () use ($id) {
				$query = Board::find($id);
				$query->status = ($query->status == 1)? 0:1;

				$query->push();

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1, $result);
	}

	/**
	 * List All Boards
	 * @return [Json] Message
	 */
	public static function listAllBoardsActive()
	{
		try {
			$result = DB::transaction(function () {
			$result = Board::where('status', true)
					->get();
			return $result;

		});
		return $result;
	} catch (Exception $e) {
		DB::rollback();
		return self::responseMessage(0, $e->getMessage());
	}
	}

	/**
	 * List All Boards
	 * @return [Json] Message
	 */
	public static function listAllBoards()
	{
		try {
			$result = DB::transaction(function () {
			$result = Board::get();
			return $result;

		});
		return $result;
	} catch (Exception $e) {
		DB::rollback();
		return self::responseMessage(0, $e->getMessage());
	}
	}

	/**
	 * List Only Offer by id
	 * @param  [Object] $offer
	 * @return [Json] Message
	 */
	public static function listBoards($board)
	{
		try {
			$result = DB::transaction(function () use ($board) {
			$result = Board::where('id',$board->id)
				->get()[0];

			return $result;

		});
		return $result;
	} catch (Exception $e) {
		DB::rollback();
		return self::responseMessage(0, $e->getMessage());
	}
	}




}
