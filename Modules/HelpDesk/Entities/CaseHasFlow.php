<?php

namespace Modules\HelpDesk\Entities;

use DB;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class CaseHasFlow extends Model
{
  use UtilsFromTraits;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'case_id',
    'flow_id',
    'status',
    'init_date',
    'end_date',
    'comments'

  ];
  protected $dateFormat = 'Y-m-d H:i:00';
  /**
   * Database connection
   * @var string
   */
  protected $connection = 'sgi';

  /**
   * Table name
   * @var string
   */
  protected $table = 'case_has_flows';

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  public function flows(){
        return $this->belongsTo('Modules\HelpDesk\Entities\Flow');
    }

  public static function createCasesHasFlow($model, $request)
  {

    try {
        $result = DB::transaction(function () use ($model, $request) {
        $CaseHasFlow = new CaseHasFlow;
        $CaseHasFlow->case_id = $model;
        $CaseHasFlow->flow_id = $request->id_siguiente;
        $CaseHasFlow->status = 1;
        $CaseHasFlow->init_date = Carbon::now();

        $CaseHasFlow->push();

        DB::commit();
      });
      return self::responseMessage(1, $result);
    } catch (Exception $e) {
      DB::rollback();
      return self::responseMessage(0, $e->getMessage());
    }
  }

  public static function actualizeCaseFlow($model, $request)
  {

    try {
      $result = DB::transaction(function () use ($model, $request) {
        $query = CaseHasFlow::where('case_id',$model)->where('status','1')->first();
        // $query->estimated_date = $request->estimated_date;
        $query->comments = $request->comment;
        $query->end_date = Carbon::now();
        $query->status = 0;

        $query->push();
        DB::commit();

        CaseHasFlow::createCasesHasFlow($model, $request);
        return $query;
      });
    } catch (Exception $e) {
      DB::rollback();
      return self::responseMessage(0, $e->getMessage());
    }
    return self::responseMessage(1, $result);
  }

  public static function createCasesHasFlow2($request)
  {

    try {
        $result = DB::transaction(function () use ($request) {
        $CaseHasFlow = new CaseHasFlow;
        $CaseHasFlow->case_id = $request;
        $CaseHasFlow->flow_id = 1;
        $CaseHasFlow->status = 1;
        $CaseHasFlow->init_date = Carbon::now();

        $CaseHasFlow->push();

        DB::commit();
      });
      return self::responseMessage(1, $result);
    } catch (Exception $e) {
      DB::rollback();
      return self::responseMessage(0, $e->getMessage());
    }
  }

}
