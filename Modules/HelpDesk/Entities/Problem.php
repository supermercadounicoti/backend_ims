<?php

namespace Modules\HelpDesk\Entities;

use DB;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class Problem extends Model
{
	use UtilsFromTraits;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'description',
		'type_id',
		'status'
	];
  protected $dateFormat = 'Y-m-d H:i:00';
	/**
	 * Database connection
	 * @var string
	 */
	protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'problems';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;

	public function type(){
        return $this->belongsTo('Modules\HelpDesk\Entities\TypeProblem');
    }

    public function priority(){
        return $this->belongsTo('Modules\HelpDesk\Entities\Priority');
    }

	/**
	 * Create
	 * @param  [type] $request
	 * @return [type] JSON and Slack message
	 */
	public static function store($request)
	{
		try {
			$result = DB::transaction(function () use ($request) {
				$query = new Problem;
				$query->name = $request->name;
				$query->description = $request->description;
				$query->type_id = $request->type_id;
				$query->priority_id = $request->priority_id;
				$query->status = 1;
				$query->push();

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Update
	 * @param  [Object] $model
	 * @param  [Request] $request
	 * @return [Json] Message
	 */
	public static function actualize($model, $request)
	{
		try {
			$result = DB::transaction(function () use ($model, $request) {
				$query = Problem::find($model->id);
				$query->name = $request->name;
				$query->description = $request->description;
				$query->type_id = $request->type_id;
				$query->priority_id = $request->priority_id;

				$query->push();

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1, $result);
	}

    /**
     * Destroy
     * @param  [Object] $model
     * @return [Json] Message
     */
    public static function remove($model)
    {
    	try {
			$result = DB::transaction(function () use ($model) {
				$query = Problem::destroy($model->id);

                DB::commit();

                return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
        return self::responseMessage(1, $result);
    }

    /**
     * Change status
     * @param  [int] $id
     * @return [Json] Message
     */
    public static function change($id)
    {
    	try {
			$result = DB::transaction(function () use ($id) {
				$query = Problem::find($id);
				$query->status = ($query->status == 1)? 0:1;

				$query->push();

                DB::commit();

                return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
        return self::responseMessage(1, $result);
    }

		public static function listproblembytype($id)
	{

			 $result = Problem::where('type_id', $id)
			->where('status',1)
			->get();
			return $result;

	}
	
	/**
	 * List All Boards
	 * @return [Json] Message
	 */
	public static function listAllProblem()
	{
		try {
			$result = DB::transaction(function () {
			$result = Problem::with('type','priority')
			->get();
			return $result;

		});
			return $result;
	} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
	}


	public static function destroyProblem($problem)
 	{
 		try {
 		$result = DB::transaction(function () use ($problem) {
 			$problem = Problem::find($problem->id);
 							$problem->status = ($problem->status == 1)? 0:1;
 							$problem->push();

 							DB::commit();
 		});
 		return self::responseMessage(1, $result);
 	} catch (Exception $e) {
 		DB::rollback();
 		return self::responseMessage(0, $e->getMessage());
 	}
 	}

 	/**
	 * List Only Offer by id
	 * @param  [Object] $offer
	 * @return [Json] Message
	 */
	public static function listProblem($problem)
	{
		try {
			$result = DB::transaction(function () use ($problem) {
			$result = Problem::with('type','priority')
					->where('id',$problem->id)
				->get()[0];

			return $result;

		});
		return $result;
	} catch (Exception $e) {
		DB::rollback();
		return self::responseMessage(0, $e->getMessage());
	}
	}
}
