<?php

namespace Modules\HelpDesk\Entities;

use DB;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class Flow extends Model
{
	use UtilsFromTraits;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'number_step',
		'description_step',
		'board_id',
		'next_step',
		'status'
	];
  protected $dateFormat = 'Y-m-d H:i:00';
	/**
	 * Database connection
	 * @var string
	 */
	protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'flows';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;

	public function board()
	{
			return $this->belongsTo('Modules\HelpDesk\Entities\Board');
	}

	public function cases()
	{
			return $this->belongsToMany(
							'Modules\HelpDesk\Entities\Cases',
							'case_has_flows',
							'flow_id',
              'case_id'

        );
	}

	public function replys()
{
  return $this->hasMany(Flow::class);
}

	/**
	 * Create
	 * @param  [type] $request
	 * @return [type] JSON and Slack message
	 */
	public static function store($request)
	{
		try {
			$result = DB::transaction(function () use ($request) {
				$query = new AttachedFile;
				$query->name = $request->name;
				$query->description = $request->description;

				$query->push();

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Update
	 * @param  [Object] $model
	 * @param  [Request] $request
	 * @return [Json] Message
	 */
	public static function actualize($model, $request)
	{
		try {
			$result = DB::transaction(function () use ($model, $request) {
				$query = AttachedFile::find($model->id);
				$query->name = $request->name;
				$query->description = $request->description;

				$query->push();

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1, $result);
	}

	/**
	 * Destroy
	 * @param  [Object] $model
	 * @return [Json] Message
	 */
	public static function remove($model)
	{
		try {
			$result = DB::transaction(function () use ($model) {
				$query = AttachedFile::destroy($model->id);

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1, $result);
	}

	/**
	 * Change status
	 * @param  [int] $id
	 * @return [Json] Message
	 */
	public static function change($id)
	{
		try {
			$result = DB::transaction(function () use ($id) {
				$query = AttachedFile::find($id);
				$query->status = ($query->status == 1)? 0:1;

				$query->push();

				DB::commit();

				return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1, $result);
	}


	public static function listAllData($id){
		try {
			$result = DB::transaction(function () use ($id) {
				$result = Flow::with([
					// 'Cases.problem',
					'cases' => function ($query) {
									$query
									->leftjoin('problems', 'problems.id', '=', 'cases.problem_id')
									->leftjoin('priority', 'priority.id', '=', 'problems.priority_id')
									->leftjoin('access_controls', 'access_controls.id', '=', 'cases.user_id')
									->leftjoin('users_data', 'users_data.id', '=', 'access_controls.user_data_id')
									->select('cases.*', 'problems.name as problema', 
										DB::raw("users_data.ud_first_name + ' ' +users_data.ud_father_surname AS usuario") ,'priority.name as prioridad')
									->where('case_has_flows.status', '=', '1');
								},

							// 	},
							// // 'problems',
							//
							//
							'board'
							])->leftjoin('flows as child', 'flows.next_step', '=', 'child.id')
							->select('flows.*','child.id as id_siguiente','child.description_step as siguiente')
							->where('flows.status',1)->get();



			return $result;

		});
				return $result;
			} catch (Exception $e) {
				DB::rollback();
				return self::responseMessage(0, $e->getMessage());
			}
	}

	public static function detailsTicket($id){
		try {
			$result = DB::transaction(function () use ($id) {
				$result = Flow::with([
					// 'Cases.problem',
					'cases' => function ($query) {
									$query
									->join('problems', 'problems.id', '=', 'cases.problem_id')
									->join('users', 'users.id', '=', 'cases.user_id')
									->select('cases.*', 'problems.name as problema' ,'users.name as usuario')
									->where('case_has_flows.status', '=', '1');
								},
							// 	},
							// // 'problems',
							//
							//
							'board'
							])->where('flows.status',1)

							->get();



			return $result;

		});
				return $result;
			} catch (Exception $e) {
				DB::rollback();
				return self::responseMessage(0, $e->getMessage());
			}
	}


	public static function flowCheck($id_inicio, $id_fin , $board){
		try {

			$result = DB::transaction(function () use ($id_inicio , $id_fin ,$board) {
				//dd($id_inicio, $id_fin , $board);
				$result = Flow::join('flows as child', 'flows.next_step', '=', 'child.id')
				->select('flows.id','flows.description_step' , 'child.id as id_siguiente','child.description_step as siguiente')
				->where('flows.description_step','=',$id_inicio)
				->where('child.description_step','=',$id_fin)
				->where('flows.board_id','=',$board)
				->get();
			return $result;

		});
				return $result;
			} catch (Exception $e) {
				DB::rollback();
				return self::responseMessage(0, $e->getMessage());
			}
}

public static function listAllDataByUser($id , $user_id){
		try {
			$result = DB::transaction(function () use ($id, $user_id) {
				$result = Flow::with([
					// 'Cases.problem',
					'cases' => function ($query) use ($user_id) {
									$query
									->leftjoin('problems', 'problems.id', '=', 'cases.problem_id')
									->leftjoin('priority', 'priority.id', '=', 'problems.priority_id')
									->leftjoin('access_controls', 'access_controls.id', '=', 'cases.user_id')
									->leftjoin('users_data', 'users_data.id', '=', 'access_controls.user_data_id')
									->select('cases.*', 'problems.name as problema',
										DB::raw("users_data.ud_first_name + ' ' +users_data.ud_father_surname AS usuario") ,'priority.name as prioridad')
									->where('case_has_flows.status', '=', '1')
									->where('cases.user_id' , '=', $user_id);
								},

							// 	},
							// // 'problems',
							//
							//
							'board'
							])->leftjoin('flows as child', 'flows.next_step', '=', 'child.id')
							->select('flows.*','child.id as id_siguiente','child.description_step as siguiente')
							->where('flows.status',1)->get();



			return $result;

		});
				return $result;
			} catch (Exception $e) {
				DB::rollback();
				return self::responseMessage(0, $e->getMessage());
			}
	}
}
