<?php

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'helpdesk', 'namespace' => 'Modules\HelpDesk\Http\Controllers'], function()
{
    Route::get('/', 'HelpDeskController@index')->middleware('authorized');
    Route::get('/type/list', 'TypeProblemController@listAllType');
    Route::get('/problem/list', 'ProblemController@listAllProblem');
    Route::get('/board/list', 'BoardController@listAllBoards');
    Route::get('/board/listactive', 'BoardController@listAllBoardsActive');
    Route::get('/priority/list', 'PriorityController@listPriorityActive');
    Route::get('/problem/listactive/{id}', 'ProblemController@listproblembytype');
    Route::get('/typeproblem/listactive', 'TypeProblemController@listAllTypeActive');
    Route::get('/flowscheck/{id_inicio}/{id_fin}/{board}','FlowController@flowCheck');
    Route::get('/alldata/{id}', 'FlowController@listAllData');
    Route::get('/alldata/{id}/{user_id}', 'FlowController@listAllDataByUser');
    Route::get('/detailsticket/{id}', 'CaseController@detailsTicket');
    Route::get('/typeProblem/status/{id}', 'TypeProblemController@changeStatus');
    Route::get('/listresponsiblesActive/{idboard}','ResponsibleController@listallresponsibles');
    Route::patch('/updateresponsable/{id}','CaseController@actualizeResponsible');
    Route::resource('/type-problem','TypeProblemController');
    Route::resource('/problem', 'ProblemController');
    Route::resource('/cases', 'CaseController');
    Route::resource('/comment', 'CommentController');
    Route::resource('/attachedFile', 'AttachedFileController');
    Route::resource('/board', 'BoardController');
    Route::resource('/flow', 'FlowController');
    Route::resource('/casehasflow', 'CaseHasFlowController');
    Route::get('/dashboard', function(){
  		return view('helpdesk::boardUser');
  	})->middleware('authorized');
});
