<?php

namespace Modules\HelpDesk\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\HelpDesk\Entities\Responsible;

class ResponsibleHelpDesk
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Responsible::where('user_id', Auth::user()->id)
                ->exists()) {
            return redirect('/helpdesk/tablero');
        }
        return $next($request);
    }
}
