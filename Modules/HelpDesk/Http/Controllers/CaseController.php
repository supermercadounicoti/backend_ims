<?php

namespace Modules\HelpDesk\Http\Controllers;

use DB;
use Storage;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\HelpDesk\Entities\Cases;
class CaseController extends Controller
{
  use UtilsFromTraits;

  public function store(Request $request)
  {
	try {
		$result = Cases::createCases($request);
	} catch (\Exception $e) {
		return $this->responseMessage(0, $e->getMessage());
	}
	return $this->responseMessage(1, 'The record was created');
  }

  public function detailsTicket($id)
  {
	  $result = Cases::detailsTicket($id);

	  return $result;
  }

  public function update($cases, Request $request)
	{
		try {
			$result = Cases::actualizeDateEstimated($cases, $request);
			return $this->responseMessage(1, 'The record was update');
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

  public function actualizeResponsible($cases, Request $request)
	{
		try {
			$result = Cases::actualizeResponsible($cases, $request);
			return $this->responseMessage(1, 'The record was update');
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}
}
