<?php

namespace Modules\HelpDesk\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\HelpDesk\Entities\TypeProblem;

class TypeProblemController extends Controller
{
	use UtilsFromTraits;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('helpdesk::maintainers.type_problem.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('helpdesk::maintainers.supermarket.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	   try {
			$result = TypeProblem::store($request);
			return $this->responseMessage(1, 'The record was created');
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Rol  $typeProblem
	 * @return \Illuminate\Http\Response
	 */
	public function show(TypeProblem $typeProblem)
	{

		$result = TypeProblem::listType($typeProblem);

		return $this->responseMessage(1, $result);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Construction  $Construction
	 * @return \Illuminate\Http\Response
	 */
	public function edit(TypeProblem $typeProblem)
	{
		return view('helpdesk::maintainers.supermarket.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\typeProblem  $typeProblem
	 * @return \Illuminate\Http\Response
	 */
	public function update(TypeProblem $typeProblem, Request $request)
	{
		try {
			$result = TypeProblem::actualize($typeProblem, $request);
			return $this->responseMessage(1, 'The record was update');
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\typeProblem  $typeProblem
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(TypeProblem $typeProblem)
	{
		try {
				$result = TypeProblem::destroytype($typeProblem);

				return $this->responseMessage(1, 'The record was changed status');

		} catch (\Exception $e) {
				return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Change status of the specified resource
	 * @param  \App\typeProblem  $typeProblem
	 * @return [type] [description]
	 */
	public function changeStatus($id)
	{
		try {
			$result = TypeProblem::change($id);

			return $this->responseMessage(1, 'The status was changed');

		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}
	/**
	 * List all boards that exists
	 * @return [type] [description]
	 */
	public function listAllTypeActive()
	{
		$result = TypeProblem::listAllTypeActive();

		return $result;
	}

	/**
	 * List all boards that exists
	 * @return [type] [description]
	 */
	public function listAllType()
	{
		$result = TypeProblem::listAllType();

		return datatables()->of($result)->toJson();
	}
}
