<?php

namespace Modules\HelpDesk\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\HelpDesk\Entities\Flow;

class FlowController extends Controller
{
    use UtilsFromTraits;

    public function listAllData($id)
  	{
  			$result = Flow::listAllData($id);

  			return datatables()->of($result)->toJson();
  	}
     public function listAllDataByUser($id , $user_id)
    {
        $result = Flow::listAllDataByUser($id , $user_id);

        return datatables()->of($result)->toJson();
    }

    public function detailsTicket($id)
  	{
  			$result = Flow::detailsTicket($id);

  			return $result;
  	}

    public function flowCheck($id_inicio, $id_fin , $board)
    {
        $result = Flow::flowCheck($id_inicio, $id_fin , $board);

        return $result;
    }
}
