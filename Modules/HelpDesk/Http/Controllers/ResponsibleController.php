<?php

namespace Modules\HelpDesk\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\HelpDesk\Entities\Responsible;

class ResponsibleController extends Controller
{
    use UtilsFromTraits;

    public function listallresponsibles($idResponsible)
  	{
  			$result = Responsible::listallresponsibles($idResponsible);

  			return $result;
  	}
}
