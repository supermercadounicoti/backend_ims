<?php

namespace Modules\HelpDesk\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\HelpDesk\Entities\Problem;

class ProblemController extends Controller
{
	use UtilsFromTraits;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('helpdesk::maintainers.problem.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('helpdesk::maintainers.supermarket.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	   try {
			$result = Problem::store($request);
			return $this->responseMessage(1, 'The record was created');
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Rol  $problem
	 * @return \Illuminate\Http\Response
	 */
	public function show(Problem $problem)
	{

		$result = Problem::listProblem($problem);

		return $this->responseMessage(1, $result);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Construction  $Construction
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Problem $problem)
	{
		return view('helpdesk::maintainers.supermarket.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\problem  $problem
	 * @return \Illuminate\Http\Response
	 */
	public function update(Problem $problem, Request $request)
	{
		try {
			$result = Problem::actualize($problem, $request);
			return $this->responseMessage(1, 'The record was update');
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\problem  $problem
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Problem $problem)
	{
		try {
				$result = Problem::destroyProblem($problem);

				return $this->responseMessage(1, 'The record was changed status');

		} catch (\Exception $e) {
				return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Change status of the specified resource
	 * @param  \App\problem  $problem
	 * @return [type] [description]
	 */
	public function changeStatus($id)
	{
		try {
			$result = Problem::change($id);

			return $this->responseMessage(1, 'The status was changed');

		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	 public function listproblembytype($id)
	 {
				try {
					 $result = Problem::listproblembytype($id);

			 } catch (\Exception $e) {
					 DB::rollback();
					 // dd($e);
					 return $this->responseMessage(0, $e->getMessage());
			 }
			 return $result;
	 }

	 /**
	 * List all boards that exists
	 * @return [type] [description]
	 */
	public function listAllProblem()
	{
		$result = Problem::listAllProblem();

		return datatables()->of($result)->toJson();
	}
}
