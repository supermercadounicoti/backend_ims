<?php

namespace Modules\HelpDesk\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\HelpDesk\Entities\Comment;

class CommentController extends Controller
{
	use UtilsFromTraits;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('helpdesk::maintainers.supermarket.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('helpdesk::maintainers.supermarket.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	   try {
			$result = Comment::store($request);
			return $this->responseMessage(1, 'The record was created');
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Rol  $comment
	 * @return \Illuminate\Http\Response
	 */
	public function show(Comment $comment)
	{

		return $this->responseMessage(1, $comment);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Construction  $Construction
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Comment $comment)
	{
		return view('helpdesk::maintainers.supermarket.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\comment  $comment
	 * @return \Illuminate\Http\Response
	 */
	public function update(Comment $comment, Request $request)
	{
		try {
			$result = Comment::actualize($comment, $request);
			return $this->responseMessage(1, 'The record was update');
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\comment  $comment
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Comment $comment)
	{
		try {  
			$result = Comment::remove($comment);
			
			return $this->responseMessage(1, 'The record was removed');
		
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Change status of the specified resource
	 * @param  \App\comment  $comment
	 * @return [type] [description]
	 */
	public function changeStatus($id)
	{
		try {  
			$result = Comment::change($id);

			return $this->responseMessage(1, 'The status was changed');
		
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}
}
