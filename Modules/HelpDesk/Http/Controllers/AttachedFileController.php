<?php

namespace Modules\HelpDesk\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\HelpDesk\Entities\AttachedFile;

class AttachedFileController extends Controller
{
	use UtilsFromTraits;

	protected $module;

	public function __construct()
	{
		$this->module = env('NAME_HELPDESK');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('helpdesk::maintainers.supermarket.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('helpdesk::maintainers.supermarket.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	   try {
	   		$request = $this->upload($this->module, $request);
			$result = AttachedFile::store($request);
			return $this->responseMessage(1, 'The record was created');
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Rol  $problem
	 * @return \Illuminate\Http\Response
	 */
	public function show(AttachedFile $attachedFile)
	{

		return $this->responseMessage(1, $problem);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Construction  $Construction
	 * @return \Illuminate\Http\Response
	 */
	public function edit(AttachedFile $attachedFile)
	{
		return view('helpdesk::maintainers.supermarket.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\problem  $problem
	 * @return \Illuminate\Http\Response
	 */
	public function update(AttachedFile $attachedFile, Request $request)
	{
		try {
			$result = AttachedFile::actualize($problem, $request);
			return $this->responseMessage(1, 'The record was update');
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\problem  $problem
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(AttachedFile $attachedFile)
	{
		try {  
			$result = AttachedFile::remove($problem);
			
			return $this->responseMessage(1, 'The record was removed');
		
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Change status of the specified resource
	 * @param  \App\problem  $problem
	 * @return [type] [description]
	 */
	public function changeStatus($id)
	{
		try {  
			$result = AttachedFile::change($id);

			return $this->responseMessage(1, 'The status was changed');
		
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}
}
