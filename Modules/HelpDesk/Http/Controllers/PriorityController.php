<?php

namespace Modules\HelpDesk\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\HelpDesk\Entities\Priority;

class PriorityController extends Controller
{
    use UtilsFromTraits;

    /**
   * List all boards that exists
   * @return [type] [description]
   */
  public function listPriorityActive()
  {
    $result = Priority::listPriorityActive();

    return $result;
  }
   

}
