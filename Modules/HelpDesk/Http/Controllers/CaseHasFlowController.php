<?php

namespace Modules\HelpDesk\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\HelpDesk\Entities\CaseHasFlow;

class CaseHasFlowController extends Controller
{
  use UtilsFromTraits;

  public function update($cases, Request $request)
  {
    try {
      $result = CaseHasFlow::actualizeCaseFlow($cases, $request);
      return $this->responseMessage(1, 'The record was update');
    } catch (\Exception $e) {
      return $this->responseMessage(0, $e->getMessage());
    }
    return $result;
  }
}
