<?php

namespace Modules\HelpDesk\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\HelpDesk\Entities\Board;


class BoardController extends Controller
{
	use UtilsFromTraits;

	protected $module;

	public function __construct()
	{
		$this->module = env('NAME_HELPDESK');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('helpdesk::maintainers.board.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('helpdesk::maintainers.supermarket.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	   try {
			$result = Board::store($request);
			return $this->responseMessage(1, 'The record was created');
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Rol  $board
	 * @return \Illuminate\Http\Response
	 */
	public function show(Board $board)
	{

		$result = Board::listBoards($board);

		return $this->responseMessage(1, $result);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Construction  $Construction
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Board $board)
	{
		return view('helpdesk::maintainers.supermarket.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\board  $board
	 * @return \Illuminate\Http\Response
	 */
	public function update(Board $board, Request $request)
	{
		try {
			$result = Board::actualize($board, $request);
			return $this->responseMessage(1, 'The record was update');
		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\board  $board
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Board $board)
	{
		try {
				$result = Board::destroyBoard($board);

				return $this->responseMessage(1, 'The record was changed status');

		} catch (\Exception $e) {
				return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * Change status of the specified resource
	 * @param  \App\board  $board
	 * @return [type] [description]
	 */
	public function changeStatus($id)
	{
		try {
			$result = Board::change($id);

			return $this->responseMessage(1, 'The status was changed');

		} catch (\Exception $e) {
			return $this->responseMessage(0, $e->getMessage());
		}
		return $result;
	}

	/**
	 * List all boards that exists
	 * @return [type] [description]
	 */
	public function listAllBoards()
	{
			$result = Board::listAllBoards();

			return datatables()->of($result)->toJson();
	}

	/**
	 * List all boards that exists
	 * @return [type] [description]
	 */
	public function listAllBoardsActive()
	{
			$result = Board::listAllBoardsActive();

			return $result;
	}
}
