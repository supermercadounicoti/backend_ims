<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

    <title>UniTv</title>
    <link href="https://fonts.googleapis.com/css?family=Fredoka+One" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amaranth" rel="stylesheet">


    <style type="text/css">

    @font-face {
  font-family: 'weather';
  src: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.eot');
  src: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.eot?#iefix') format('embedded-opentype'),
       url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.woff') format('woff'),
       url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.ttf') format('truetype'),
       url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.svg#artill_clean_weather_iconsRg') format('svg');
  font-weight: normal;
  font-style: normal;
}
#weather {
  width: 500px;
  margin: 0px auto;
  text-align: center;
  text-transform: uppercase;
}
i {
  color: #fff;
  font-family: weather;
  font-size: 150px;
  font-weight: normal;
  font-style: normal;
  line-height: 1.0;
  text-transform: none;
}

.icon-0:before { content: ":"; }
.icon-1:before { content: "p"; }
.icon-2:before { content: "S"; }
.icon-3:before { content: "Q"; }
.icon-4:before { content: "S"; }
.icon-5:before { content: "W"; }
.icon-6:before { content: "W"; }
.icon-7:before { content: "W"; }
.icon-8:before { content: "W"; }
.icon-9:before { content: "I"; }
.icon-10:before { content: "W"; }
.icon-11:before { content: "I"; }
.icon-12:before { content: "I"; }
.icon-13:before { content: "I"; }
.icon-14:before { content: "I"; }
.icon-15:before { content: "W"; }
.icon-16:before { content: "I"; }
.icon-17:before { content: "W"; }
.icon-18:before { content: "U"; }
.icon-19:before { content: "Z"; }
.icon-20:before { content: "Z"; }
.icon-21:before { content: "Z"; }
.icon-22:before { content: "Z"; }
.icon-23:before { content: "Z"; }
.icon-24:before { content: "E"; }
.icon-25:before { content: "E"; }
.icon-26:before { content: "3"; }
.icon-27:before { content: "a"; }
.icon-28:before { content: "A"; }
.icon-29:before { content: "a"; }
.icon-30:before { content: "A"; }
.icon-31:before { content: "6"; }
.icon-32:before { content: "1"; }
.icon-33:before { content: "6"; }
.icon-34:before { content: "1"; }
.icon-35:before { content: "W"; }
.icon-36:before { content: "1"; }
.icon-37:before { content: "S"; }
.icon-38:before { content: "S"; }
.icon-39:before { content: "S"; }
.icon-40:before { content: "M"; }
.icon-41:before { content: "W"; }
.icon-42:before { content: "I"; }
.icon-43:before { content: "W"; }
.icon-44:before { content: "a"; }
.icon-45:before { content: "S"; }
.icon-46:before { content: "U"; }
.icon-47:before { content: "S"; }

#weather h2 {
  margin: 0 0 8px;
  color: #fff;
  font-size: 100px;
  font-weight: 300;
  text-align: center;
  text-shadow: 0px 1px 3px rgba(0, 0, 0, 0.15);
}

#weather ul {
  margin: 0;
  padding: 0;
}

#weather li {
  background: #fff;
  background: rgba(255,255,255,0.90);
  padding: 20px;
  display: inline-block;
  border-radius: 5px;
}

#weather .currently {
  margin: 0 20px;
}


    .fecha {
        font-family: 'Amaranth', sans-serif;
        text-align: center;
        font-size:2em;
        background: rgba(255,255,255,.8);

        width: 80%;
        color:#329961;
    }

    .reloj {
        font-family: 'Amaranth', sans-serif;
        width: 80%;
        font-size: 3.5em;
        text-align: center;
        background: rgba(255,255,255,.8);
        color:#329961;

    }
    .publicidad{
         font-family: Oswald, Arial;
        width: 100%;
        padding: 40px;
        font-size: 4em;
        text-align: center;
        /* background: rgba(255,255,255,.3); */

    }
    .publicidad2{
         font-family: Oswald, Arial;
        width: 100%;
        padding: 20px;
        font-size: 4em;
        text-align: center;
        /* background: rgba(255,255,255,.3); */

    }
    /* .horarios{
         font-family: Oswald, Arial;
        width: 100%;

        padding: 10px;
        font-size: 4em;
        text-align: center;
        background: rgba(255,255,255,.3);

    } */
        /*
     * Globals
     */

    /* Links */
    a,
    a:focus,
    a:hover {
      color: #fff;
    }

    /* Custom default button */
    .btn-secondary,
    .btn-secondary:hover,
    .btn-secondary:focus {
      color: #333;
      text-shadow: none; /* Prevent inheritance from `body` */
      background-color: #fff;
      border: .05rem solid #fff;
    }

    /*
     * Base structure
     */

    html,
    body {
      height: 100%;
      /* background: linear-gradient(to bottom right, #329961, #C6C541); */
      background-color: #D5D5D5;
      /*background-image: url(images/congruent_pentagon.png);*/
    }

    body {
      /* padding:40px; */
      /* color: #080808; */
      /* text-shadow: 0 .05rem .1rem rgba(0, 0, 0, .5);
      box-shadow: inset 0 0 5rem rgba(0, 0, 0, .5); */
    }
    .h1unico
    {
         font-family: 'Cookie', cursive;
         padding-left: 40px;
         padding-top: 80px;
         padding-bottom: 40px;
         font-size: 2.0rem;
         color:#FFF;
    }
    .h1trabajador
    {
         padding-top: 0px;
         padding-bottom: 20px;
         font-family: 'Cookie', cursive;
         font-size: 2.5rem;
         color:#FFF;
         text-align: center;
    }

    .logo {
       position: absolute;
       left: 70%;
       right: 0;
       top: 30%;
       margin: 0 auto;
       z-index: 9999999;
    }
*,*:after, *:before{
  margin: 0;
  padding: 0;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
#contenedor_carga{
  background: linear-gradient(to bottom right, #329961, #C6C541);
  height: 100%;
  width: 100%;
  position: fixed;
  -webkit-transition: all 1s ease;
  -o-transition: all 1s ease;
  transition: all 1s ease;
  z-index: 10000;

}
.spinner {
  height: 150px;
  width: 150px;
  border-radius: 100%;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
}

.double-bounce1, .double-bounce2 {
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background-color: #fff;
  opacity: 0.6;
  position: absolute;
  top: 0;
  left: 0;

  -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
  animation: sk-bounce 2.0s infinite ease-in-out;
}

.double-bounce2 {
  -webkit-animation-delay: -1.0s;
  animation-delay: -1.0s;
}

@-webkit-keyframes sk-bounce {
  0%, 100% { -webkit-transform: scale(0.0) }
  50% { -webkit-transform: scale(1.0) }
}

@keyframes sk-bounce {
  0%, 100% {
    transform: scale(0.0);
    -webkit-transform: scale(0.0);
  } 50% {
    transform: scale(1.0);
    -webkit-transform: scale(1.0);
  }
}


    </style>
    </head>

	<script>
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};
	</script>


</head>

<body>
    <div id="contenedor_carga">
      <div class="spinner">
  <div class="double-bounce1"></div>
  <div class="double-bounce2"></div>
</div>
      <!-- <div id="carga">
          <img src="http://www.supermercadounico.cl/img/sitio/logo_unico.png" width="110%" alt=""> <br><br>
      <center><span style="font-size:30px;color:#fff;font-family: 'Amaranth', sans-serif;"> Cargando...</span></center>
      </div> -->
    </div>
	<div id="app">



            <img src="{{URL::asset('/images/kiosko.png')}}" alt="">
            <div class="container-fluid">
              <div class="row">


            <div class="col-md-8">
              <div id="carouselExampleSlidesOnly" class="carousel slide publicidad" data-ride="carousel" style="">
               <div class="carousel-inner">

                  <div  :class=" ['carousel-item', {'active': index === 0}]" v-for="(item, index) in images_slider_informative" >
                   <img class="d-block w-100" v-bind:src="item.path" alt="First slide">
                 </div>
                 <!-- ? 'active' : '' -->
                 <!-- <div class="carousel-item">
                   <img class="d-block w-100" src="http://www.supermercadounico.cl/upload/paginas/archivos/08-02-2018-13-39-32_banner.jpg" alt="First slide">
                 </div>  -->


               </div>

             </div>
             <div class="row">
                <div class="col-md-6">
                  <div style="padding-left:40px">


                    <div class="embed-responsive embed-responsive-16by9" style="width:560px; height:329px" >
                <iframe width="560" height="315" src="http://www.youtube.com/embed/3QTyrIfqbOg?autoplay=1&loop=1&playlist=3QTyrIfqbOg" frameborder="0" allowfullscreen></iframe>
                </div>
                </div>
                </div>




                <div class="col-md-6">
                    <img src="{{URL::asset('/images/img.png')}}" alt="">
                </div>
             </div>

          <!-- v-bind:src="data.imageSrc" -->

          </div>
              <!-- <slider-informative></slider-informative>
              <videos></videos>
              <slider-address></slider-address> -->

            <div class="col-md-4">



              <div class="fecha" style="position: absolute; top: -165px;">
                       <span id="diaSemana"></span>
                       <span id="dia" class="dia"></span>
                       <span>de </span>
                       <span id="mes" class="mes"></span>
                       <span>del </span>
                       <span id="year" class="year"></span>
                   </div>

                   <div class="reloj" style="position: absolute; top: -117px;">
                       <span id="horas" class="horas"></span>
                       <span>:</span>
                       <span id="minutos" class="minutos"></span>
                       <span>:</span>
                       <span id="segundos" class="segundos"></span>
                           <span id="ampm" class="ampm"></span>


                   </div>


 <br><br>
                   <div class="">
                     <div id="employees" class="carousel slide publicidad2" data-ride="carousel" style="">
                      <div class="carousel-inner"  >

                         <div  :class=" ['carousel-item',{'active': index === 0 }]" v-for="(item, index) in images_employees" >
                          <img class="d-block w-100" v-bind:src="item.path" alt="First slide">
                        </div>
                        <!-- <div class="carousel-item">
                          <img class="d-block w-100" src="http://www.supermercadounico.cl/upload/paginas/archivos/08-02-2018-13-39-32_banner.jpg" alt="First slide">
                        </div>  -->


                      </div>

                    </div>
                   </div>
                   <div class="">
                     <div id="carousel" class="carousel slide publicidad2" data-ride="carousel" style="">
                      <div class="carousel-inner"  >

                         <div  :class=" ['carousel-item',{'active': index === 0 }]" v-for="(item, index) in images_product" >
                          <img class="d-block w-100" v-bind:src="item.path" alt="First slide">
                        </div>
                        <!-- <div class="carousel-item">
                          <img class="d-block w-100" src="http://www.supermercadounico.cl/upload/paginas/archivos/08-02-2018-13-39-32_banner.jpg" alt="First slide">
                        </div>  -->


                      </div>

                    </div>
                   </div>
                   <br>
                   <div class="" style="background-color:#A2BE30;margin-left:20px;margin-right:20px;padding-top:20px" >
                     <div id="weather"></div>
                     <br>
                   </div>

            </div>
            </div>

</div>

        <!-- <button v-on:click="getSupermarket">ir</button> -->




  	</div>

    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.1.0/jquery.simpleWeather.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>


    <script>
        var socket = io('http://192.168.10.10:3000');

        const app = new Vue({
            el: '#app',
            data: {
                value: '',
                options: [
                  { text: 'Nacimiento', value: '1' },
                  { text: 'Santa barbara', value: '2' },
                  { text: 'Laja', value: '3' },
                  { text: 'Angol', value: '4' },
                  { text: 'Mulchen', value: '5' },
                  { text: 'Los Angeles', value: '6' },
                ],
                // images_slider_informative:{
                //
                // }
                images_slider_informative: [],
                videos:[],
                images_product: [],
                images_employees:[]
            },
            mounted: function() {
                let _self = this;
                let _$ = $;
                let supermarket = '1';
                _self.getSliderInformative();
                _self.getVideos();
                _self.getImages();
                _self.getEmployees();

                socket.on('sendPackage:SendPackageEvent', function(message) {


                    if (message.supermarket_id == 1  /*supermercado id*/ && message.disabled == 0){
                      if(message.type_id == 1){
                        _self.images_slider_informative.push({id:message.informative_id , path:'/storage/'+message.path});
                      }else if (message.type_id == 2){
                        _self.images_product.push({id:message.informative_id , path:'/storage/'+message.path});
                      }
                      else if (message.type_id == 3){
                        _self.images_employees.push({id:message.informative_id , path:'/storage/'+message.path});
                      }


                    }
                    else if(message.supermarket_id == 1 /*supermercado id*/ && message.disabled == 1){
                      if(message.type_id == 1){
                        _$.each(_self.images_slider_informative, function(value, key) {

                          for(var i = 0; i < _self.images_slider_informative.length; i++) {
                                if(_self.images_slider_informative[i].id == message.informative_id) {
                                    _self.images_slider_informative.splice(i, 1);
                                    break;
                                }
                            }



                          });

                          let _ActiveElement = _$('#carouselExampleSlidesOnly').find('.carousel-item.active');
                          _ActiveElement.removeClass('active');
                          let _NextElement =  _$('#carouselExampleSlidesOnly').find('.carousel-item').first();
                          _NextElement.addClass('active');
                          // _$("#carouselExampleSlidesOnly .carousel-inner .carousel-item:last-child").removeClass("active");
                          // _$("#carouselExampleSlidesOnly .carousel-inner .carousel-item:first").addClass("active");
                      }
                      else if(message.type_id == 2){
                        _$.each(_self.images_product, function(value, key) {

                          for(var i = 0; i < _self.images_product.length; i++) {
                                if(_self.images_product[i].id == message.informative_id) {

                                    _self.images_product.splice(i, 1);

                                    break;
                                }
                            }


                          });
                          let _ActiveElement = _$('#carousel').find('.carousel-item.active');
                          _ActiveElement.removeClass('active');
                          let _NextElement =  _$('#carousel').find('.carousel-item').first();
                          _NextElement.addClass('active');
                      }
                      else if(message.type_id == 3){
                        _$.each(_self.images_employees, function(value, key) {

                          for(var i = 0; i < _self.images_employees.length; i++) {
                                if(_self.images_employees[i].id == message.informative_id) {

                                    _self.images_employees.splice(i, 1);

                                    break;
                                }
                            }


                          });
                          let _ActiveElement = _$('#employees').find('.carousel-item.active');
                          _ActiveElement.removeClass('active');
                          let _NextElement =  _$('#employees').find('.carousel-item').first();
                          _NextElement.addClass('active');
                      }




                    }
                });
            },
            methods: {
              getSliderInformative() {
                let _self = this;
                let _$ = $;
                axios.get('./informative/list/1').then(function (response) {
                  _$.each(response.data.data, function(value, key) {
                    _$.each(key.supermarkets, function(value, key2) {
                      if(key2.id == 1 /*supermercado id*/ && key.status == 1){
                        _self.images_slider_informative.push({id:key.id , path:'/storage/'+key.path});
                        }
                      });
                    });
                  }).catch(function(error) {
                      return rej();
                  });
                },
              getVideos(){
                let _self = this;
                let _$ = $;
                axios.get('video/list').then(function (response) {

                  _$.each(response.data.data, function(value, key) {

                    _$.each(key.supermarkets, function(value, key2) {
                      if(key2.id == 1 && key.status == 1){
                        _self.videos.push({id:key.id , url:key.url});
                        }
                      });
                    });

                  }).catch(function(error) {
                      return rej();
                  });
              },
              getImages(){
                let _self = this;
                let _$ = $;
                axios.get('./informative/list/2').then(function (response) {
                  _$.each(response.data.data, function(value, key) {
                    _$.each(key.supermarkets, function(value, key2) {
                      if(key2.id == 1 /*supermercado id*/ && key.status == 1){
                        _self.images_product.push({id:key.id , path:'/storage/'+key.path});
                        }
                      });
                    });
                  }).catch(function(error) {
                      return rej();
                  });
              },
              getEmployees(){
                let _self = this;
                let _$ = $;
                axios.get('./informative/list/3').then(function (response) {
                  _$.each(response.data.data, function(value, key) {
                    _$.each(key.supermarkets, function(value, key2) {
                      if(key2.id == 1 /*supermercado id*/ && key.status == 1){
                        _self.images_employees.push({id:key.id , path:'/storage/'+key.path});
                        }
                      });
                    });
                  }).catch(function(error) {
                      return rej();
                  });
              }
            }
        });
    </script>

    <script type="text/javascript">
        window.onload = function(){
            var contenedor = document.getElementById('contenedor_carga');
            contenedor.style.visibility = 'hidden';
            contenedor.style.opacity = '0';
        };
        $(document).ready(function(){
                      $.simpleWeather({
                location: 'Angol, CHL',
                woeid: '',
                unit: 'C',
                language:'ES',
                success: function(weather) {
                  html = '<h2><i class="icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+'</h2>';
                  html += '<ul><li>'+weather.city+'</li>';


                  $("#weather").html(html);
                },
                error: function(error) {
                  $("#weather").html('<p>'+error+'</p>');
                }
              });
        var actualizarHora = function(){
            // Obtenemos la fecha actual, incluyendo las horas, minutos, segundos, dia de la semana, dia del mes, mes y año;
            var fecha = new Date(),
                horas = fecha.getHours(),
                ampm,
                minutos = fecha.getMinutes(),
                segundos = fecha.getSeconds(),
                diaSemana = fecha.getDay(),
                dia = fecha.getDate(),
                mes = fecha.getMonth(),
                year = fecha.getFullYear();

            // Accedemos a los elementos del DOM para agregar mas adelante sus correspondientes valores
            var pHoras = document.getElementById('horas'),
                pAMPM = document.getElementById('ampm'),
                pMinutos = document.getElementById('minutos'),
                pSegundos = document.getElementById('segundos'),
                pDiaSemana = document.getElementById('diaSemana'),
                pDia = document.getElementById('dia'),
                pMes = document.getElementById('mes'),
                pYear = document.getElementById('year');


            // Obtenemos el dia se la semana y lo mostramos
            var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
            pDiaSemana.textContent = semana[diaSemana];

            // Obtenemos el dia del mes
            pDia.textContent = dia;

            // Obtenemos el Mes y año y lo mostramos
            var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
            pMes.textContent = meses[mes];
            pYear.textContent = year;

            // Cambiamos las hora de 24 a 12 horas y establecemos si es AM o PM

            if (horas >= 12) {
                horas = horas - 12;
                ampm = 'PM';
            } else {
                ampm = 'AM';
            }

            // Detectamos cuando sean las 0 AM y transformamos a 12 AM
            if (horas == 0 ){
                horas = 12;
            }

            // Si queremos mostrar un cero antes de las horas ejecutamos este condicional
            // if (horas < 10){horas = '0' + horas;}
            pHoras.textContent = horas;
            pAMPM.textContent = ampm;

            // Minutos y Segundos
            if (minutos < 10){ minutos = "0" + minutos; }
            if (segundos < 10){ segundos = "0" + segundos; }

            pMinutos.textContent = minutos;
            pSegundos.textContent = segundos;
        };

        actualizarHora();
        var intervalo = setInterval(actualizarHora, 1000);
    }())
    </script>
</body>
</html>
