@extends('helpdesk::master')
@section('title', 'HelpDesk - Board')
@section('style')
<style media="screen">
.main {
  /* background: linear-gradient(to bottom right, #053B99, #D62E00); */
  background-color: rgb(75, 191, 107);
  min-width: 100%;
  min-height: 100% !important;

}
.row > .col-xs-3 {
    display:flex;
    flex: 0 0 25%;
    max-width: 25%;
    overflow-x: auto;
  }

.flex-nowrap {
    -webkit-flex-wrap: nowrap!important;
    -ms-flex-wrap: nowrap!important;
    flex-wrap: nowrap!important;
      overflow-x: auto;
}
.flex-row {
    display:flex;
    -webkit-box-orient: horizontal!important;
    -webkit-box-direction: normal!important;
    -webkit-flex-direction: row!important;
    -ms-flex-direction: row!important;
    flex-direction: row!important;
      overflow-x: auto;
}


.well {
    min-height: 300px;
    width: 100%;
}
*,*:after, *:before{
  margin: 0;
  padding: 0;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
#contenedor_carga{
  background: rgb(75, 191, 107);
  height: 90%;
  width: 100%;
  position: fixed;
  -webkit-transition: all 1s ease;
  -o-transition: all 1s ease;
  transition: all 1s ease;
  /* z-index: 10000; */

}
.spinner {
  height: 150px;
  width: 150px;
  border-radius: 100%;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
}

.double-bounce1, .double-bounce2 {
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background-color: #fff;
  opacity: 0.6;
  position: absolute;
  top: 0;
  left: 0;

  -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
  animation: sk-bounce 2.0s infinite ease-in-out;
}

.double-bounce2 {
  -webkit-animation-delay: -1.0s;
  animation-delay: -1.0s;
}

@-webkit-keyframes sk-bounce {
  0%, 100% { -webkit-transform: scale(0.0) }
  50% { -webkit-transform: scale(1.0) }
}

@keyframes sk-bounce {
  0%, 100% {
    transform: scale(0.0);
    -webkit-transform: scale(0.0);
  } 50% {
    transform: scale(1.0);
    -webkit-transform: scale(1.0);
  }
}
</style>
@stop
@section('wrapper')
<div>
  <div>
  
   <boards></boards>

@stop

@section('scripts')
<script type="text/javascript">
window.onload = function() {
$('body').addClass('sidebar-hidden');
// var contenedor = document.getElementById('contenedor_carga');
// contenedor.style.visibility = 'hidden';
// contenedor.style.opacity = '0';
}
// $("#closeModal").click(function() {
//
//   $('.bd-search-modal-lg').modal('hide');
// });
</script>
@stop
