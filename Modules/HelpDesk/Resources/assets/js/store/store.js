import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


const state = {
  count: 0,
  fresh_id : 0

}

const mutations = {
  increment (state) {
    state.count++
  },
  decrement (state) {
    state.count--
  },
  cleanFreshId(state) {
    state.fresh_id = 0
  },
  setFreshId(state, id) {
    state.fresh_id = id
  },

}

const actions = {
  cleanFreshId: ({ commit }) => commit('cleanFreshId'),
  setFreshId: ({ commit }, id) => commit('setFreshId', id),
  increment: ({ commit }) => commit('increment'),
  decrement: ({ commit }) => commit('decrement'),
  incrementIfOdd ({ commit, state }) {
    if ((state.count + 1) % 2 === 0) {
      commit('increment')

    }
  },
  incrementAsync ({ commit }) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        commit('increment')
        resolve()
      }, 1000)
    })
  }
}

const getters = {
  // evenOrOdd: state => state.count % 2 === 0 ? 'even' : 'odd',
  getid: state => state.fresh_id

}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
