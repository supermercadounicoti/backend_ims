<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->dateTime('init_date');
            $table->dateTime('end_date')->nullable();
            $table->dateTime('estimated_date')->nullable();
            $table->integer('responsible_id')->nullable()->unsigned();
            $table->integer('supermarket_id')->unsigned();
            $table->integer('problem_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->foreign('problem_id')->references('id')->on('problems');
            $table->foreign('supermarket_id')->references('id')->on('supermarkets');
                $table->foreign('responsible_id')->references('id')->on('responsibles');
            $table->foreign('user_id')->references('id')->on('access_controls');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases');
    }
}
