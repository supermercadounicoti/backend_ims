<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProblemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('problems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->integer('priority_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->boolean('status');

            $table->timestamps();

            $table->foreign('type_id')
                ->references('id')->on('type_problems');
                $table->foreign('priority_id')
                    ->references('id')->on('priority');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
          Schema::dropIfExists('problems');
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}
