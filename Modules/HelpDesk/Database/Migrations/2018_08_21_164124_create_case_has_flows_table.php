<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseHasFlowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_has_flows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('case_id')->unsigned();
            $table->integer('flow_id')->unsigned();
            $table->boolean('status');
            $table->dateTime('init_date');
            $table->dateTime('end_date')->nullable();
            $table->longText('comments')->nullable();
            $table->foreign('flow_id')->references('id')->on('flows');
            $table->foreign('case_id')->references('id')->on('cases');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
      Schema::dropIfExists('case_has_flows');
        Schema::dropIfExists('informative');
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');


    }
}
