<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number_step');
            $table->string('description_step');
            $table->integer('board_id')->unsigned();
            $table->integer('next_step')->nullable()->unsigned();
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('board_id')
                ->references('id')->on('boards');
            $table->foreign('next_step')
                ->references('id')->on('flows');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
    Schema::dropIfExists('flows');
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}
