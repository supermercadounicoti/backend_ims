<?php

namespace Modules\HelpDesk\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\HelpDesk\Entities\Cases;

class CaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
	  Cases::insert([
			[
				'name' => 'tengo un problema',
				'init_date' => '2018-02-07 14:18:00',
				'end_date' => '2018-02-07 14:18:00',
				'estimated_date'=> '2018-02-07 14:18:00',
				'supermarket_id' => 1,
				'responsible_id' => 1,
				'problem_id' => 1,
				'user_id' => 1,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			]/*,[
				'name' => 'tengo un problema',
				'init_date' => '2018-02-07 14:18:00',
				'end_date' => '2018-02-07 14:18:00',
				'supermarket_id' => '1',
				'problem_id' => '2',
				'estimated_date'=> '2018-02-07 14:18:00',
				'priority_id' => '2'
				'user_id' => 1,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'tengo un problema',
				'init_date' => '2018-02-07 14:18:00',
				'end_date' => '2018-02-07 14:18:00',
				'supermarket_id' => '1',
				'problem_id' => '2',
				'estimated_date'=> '2018-02-07 14:18:00',
				'priority_id' => '3'
				'user_id' => 1,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'tengo un problema',
				'init_date' => '2018-02-07 14:18:00',
				'end_date' => '2018-02-07 14:18:00',
				'supermarket_id' => '1',
				'problem_id' => '2',
				'estimated_date'=> '2018-02-07 14:18:00',
				'priority_id' => '1'
				'user_id' => 1,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'tengo un problema',
				'init_date' => '2018-02-07 14:18:00',
				'end_date' => '2018-02-07 14:18:00',
				'supermarket_id' => '1',
				'problem_id' => '2',
				'estimated_date'=> '2018-02-07 14:18:00',
				'priority_id' => '2'
				'user_id' => 1,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			]*/
	  	]);
	}
}
