<?php

namespace Modules\HelpDesk\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class HelpDeskDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(BoardSeeder::class);
        $this->call(FlowSeeder::class);
        $this->call(PrioritySeederTableSeeder::class);
        $this->call(TypeProblemSeeder::class);
        $this->call(ProblemSeeder::class);
        $this->call(ResponsibleSeeder::class);
        $this->call(CaseSeeder::class);
    }
}
