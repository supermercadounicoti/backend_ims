<?php

namespace Modules\HelpDesk\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\HelpDesk\Entities\Priority;

class PrioritySeederTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
	  Priority::insert([
		[
		  	'name' => 'Alta',
		  	'description' => 'Alta',
		  	'created_at' => '2018-02-07 14:18:00',
		  	'updated_at' => '2018-02-07 14:18:00'
		],[
			'name' => 'Media',
			'description' => 'Media',
			'created_at' => '2018-02-07 14:18:00',
			'updated_at' => '2018-02-07 14:18:00'
		],[
			'name' => 'Baja',
			'description' => 'Baja',
			'created_at' => '2018-02-07 14:18:00',
			'updated_at' => '2018-02-07 14:18:00'
		]
	  ]);
	}
}
