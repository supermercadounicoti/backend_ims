<?php

namespace Modules\HelpDesk\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\HelpDesk\Entities\Responsible;

class ResponsibleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Responsible::insert([
            [
                'user_id' => 1,
                'status' => 1,
                'created_at' => '2018-02-07 14:18:00',
                'updated_at' => '2018-02-07 14:18:00'
            ]
        ]);
    }
}
