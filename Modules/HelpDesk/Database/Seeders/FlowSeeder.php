<?php

namespace Modules\HelpDesk\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\HelpDesk\Entities\Flow;

class FlowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Flow::insert([
          [
                'number_step' => '1',
                'description_step' => 'Por Hacer',
                'board_id' => '1',
                'next_step' => '2',
                'status' => true,
                'created_at' => '2018-02-07 14:18:00',
                'updated_at' => '2018-02-07 14:18:00',
          ],[
                'number_step' => '2',
                'description_step' => 'En Progreso',
                'board_id' => '1',
                'next_step' => '3',
                'status' => true,
                'created_at' => '2018-02-07 14:18:00',
                'updated_at' => '2018-02-07 14:18:00',
              ],[
                'number_step' => '3',
                'description_step' => 'En Revisión',
                'board_id' => '1',
                'next_step' => '4',
                'status' => true,
                'created_at' => '2018-02-07 14:18:00',
                'updated_at' => '2018-02-07 14:18:00',
              ],[
                  'number_step' => '4',
                  'description_step' => 'Terminado',
                  'board_id' => '1',
                  'next_step' => null,
                  'status' => true,
                  'created_at' => '2018-02-07 14:18:00',
                  'updated_at' => '2018-02-07 14:18:00',
              ]
      ]);
    }
}
