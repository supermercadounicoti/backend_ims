<?php

namespace Modules\HelpDesk\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\HelpDesk\Entities\TypeProblem;

class TypeProblemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      TypeProblem::insert([
        [
                  'name' => 'Red',
                  'description' => 'Deterioro del Cable, remplazo de puntas, nuevas extension',
                  'status' => true,
                  'created_at' => '2018-02-07 14:18:00',
                  'updated_at' => '2018-02-07 14:18:00'
              ],[
                  'name' => 'Hardware Impresora',
                  'description' => 'Mantencion y reparacion basica de impresoras',
                  'status' => true,
                  'created_at' => '2018-02-07 14:18:00',
                  'updated_at' => '2018-02-07 14:18:00'
              ],[
                  'name' => 'Software Impresora',
                  'description' => 'Instlacion y configuracion de impresoras',
                  'status' => true,
                  'created_at' => '2018-02-07 14:18:00',
                  'updated_at' => '2018-02-07 14:18:00'
              ],[
                  'name' => 'Hardware Pc o Notebook',
                  'description' => 'Mantencion y Reparacion basica de equipos',
                  'status' => true,
                  'created_at' => '2018-02-07 14:18:00',
                  'updated_at' => '2018-02-07 14:18:00'
              ],[
                  'name' => 'Software Pc o Notebook',
                  'description' => 'Instalacion de Software, restauracion de informacion, Respaldos.',
                  'status' => true,
                  'created_at' => '2018-02-07 14:18:00',
                  'updated_at' => '2018-02-07 14:18:00'
              ],[
                  'name' => 'Soporte de Sistema Compra y Venta',
                  'description' => 'Mantencion de programas, correccion de errores en digitacion, instalacion.',
                  'status' => true,
                  'created_at' => '2018-02-07 14:18:00',
                  'updated_at' => '2018-02-07 14:18:00'
              ],[
                  'name' => 'No especificado',
                  'description' => 'No especificado',
                  'status' => true,
                  'created_at' => '2018-02-07 14:18:00',
                  'updated_at' => '2018-02-07 14:18:00'
              ]

      ]);
    }
}
