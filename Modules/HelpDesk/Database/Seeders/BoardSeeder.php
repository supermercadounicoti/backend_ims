<?php

namespace Modules\HelpDesk\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\HelpDesk\Entities\Board;

class BoardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Board::insert([
        [
                  'name' => 'Desarrollo Informatica',
                  'description' => 'Desarrollos de sistemas',
                  'status' => true,
                  'created_at' => '2018-02-07 14:18:00',
                  'updated_at' => '2018-02-07 14:18:00',
              ],[
                  'name' => 'Soporte Informatica',
                  'description' => 'Soporte de informatica',
                  'status' => true,
                  'created_at' => '2018-02-07 14:18:00',
                  'updated_at' => '2018-02-07 14:18:00',
              ]
      ]);
    }
}
