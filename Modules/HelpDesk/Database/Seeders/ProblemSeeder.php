<?php

namespace Modules\HelpDesk\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\HelpDesk\Entities\Problem;

class ProblemSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
	  Problem::insert([
			[
				'name' => 'Deterioro de Cable',
				'description' => 'Deterioro de Cable',
				'priority_id' => '2',
				'type_id' => '1',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Extension Cable de Red',
				'description' => 'Extension Cable de Red',
				'priority_id' => '2',
				'type_id' => '1',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Punto de Red',
				'description' => 'Punto de Red',
				'priority_id' => '2',
				'type_id' => '1',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Mantencion Impresora',
				'description' => 'Mantencion Impresora',
				'priority_id' => '2',
				'type_id' => '2',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Atasco de Papel',
				'description' => 'Atasco de Papel',
				'priority_id' => '2',
				'type_id' => '2',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Carga de Tinta',
				'description' => 'Carga de Tinta',
				'priority_id' => '2',
				'type_id' => '2',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Instalacion Impresora',
				'description' => 'Instalacion Impresora',
				'priority_id' => '2',
				'type_id' => '3',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Error de Disco',
				'description' => 'Error de Disco',
				'priority_id' => '2',
				'type_id' => '4',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Error de Memoria',
				'description' => 'Error de Memoria',
				'priority_id' => '2',
				'type_id' => '4',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Error de hardware en general',
				'description' => 'Error de hardware en general',
				'priority_id' => '2',
				'type_id' => '4',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Instalacion Sistema Operativo',
				'description' => 'Instalacion Sistema Operativo',
				'priority_id' => '2',
				'type_id' => '5',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Respaldos de Informacion',
				'description' => 'Respaldos de Informacion',
				'priority_id' => '2',
				'type_id' => '5',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Configuracion de Casillas de Correo',
				'description' => 'Desarrollos de sistemas',
				'priority_id' => '2',
				'type_id' => '5',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Instalacion de Programa Compra y Venta',
				'description' => 'Desarrollos de sistemas',
				'priority_id' => '2',
				'type_id' => '5',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Formateo de equipos',
				'description' => 'Desarrollos de sistemas',
				'priority_id' => '2',
				'type_id' => '5',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Seguimiento de Datos',
				'description' => 'Seguimiento de Datos',
				'priority_id' => '2',
				'type_id' => '6',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Anulacion de Movimientos',
				'description' => 'Anulacion de Movimientos',
				'priority_id' => '2',
				'type_id' => '6',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Revision estado de Procesos',
				'description' => 'Revision estado de Procesos',
				'priority_id' => '2',
				'type_id' => '6',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Revision de Ventas',
				'description' => 'Revision de Ventas',
				'priority_id' => '2',
				'type_id' => '6',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			],[
				'name' => 'Auditorias de Informacion',
				'description' => 'Auditorias de Informacion',
				'priority_id' => '2',
				'type_id' => '6',
				'status' => true,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00'
			]
	  ]);
	}
}
