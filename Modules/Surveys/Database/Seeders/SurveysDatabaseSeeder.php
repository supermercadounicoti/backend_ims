<?php

namespace Modules\Surveys\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SurveysDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(CitiesTableSeeder::class);
        // $this->call("OthersTableSeeder");
    }
}
