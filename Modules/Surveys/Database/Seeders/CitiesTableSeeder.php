<?php

namespace Modules\Surveys\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Modules\Surveys\Entities\Cities::insert([
            [
                'city_name' => "Concepción", 
                'city_description' => "Concepción",
                'city_status' => 1,
            ],[
                'city_name' => "Coronel", 
                'city_description' => "Coronel",
                'city_status' => 1,
            ],[
                'city_name' => "Chiguayante", 
                'city_description' => "Chiguayante",
                'city_status' => 1,
            ],[
                'city_name' => "Florida", 
                'city_description' => "Florida",
                'city_status' => 1,
            ],[
                'city_name' => "Hualqui", 
                'city_description' => "Hualqui",
                'city_status' => 1,
            ],[
                'city_name' => "Lota", 
                'city_description' => "Lota",
                'city_status' => 1,
            ],[
                'city_name' => "Penco", 
                'city_description' => "Penco",
                'city_status' => 1,
            ],[
                'city_name' => "San Pedro de la Paz", 
                'city_description' => "San Pedro de la Paz",
                'city_status' => 1,
            ],[
                'city_name' => "Santa Juana", 
                'city_description' => "Santa Juana",
                'city_status' => 1,
            ],[
                'city_name' => "Talcahuano", 
                'city_description' => "Talcahuano",
                'city_status' => 1,
            ],[
                'city_name' => "Tomé", 
                'city_description' => "Tomé",
                'city_status' => 1,
            ],[
                'city_name' => "Hualpén", 
                'city_description' => "Hualpén",
                'city_status' => 1,
            ],[
                'city_name' => "Lebu", 
                'city_description' => "Lebu",
                'city_status' => 1,
            ],[
                'city_name' => "Arauco", 
                'city_description' => "Arauco",
                'city_status' => 1,
            ],[
                'city_name' => "Contulmo", 
                'city_description' => "Contulmo",
                'city_status' => 1,
            ],[
                'city_name' => "Curanilahue", 
                'city_description' => "Curanilahue",
                'city_status' => 1,
            ],[
                'city_name' => "Los Álamos", 
                'city_description' => "Los Álamos",
                'city_status' => 1,
            ],[
                'city_name' => "Tirúa", 
                'city_description' => "Tirúa",
                'city_status' => 1,
            ],[
                'city_name' => "Los Ángeles", 
                'city_description' => "Los Ángeles",
                'city_status' => 1,
            ],[
                'city_name' => "Cabrero", 
                'city_description' => "Cabrero",
                'city_status' => 1,
            ],[
                'city_name' => "Laja", 
                'city_description' => "Laja",
                'city_status' => 1,
            ],[
                'city_name' => "Mulchén", 
                'city_description' => "Mulchén",
                'city_status' => 1,
            ],[
                'city_name' => "Nacimiento", 
                'city_description' => "Nacimiento",
                'city_status' => 1,
            ],[
                'city_name' => "Negrete", 
                'city_description' => "Negrete",
                'city_status' => 1,
            ],[
                'city_name' => "Nacimiento", 
                'city_description' => "Nacimiento",
                'city_status' => 1,
            ],[
                'city_name' => "Quilaco", 
                'city_description' => "Quilaco",
                'city_status' => 1,
            ],[
                'city_name' => "Quilleco", 
                'city_description' => "Quilleco",
                'city_status' => 1,
            ],[
                'city_name' => "San Rosendo", 
                'city_description' => "San Rosendo",
                'city_status' => 1,
            ],[
                'city_name' => "Santa Bárbara", 
                'city_description' => "Santa Bárbara",
                'city_status' => 1,
            ],[
                'city_name' => "Tucapel", 
                'city_description' => "Tucapel",
                'city_status' => 1,
            ],[
                'city_name' => "Yumbel", 
                'city_description' => "Yumbel",
                'city_status' => 1,
            ],[
                'city_name' => "Alto Biobío", 
                'city_description' => "Alto Biobío",
                'city_status' => 1,
            ],[
                'city_name' => "Yumbel", 
                'city_description' => "Yumbel",
                'city_status' => 1,
            ],[
                'city_name' => "Chillán", 
                'city_description' => "Chillán",
                'city_status' => 1,
            ],[
                'city_name' => "Bulnes", 
                'city_description' => "Bulnes",
                'city_status' => 1,
            ],[
                'city_name' => "Cobquecura", 
                'city_description' => "Cobquecura",
                'city_status' => 1,
            ],[
                'city_name' => "Coelemu", 
                'city_description' => "Coelemu",
                'city_status' => 1,
            ],[
                'city_name' => "Coihueco", 
                'city_description' => "Coihueco",
                'city_status' => 1,
            ],[
                'city_name' => "Chillán Viejo", 
                'city_description' => "Chillán Viejo",
                'city_status' => 1,
            ],[
                'city_name' => "El Carmen", 
                'city_description' => "El Carmen",
                'city_status' => 1,
            ],[
                'city_name' => "Ninhue", 
                'city_description' => "Ninhue",
                'city_status' => 1,
            ],[
                'city_name' => "Ñiquén", 
                'city_description' => "Ñiquén",
                'city_status' => 1,
            ],[
                'city_name' => "Pemuco", 
                'city_description' => "Pemuco",
                'city_status' => 1,
            ],[
                'city_name' => "Pinto", 
                'city_description' => "Pinto",
                'city_status' => 1,
            ],[
                'city_name' => "Portezuelo", 
                'city_description' => "Portezuelo",
                'city_status' => 1,
            ],[
                'city_name' => "Quillón", 
                'city_description' => "Quillón",
                'city_status' => 1,
            ],[
                'city_name' => "Quirihue", 
                'city_description' => "Quirihue",
                'city_status' => 1,
            ],[
                'city_name' => "Ránquil", 
                'city_description' => "Ránquil",
                'city_status' => 1,
            ],[
                'city_name' => "San Carlos", 
                'city_description' => "San Carlos",
                'city_status' => 1,
            ],[//
                'city_name' => "San Fabián", 
                'city_description' => "San Fabián",
                'city_status' => 1,
            ],[
                'city_name' => "San Ignacio", 
                'city_description' => "San Ignacio",
                'city_status' => 1,
            ],[
                'city_name' => "San Nicolás", 
                'city_description' => "San Nicolás",
                'city_status' => 1,
            ],[
                'city_name' => "Treguaco", 
                'city_description' => "Treguaco",
                'city_status' => 1,
            ],[
                'city_name' => "Yungay", 
                'city_description' => "Yungay",
                'city_status' => 1,
            ]
        ]);
    }
}
