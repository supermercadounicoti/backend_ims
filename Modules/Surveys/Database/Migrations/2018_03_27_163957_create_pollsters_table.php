<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollstersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pollsters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pollster_user_id')->unsigned();
            $table->boolean('pollster_status');
            $table->timestamps();

            $table->foreign('pollster_user_id')
                ->references('id')->on('access_controls');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pollsters');
    }
}
