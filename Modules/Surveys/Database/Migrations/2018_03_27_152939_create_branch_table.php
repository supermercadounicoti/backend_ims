<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch', function (Blueprint $table) {
            $table->increments('id');
            $table->string('branch_name');
            $table->string('branch_address');
            $table->boolean('branch_status');
            $table->integer('branch_company_id')->unsigned();
            $table->integer('branch_city_id')->unsigned();
            $table->timestamps();


            $table->foreign('branch_company_id')
                ->references('id')->on('companies');
            $table->foreign('branch_city_id')
                ->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch');
    }
}
