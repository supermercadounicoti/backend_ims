<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sur_name');
            $table->integer('sur_created_by')->unsigned();
            $table->integer('sur_pollster_id')->unsigned();
            $table->integer('sur_branch_id')->unsigned();
            $table->tinyInteger('sur_status');

            $table->timestamps();

            $table->foreign('sur_pollster_id')
                ->references('id')->on('pollsters');
            $table->foreign('sur_created_by')
                ->references('id')->on('access_controls');
            $table->foreign('sur_branch_id')
                ->references('id')->on('branch');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
