<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sd_survey_id')->unsigned();
            $table->string('sd_code');
            $table->string('sd_description');
            $table->string('sd_price');
            $table->boolean('sd_status');
            $table->timestamps();

            $table->foreign('sd_survey_id')
                ->references('id')->on('surveys');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys_details');
    }
}
