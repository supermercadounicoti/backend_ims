<?php

Route::group(['middleware' => 'web', 'prefix' => 'surveys', 'namespace' => 'Modules\Surveys\Http\Controllers'], function()
{
    Route::get('/', 'SurveysController@index');

    Route::get('/pollster/list', 'PollstersController@listPollster');
    Route::get('/pollster/table/list', 'PollstersController@getPollsterList');
    Route::resource('/pollster', 'PollstersController');

    Route::get('/cities/list', 'CitiesController@getCities');
    Route::resource('/cities', 'CitiesController');

     Route::get('/code/list/{code}', 'ChangePriceController@listProductCode');

    Route::get('/companies/list', 'CompaniesController@getCompanies');
    Route::get('/companies/table/list', 'CompaniesController@getCompaniesList');
    Route::resource('/companies', 'CompaniesController');

    Route::get('/branch/company/{company_id}', 'BranchController@getBranchById');
    Route::get('/branch/list', 'BranchController@getBranch');
    Route::get('/branch/table/list', 'BranchController@getBranchList');
    Route::resource('/branch', 'BranchController');

    Route::get('/surveys/table/list', 'SurveysController@getSurveysList');
    Route::resource('/surveys', 'SurveysController');

    Route::get('/surveysDetails/list/{surveys_id}', 'SurveyDetailsController@listSurveysDetails');
    Route::get('/reports', function () {
        return view('surveys::report.index');
    });
});
