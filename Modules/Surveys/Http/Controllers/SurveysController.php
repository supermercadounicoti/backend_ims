<?php

namespace Modules\Surveys\Http\Controllers;

use Storage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Surveys\Entities\SurveysHeader;

class SurveysController extends Controller
{
    protected $isError;

    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('surveys::survey.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('surveys::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $request = $this->uploadFile($request);
            if ($this->validateHeaderExcel($request)) {
                return self::responseMessage(1068, 'The Excel does not contain the necessary columns');
            }
            $request = $this->readExcel($request);
            $transaction = SurveysHeader::store($request);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1001);
    }

    /**
     * Upload excel file
     * @param  $request
     * @return Response
     */
    public function uploadFile($request)
    {
        try {
            $img = $request->file('file');
            $request->description = $img->getClientOriginalName();
            $request->{'path'} = Storage::disk('public')->put('Surveys', $img);
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }

        return $request;
    }

    /**
     * Read Excel file
     * @param  $request
     * @return Response
     */
    public function readExcel($request)
    {
        try {
            $request->{'details'} = Excel::load('storage/'.$request->path )->get(['codigo', 'descripcion']);
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }

        return $request; 
    }

    /**
     * Validate Header of Excel
     * @param  $request
     * @return Response
     */
    public function validateHeaderExcel($request)
    {
        try {
            $this->isError = false;
            $status = Excel::load('storage/'.$request->path , function($reader) {

                $firstrow = $reader->first()->toArray();
                if (!isset($firstrow['codigo']) && !isset($firstrow['descripcion']) ) {
                    $this->isError = true;
                }
            });
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }

        return $this->isError; 
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('surveys::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('surveys::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(SurveysHeader $survey)
    {
        try {
            $transaction = SurveysHeader::changeStatus($survey);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1);
    }

    /**
     * List active surveys
     * @return Response
     */
    public function listSurveys(Request $request)
    {
        try {
            $transaction = SurveysHeader::listSurveys($request);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1, $transaction);
    }

    /**
     * List surveys
     * @return Response
     */
    public function getSurveysList()
    {
        try {
            $transaction = SurveysHeader::getSurveysList();
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return datatables()->of($transaction)->toJson();
    }
}
