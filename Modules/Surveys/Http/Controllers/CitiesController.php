<?php

namespace Modules\Surveys\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Modules\Surveys\Entities\Cities;

class CitiesController extends Controller
{
	use UtilsFromTraits;
	/**
	 * Display a listing of the resource.
	 * @return Response
	 */
	public function index()
	{
		return view('surveys::index');
	}

	/**
	 * Show the form for creating a new resource.
	 * @return Response
	 */
	public function create()
	{
		return view('surveys::create');
	}

	/**
	 * Store a newly created resource in storage.
	 * @param  Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		try {
			$transaction = Cities::store($request);
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1001);
	}

	/**
	 * Show the specified resource.
	 * @return Response
	 */
	public function show(Cities $city)
	{
		return $this->responseMessage(1001, $city);
	}

	/**
	 * Show the form for editing the specified resource.
	 * @return Response
	 */
	public function edit()
	{
		return view('surveys::edit');
	}

	/**
	 * Update the specified resource in storage.
	 * @param  Request $request
	 * @return Response
	 */
	public function update(Request $request)
	{
	}

	/**
	 * Remove the specified resource from storage.
	 * @return Response
	 */
	public function destroy()
	{
	}


	/**
	 * List all cities
	 * @return Response
	 */
	public function getCities()
	{
		$transaction = Cities::getCities();

		return $this->responseMessage(1001, $transaction);
	}
}
