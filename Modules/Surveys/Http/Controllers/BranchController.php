<?php

namespace Modules\Surveys\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Modules\Surveys\Entities\Branch;

class BranchController extends Controller
{
    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('surveys::branch.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('surveys::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $transaction = Branch::store($request);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1001);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Branch $branch)
    {
        return $this->responseMessage(1001, $branch);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('surveys::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, Branch $branch)
    {
        try {
            $transaction = Branch::updateBranch($request, $branch);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1001);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request, Branch $branch)
    {
        try {
            $transaction = Branch::changeStatus($branch);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1001);
    }

    /**
     *  get branch with companies
     * @return [type] [description]
     */
    public function getBranch()
    {
        $transaction = Branch::getBranch();

        return $this->responseMessage(1, $transaction);
    }

    /**
     *  get branch with companies
     * @return [type] [description]
     */
    public function getBranchList()
    {
        $transaction = Branch::getBranchList();

        return datatables()->of($transaction)->toJson();
    }

    /**
     *  get branch with companies
     * @return [type] [description]
     */
    public function getBranchById($company_id)
    {
        $transaction = Branch::getBranchById($company_id);

        return $this->responseMessage(1, $transaction);
    }
}
