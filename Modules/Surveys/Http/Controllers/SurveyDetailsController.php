<?php

namespace Modules\Surveys\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Modules\Surveys\Entities\SurveysHeader;
use Modules\Surveys\Entities\SurveyDetails;

class SurveyDetailsController extends Controller
{
    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('surveys::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('surveys::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('surveys::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('surveys::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * list details of surveys
     * @return Response
     */
    public function listSurveysDetails($surveys_id)
    {
        try {
            $transaction = SurveyDetails::listDetails($surveys_id);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return datatables()->of($transaction)->toJson();
    }

    /**
     * list details of surveys
     * @return Response
     */
    public function getDetails($surveys_id)
    {
        try {
            $transaction = SurveyDetails::getDetails($surveys_id);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1, $transaction);
    }

    /**
     * Enter price of product
     * @param Request $request [description]
     */
    public function EnterPriceProduct(Request $request)
    {
        try {
            $transaction = SurveyDetails::getProduct($request);
            $transaction = SurveyDetails::enterPrice($request, $transaction);
            $activeProduct = SurveyDetails::countActiveProduct($request);
            if (!$activeProduct) {
                $transaction = SurveysHeader::disabled($request);
            }
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1, $transaction);
    }
}
