<?php

namespace Modules\Surveys\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Modules\Surveys\Entities\Pollsters;

class PollstersController extends Controller
{
    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('surveys::pollster.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('surveys::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $transaction = Pollsters::store($request);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1, $transaction);  
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('surveys::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('surveys::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request, Pollsters $pollster)
    {
        try {
            $transaction = Pollsters::changeStatus($pollster);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1001);
    }

    public function listPollster()
    {
        try {
            $transaction = Pollsters::getAll();
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1, $transaction);
    }

    public function getPollsterList()
    {
        try {
            $transaction = Pollsters::getPollsterList();
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return datatables()->of($transaction)->toJson();  
    }
}
