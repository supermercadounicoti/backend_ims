<?php

namespace Modules\Surveys\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Modules\Surveys\Entities\Companies;

class CompaniesController extends Controller
{
	use UtilsFromTraits;
	/**
	 * Display a listing of the resource.
	 * @return Response
	 */
	public function index()
	{
		return view('surveys::companies.index');
	}

	/**
	 * Show the form for creating a new resource.
	 * @return Response
	 */
	public function create()
	{
		return view('surveys::create');
	}

	/**
	 * Store a newly created resource in storage.
	 * @param  Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		try {
			$transaction = Companies::store($request);
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1001);
	}

	/**
	 * Show the specified resource.
	 * @return Response
	 */
	public function show(Companies $company)
	{
		return $this->responseMessage(1001, $company);
	}

	/**
	 * Show the form for editing the specified resource.
	 * @return Response
	 */
	public function edit()
	{
		return view('surveys::edit');
	}

	/**
	 * Update the specified resource in storage.
	 * @param  Request $request
	 * @return Response
	 */
	public function update(Request $request, Companies $company)
	{
		try {
			$transaction = Companies::updateCompanies($request, $company);
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1001);
	}

	/**
	 * Remove the specified resource from storage.
	 * @return Response
	 */
	public function destroy(Request $request, Companies $company)
	{
		try {
			$transaction = Companies::changeStatus($company);
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1001);
	}

	/**
	 * List all companies
	 * @return Response
	 */
	public function getCompanies()
	{
		$transaction = Companies::getCompanies();

		return $this->responseMessage(1001, $transaction);
	}

	/**
	 * List all companies
	 * @return Response
	 */
	public function getCompaniesList()
	{
		$transaction = Companies::getCompaniesList();
		return datatables()->of($transaction)->toJson();
	}
}
