<?php

namespace Modules\Surveys\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;


class KardexPrice extends Model
{	
	use UtilsFromTraits;

    protected $fillable = [
    	'Codigo_local',
    	'Codigo',
    	'Descripcion',
    	'Precio_Neto',
    	'Descuento1',
    	'Descuento2',
    	'Descuento3',
    	'Descuento4',
    	'Flete',
    	'Imp',
    	'Precio_Compra',
    	'Margen',
    	'Fecha',
    	'Tipo',
    	'Reg'
    ];

    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sqlsrv';

    /**
     * Table name
     * @var string
     */
    protected $table = 'KARDEX_PRECIOS';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    public static function getCodePrice($code)
    {
        try {
            $result = static::where('Codigo','=',$code)->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }
}
