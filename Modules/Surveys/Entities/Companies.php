<?php

namespace Modules\Surveys\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'com_name',
    	'com_description',
    	'com_status',
    ];
    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'companies';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * Branch relationship 
     */
    public function branches()
    {
        return $this->hasMany('Modules\Surveys\Entities\Branch', 'branch_company_id');
    }


    /**
     * Create
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $query = new Companies;
                $query->com_name = $request->name;
                $query->com_description = $request->description;
                $query->com_status = 1;
                $query->push();

                DB::commit();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * get companies
     * @return Response
     */
    public static function getCompanies()
    {
        try {
            $result = static::with('branches')
                ->where('com_status', 1)
                ->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * get companies
     * @return Response
     */
    public static function getCompaniesList()
    {
        try {
            $result = static::with('branches')
                ->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * update companies
     * @return Response
     */
    public static function updateCompanies($request, $company)
    {
        try {
            $company->com_name = $request->com_name;
            $company->com_description = $request->com_description;
            $company->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $company;
    }

    /**
     * Change status
     * @return Response
     */
    public static function changeStatus($company)
    {
        try {
            $company->com_status = ($company->com_status == 1)? 0:1;
            $company->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $company;  
    }
}
