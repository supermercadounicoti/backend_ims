<?php

namespace Modules\Surveys\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'city_name',
    	'city_description',
    	'city_status',
    ];
    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    // protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'cities';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * Create
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $query = new Cities;
                $query->city_name = $request->name;
                $query->city_description = $request->description;
                $query->city_status = 1;
                $query->push();

                DB::commit();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * get cities
     * @return Response
     */
    public static function getCities()
    {
        try {
            $result = static::where('city_status', 1)
                ->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }
}
