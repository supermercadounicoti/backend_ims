<?php

namespace Modules\Surveys\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class ChangePrice extends Model
{	
	use UtilsFromTraits;

    protected $fillable = [
    	'Correlativo_Cambio',
    	'Codigo_Local',
    	'Codigo',
    	'Lista_Precio',
    	'Costo_Actual',
    	'Margen_Actual',
    	'Precio_Actual',
    	'Costo_Cambio',
    	'Margen_Cambio',
    	'Precio_Cambio',
    	'Fecha_Inicio',
    	'Hora_Inicio',
    	'Fecha_Fin',
    	'Hora_Fin',
    	'Usuario_Programa_Cambio',
    	'Usuario_Activa_Cambio',
    	'Tipo_Cambio',
    	'Codigo_Estado',
    	'hora_activacion'
    ];

    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sqlsrv';

    /**
     * Table name
     * @var string
     */
    protected $table = 'TABLA_CAMBIO_PRECIO';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    public function history(){
        return $this->belongsTo('Modules\Surveys\Entities\KardexPrice', 'Codigo');
    }

    public static function getCodePrice($code)
    {
        try {
            $result = static::where('Codigo','=',$code)->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }


}
