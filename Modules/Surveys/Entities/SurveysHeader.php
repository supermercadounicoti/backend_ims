<?php

namespace Modules\Surveys\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;
use Modules\Surveys\Entities\SurveyDetails;

class SurveysHeader extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'sur_name',
    	'sur_created_by',
    	'sur_pollster_id',
        'sur_branch_id'
    ];
    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'surveys';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * SurveysDetails relationship 
     */
    public function details()
    {
        return $this->hasMany('Modules\Surveys\Entities\SurveyDetails', 'sd_survey_id');
    }

    /**
     * Branch relationship 
     */
    public function branch(){
        return $this->belongsTo('Modules\Surveys\Entities\Branch', 'sur_branch_id');
    }

    /**
     * Pollster relationship 
     */
    public function pollster(){
        return $this->belongsTo('Modules\Surveys\Entities\Pollsters', 'sur_pollster_id');
    }
    /**
     * Create
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $query = new SurveysHeader;
                $query->sur_name = $request->sur_name;
                $query->sur_created_by = \Auth::user()->id;
                $query->sur_status = 1;
                $query->sur_pollster_id = $request->sur_pollster_id;
                $query->sur_branch_id = $request->sur_branch_id;
                $query->push();

                $request->{'survey_id'} = $query->id;
                $details = SurveyDetails::store($request);
                DB::commit();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * List all surveys of responsible
     * @param  [type] $request [description]
     * @return [type]          [description]
     */
    public static function listSurveys($request)
    {
        try {
            $result = static::with(['branch'])
                ->whereHas('pollster', function ($query) use ($request) {
                    $query->where('pollster_user_id', $request->user()->id);
                })
                ->where('sur_status', true)
                ->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Get surveys
     * @return Response
     */
    public static function getSurveysList()
    {
        try {
            $result = static::with(['branch'])
                ->join('pollsters', 'pollsters.id', '=', 'surveys.sur_pollster_id')
                ->leftjoin('access_controls', 'access_controls.id', '=', 'pollsters.pollster_user_id')
                ->leftjoin('users_data', 'users_data.id', '=', 'access_controls.user_data_id')
                ->select("pollsters.*",
                    "surveys.*",
                    DB::raw("users_data.ud_first_name + ' ' +users_data.ud_father_surname AS name")

                )
                ->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Get surveys data
     * @param  [type] $request [description]
     * @return [type]          [description]
     */
    public static function getSurvery($request)
    {
        try {
            $result = static::where('id', $request->survey_id)
                ->first();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;  
    }

    /**
     * Disabled surveys
     * @return Response
     */
    public static function disabled($request)
    {
        try {
            $result = static::getSurvery($request);
            $result->sur_status = 2;
            $result->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result; 
    }

    /**
     *  Clossing survey
     * @param  [type] $survey_id [description]
     * @return [type]            [description]
     */
    public static function changeStatus($survey)
    {
        try {
            $result = static::where('id', $survey->id)
                ->first();
            $result->sur_status = ($result->sur_status == 1)? 0:1;
            $result->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result; 
    }
}