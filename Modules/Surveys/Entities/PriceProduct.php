<?php

namespace Modules\Surveys\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class PriceProduct extends Model
{
    protected $fillable = [
    	'Codigo_Local',
    	'Codigo',
    	'Lista_Precio',
    	'Costo',
    	'Margen',
    	'Cambio_Autorizar',
    	'Reg'
    ];
     protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sqlsrv';

    /**
     * Table name
     * @var string
     */
    protected $table = 'TABLA_PRECIOS';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}
