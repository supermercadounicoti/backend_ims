<?php

namespace Modules\Surveys\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class Pollsters extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'pollster_user_id',
    	'pollster_status',
    ];
    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'pollsters';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Create
     * @param  [type] $request
     * @return [type] JSON and Slack message
    **/
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $query = new Pollsters;
                $query->pollster_user_id = $request->pollster_user_id;
                $query->pollster_status = 1;
                $query->push();

                DB::commit();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Get all pollster by status true
     * @return Response
     */
    public static function getAll()
    {
        try {
            $result = DB::transaction(function () {

                $result = static::leftjoin('access_controls', 'access_controls.id', '=', 'pollsters.pollster_user_id')
                    ->leftjoin('users_data', 'users_data.id', '=', 'access_controls.user_data_id')
                    ->select("pollsters.*", DB::raw("users_data.ud_first_name + ' ' +users_data.ud_father_surname AS name"))
                    ->where('pollsters.pollster_status', 1)
                ->get();
                return $result;

            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * get pollster list
     * @return Response
     */
    public static function getPollsterList()
    {
        try {
            $result = DB::transaction(function () {

                $result = static::leftjoin('access_controls', 'access_controls.id', '=', 'pollsters.pollster_user_id')
                    ->leftjoin('users_data', 'users_data.id', '=', 'access_controls.user_data_id')
                    ->select("pollsters.*", DB::raw("users_data.ud_first_name + ' ' +users_data.ud_father_surname AS name"))
                ->get();
                return $result;

            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }
    
    /**
     * Change status
     * @return Response
     */
    public static function changeStatus($pollster)
    {
        try {
            $pollster->pollster_status = ($pollster->pollster_status == 1)? 0:1;
            $pollster->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $pollster;  
    }
}
