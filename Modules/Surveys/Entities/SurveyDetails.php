<?php

namespace Modules\Surveys\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class SurveyDetails extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'sd_survey_id',
    	'sd_code',
    	'sd_description',
    	'sd_price',
    	'sd_status',
    ];
    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'surveys_details';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Create
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                foreach ($request->details as $key => $detail) {
                    $query = new SurveyDetails;
                    $query->sd_survey_id = $request->survey_id;
                    $query->sd_code = $detail->codigo;
                    $query->sd_description = $detail->descripcion;
                    $query->sd_price = 0;
                    $query->sd_status = 1;
                    $query->push();
                }

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * list details of surveys
     * @return Response
     */
    public static function listDetails($surveys_id)
    {
        try {
            $result = static::where([
                    [
                        'sd_survey_id', $surveys_id
                    ]
                ])
                ->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }


    /**
     * list details of surveys
     * @return Response
     */
    public static function getDetails($surveys_id)
    {
        try {
            $result = static::where([
                    [
                        'sd_survey_id', $surveys_id
                    ],[
                        'sd_status', true
                    ]
                ])
                ->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * get list of product
     * @return Response
     */
    public static function getProduct($request)
    {
        try {
            $result = static::where([
                    [
                        'sd_survey_id', $request->survey_id
                    ],[
                        'sd_status', true
                    ],[
                        'sd_code', $request->code
                    ]
                ])
                ->first();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * validate product 
     * @return Response
     */
    public static function countActiveProduct($request)
    {
        try {
            $result = static::where([
                    [
                        'sd_survey_id', $request->survey_id
                    ],[
                        'sd_status', true
                    ]
                ])
                ->count();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    public static function enterPrice($request, $transaction)
    {
        try {
            $transaction->sd_price = $request->price;
            $transaction->sd_status = false;
            $transaction->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $transaction;
    }
}
