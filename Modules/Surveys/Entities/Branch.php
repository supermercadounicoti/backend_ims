<?php

namespace Modules\Surveys\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'branch_name',
    	'branch_address',
    	'branch_status',
    	'branch_company_id',
    	'branch_city_id',
    ];
    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'branch';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the post that owns the comment.
     */
    public function companies()
    {
        return $this->belongsTo('Modules\Surveys\Entities\Companies', 'branch_company_id');
    }

    /**
     * Create
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $query = new Branch;
                $query->branch_name = $request->name;
                $query->branch_address = $request->description;
                $query->branch_company_id = $request->company_id;
                $query->branch_city_id = $request->city_id;
                $query->branch_status = 1;
                $query->push();

                DB::commit();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }


    /**
     * get branch
     * @return Response
     */
    public static function getBranch()
    {
        try {
            $result = static::with('companies')
                ->where('branch_status', 1)
                ->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * get branch
     * @return Response
     */
    public static function getBranchList()
    {
        try {
            $result = static::with('companies')
                ->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * [getBranchById description]
     * @return [type] [description]
     */
    public static function getBranchById($company_id)
    {
        try {
            $result = static::
                where([
                    [
                        'branch_status', 1
                    ],[
                        'branch_company_id', $company_id
                    ]
                ])
                ->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * update companies
     * @return Response
     */
    public static function updateBranch($request, $branch)
    {
        try {
            $branch->branch_name = $request->branch_name;
            $branch->branch_address = $request->branch_address;
            $branch->branch_company_id = $request->branch_company_id;
            $branch->branch_city_id = $request->branch_city_id;
            $branch->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $branch;
    }

    /**
     * Change status
     * @return Response
     */
    public static function changeStatus($branch)
    {
        try {
            $branch->branch_status = ($branch->branch_status == 1)? 0:1;
            $branch->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $branch;  
    }
}
