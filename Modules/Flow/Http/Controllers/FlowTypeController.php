<?php

namespace Modules\Flow\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Modules\Flow\Entities\FlowType;

class FlowTypeController extends Controller
{
    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('flow::type_flow.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('flow::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $transaction = FlowType::store($request);
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return self::responseMessage(1);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(FlowType $flow_type)
    {
        return $flow_type;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('flow::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, FlowType $flow_type)
    {
        try {
            $transaction = FlowType::updateFlowType($request, $flow_type);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1001);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(FlowType $flow_type)
    {
         try {
            $transaction = FlowType::changeStatus($flow_type);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1);
    }

    public function listData($status = 2, $mode = 0){
        try {
            if ($status == 0 || $status == 1) {
                $where = [
                    'status' => $status
                ];
            } elseif ($status == 2) {
                $where = [];
            } else {
                return self::responseMessage(1064);
            }
            $transaction = FlowType::listData($where);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return ($mode == 0)? $transaction : 
            datatables()->of($transaction)->toJson();
    }
}
