<?php

namespace Modules\Flow\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Modules\Flow\Entities\CaseFlow;

class CaseController extends Controller
{
    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('flow::cases.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('flow::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //dd($request);
        try {
            $transaction = CaseFlow::store($request);
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return self::responseMessage(1);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(CaseFlow $case)
    {
        return self::responseMessage(1, $case);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('flow::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, CaseFlow $case)
    {
        try {
            $transaction = CaseFlow::actualize($request, $case->id);
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return self::responseMessage(1);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(CaseFlow $case)
    {
        try {
            $transaction = CaseFlow::destroy($case->id);
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return self::responseMessage(1);
    }

    public function listInbox()
    {
        try {
            $transaction = CaseFlow::listInbox();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return self::responseMessage(1, $transaction);
    }

    public function countNewCases()
    {
        try {
            $transaction = CaseFlow::countNewCases();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return self::responseMessage(1, $transaction);
    }
    
    public function listTop5Cases()
    {
        try {
            $transaction = CaseFlow::listTop5Cases();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return self::responseMessage(1, $transaction);
    }
}
