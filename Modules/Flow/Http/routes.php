<?php

Route::group(['middleware' => 'web', 'prefix' => 'flow', 'namespace' => 'Modules\Flow\Http\Controllers'], function()
{
	Route::get('/', 'FlowController@index');
    Route::resource('/flow', 'FlowController');
    Route::get('/cases/list-count','CaseController@countNewCases');
    Route::get('/cases/list-top5','CaseController@listTop5Cases');
    
    Route::get('/cases/list-inbox', 'CaseController@listInbox');
    Route::resource('/cases', 'CaseController');

    Route::get('/flow-type/list', 'FlowTypeController@listData');
    Route::get('/flow-type/list/active', 'FlowTypeController@listActiveData');
    Route::get('/flow-type/list/{status?}/{mode?}', 'FlowTypeController@listData');
    Route::resource('/flow-type', 'FlowTypeController');
    Route::get('/create-case', function () {
        return view('flow::create-cases.index');
   	});
});
