<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseHasFlowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_has_flow', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('chf_case_id')->unsigned();
            $table->integer('chf_flow_id')->unsigned();
            $table->string('chf_comments')->nullable();
            $table->datetime('chf_date_init')->nullable();
            $table->datetime('chf_date_end')->nullable();
            $table->boolean('chf_approval')->nullable();
            $table->boolean('chf_status')->default(1);
            $table->timestamps();

            $table->foreign('chf_case_id')->references('id')->on('case');
            $table->foreign('chf_flow_id')->references('id')->on('flow');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case_has_flow');
    }
}
