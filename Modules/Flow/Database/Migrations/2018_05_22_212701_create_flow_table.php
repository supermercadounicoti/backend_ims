<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flow', function (Blueprint $table) {
            $table->increments('id');
            $table->string('flow_name')->nullable();
            $table->integer('flow_next')
                ->nullable()
                ->unsigned();
            $table->string('flow_description');
            $table->integer('flow_type_id')
                ->unsigned();
            $table->boolean('flow_status')
                ->default(1);
            $table->integer('flow_roles_id')
               /* ->unsigned()*/;
            $table->timestamps();

            $table->foreign('flow_type_id')
                ->references('id')->on('flow_type');

            /*$table->foreign('flow_roles_id')
                ->references('id')->on('roles');*/

            $table->foreign('flow_next')
                ->references('id')->on('flow');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flow');
    }
}
