<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('case_access_control_id');
            $table->string('case_description', 200);
            $table->boolean('case_is_readed')
                    ->default(0);
            $table->boolean('case_status')
                    ->default(1);
            $table->timestamps();
            // $table->foreign('case_access_control_id')
            //     ->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case');
    }
}
