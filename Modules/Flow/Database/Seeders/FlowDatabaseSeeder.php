<?php

namespace Modules\Flow\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class FlowDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(FlowTypeSeederTableSeeder::class);
        $this->call(FlowSeederTableSeeder::class);
        $this->call(CaseSeederTableSeeder::class);
        $this->call(CaseHasFlowSeederTableSeeder::class);
    }
}
