<?php

namespace Modules\Flow\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CaseSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Modules\Flow\Entities\CaseFlow::insert([
            [   
                'case_access_control_id' => '1',
                'case_description' => 'Primer test de caso',
               
            ],[
               'case_access_control_id' => '1',
                'case_description' => 'Primer test de caso',
            ]
        ]);
    }
}
