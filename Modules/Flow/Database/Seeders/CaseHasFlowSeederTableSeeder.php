<?php

namespace Modules\Flow\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CaseHasFlowSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Modules\Flow\Entities\CaseHasFlow::insert([
            [   
                'chf_case_id' => '1',
                'chf_flow_id' => '1',
                'chf_comments' => 'Flujo de aprobación de credito simple',
                'chf_date_init' => '2018-06-20 00:00:00',
                'chf_date_end' => null,
                'chf_approval' => null
            ]
        ]);
    }
}
