<?php

namespace Modules\Flow\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class FlowTypeSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Modules\Flow\Entities\FlowType::insert([
            [
                'f_type_name' => 'Credito Simple',
                'f_type_description' => 'Flujo de aprobación de credito simple'
            ],[
                 'f_type_name' => 'Test 1',
                'f_type_description' => 'Test 1'
            ]
        ]);
    }
}
