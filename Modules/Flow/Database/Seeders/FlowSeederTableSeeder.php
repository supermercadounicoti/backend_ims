<?php

namespace Modules\Flow\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class FlowSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   

         \Modules\Flow\Entities\Flow::insert([
            [   
                'flow_name' => 'Segunda aprobación',
                'flow_next' => null,
                'flow_description' => 'Flujo de aprobación de credito simple',
                'flow_type_id' => '1',
                'flow_roles_id' => '1'
            ],[
                'flow_name' => 'Primera aprobación',
                'flow_next' => '1',
                'flow_description' => 'Flujo de aprobación de credito simple',
                'flow_type_id' => '1',
                'flow_roles_id' => '1'
            ]
        ]);
    }
}
