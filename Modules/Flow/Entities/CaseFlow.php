<?php

namespace Modules\Flow\Entities;

use DB;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;
use Modules\Flow\Entities\CaseHasFlow;

class CaseFlow extends Model
{
	use UtilsFromTraits;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
    	'case_access_control_id',
    	'case_description',
    	'case_is_readed',
    	'case_status'
    ];

    /**
	 * Database connection
	 * @var string
	 */
	//protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'case';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ 
        'created_at', 'updated_at'
    ];

    /**
     * Date format
     * @var string
     */
	public $timestamps = true;
	//protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * The flow that belong to the case.
     */
    public function flows()
    {
        return $this->belongsToMany('Modules\Flow\Entities\Flow', 
            'case_has_flow',
            'chf_case_id',
            'chf_flow_id');
    }
    // public function flowType()
    // {
    //     return $this->belongsToMany('Modules\Flow\Entities\FlowType', 
    //         'case_has_flow',
    //         'chf_case_id',
    //         'chf_flow_id');
    // }

    /**
     * Store records
     * @param  [type] $request
     * @return object
     */
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $result = new CaseFlow;
                $result->case_access_control_id = 1;
                $result->case_description = $request->case_description;
                
                $result->push();
                DB::commit();
                $request->{"case_id"} = $result->id;
                \Modules\SimpleCredit\Entities\Clients::store($request);
                //return $result;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    public static function actualize($request, $case_id)
    {
    	try {
            $result = DB::transaction(function () use ($request, $case_id) {
                $result = CaseFlow::find($case_id);
                $result->case_description = $request->case_description;
                
                $result->push();
                
                DB::commit();

                return $result;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    /**
     * destroy records
     * @param  [type] $case_id [description]
     * @return [type]          [description]
     */
    public static function destroy($case_id)
    {
    	try {
            $result = DB::transaction(function () use ($case_id) {
                $result = CaseFlow::find($case_id);
                $result->case_status = ($result->case_status == 1)? 0:1;
                
                $result->push();
                
                DB::commit();

                return $result;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    /**
     * List Inbox recods
     * @return [type] [description]
     */
    public static function listInbox()
    {
        try {
            $result = static::where('case_is_readed', 0)
                ->whereHas('flows', function($query){
                    $query->where('flow_roles_id', 1);
                    $query->where('flow_type_id', 1);
                })
                ->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    public static function countNewCases()
    {
        try {
            $result = static::where('case_is_readed', 0)->count();
            
        } catch (Exception $e) {
            
        }
        return $result;
    }
    public static function listTop5Cases()
    {
        try {
            $result = static::with(['flows' => function ($query) {
                                        $query
                                        ->leftjoin('flow_type', 'flow_type.id', '=', 'flow.flow_type_id')
                                        ->select('flow.*', 'flow_type.f_type_name as flow_type');
                                    }])
            ->where('case_is_readed', 0)->take(5)->get();
             
        } catch (Exception $e) {
            
        }
        return $result;
    }
}
