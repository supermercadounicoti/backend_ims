<?php

namespace Modules\Flow\Entities;

use Illuminate\Database\Eloquent\Model;

class CaseHasFlow extends Model
{
    protected $fillable = [
    	'chf_case_id',
    	'chf_flow_id'
    ];

    /**
	 * Database connection
	 * @var string
	 */
	//protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'case_has_flow';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * Store records
     * @param  [type] $request
     * @return object
     */
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $result = new CaseHasFlow;
                $result->case_access_control_id = 1;
                $result->case_description = $request->case_description;
                
                $result->push();
                
                DB::commit();
                CaseHasFlow::store($result->id);
                //return $result;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }


}
