<?php

namespace Modules\Flow\Entities;

use DB;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class flow extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'flow_name',
    	'flow_next',
    	'flow_description',
    	'flow_type_id',
    	'flow_status',
    	'flow_roles_id'
    ];

    /**
	 * Database connection
	 * @var string
	 */
	//protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'flow';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * Date format
     * @var string
     */
    public $timestamps = true;
    protected $dateFormat = 'Y-m-d H:i:00';

    /**
     * Get the flow type for the flow.
     */
    public function flowType()
    {
        return $this->belongsTo('Modules\Flow\Entities\FlowType');
    }

    /**
     * Store new records
     * @return [type] [description]
     */
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $result = new flow;
                $result->flow_next = $request->flow_next;
                $result->flow_description = $request->flow_description;
                $result->flow_type_id = $request->flow_type_id;
                $result->flow_roles_id = $request->flow_roles_id;
                
                $result->push();
                
                DB::commit();
                
                return $result;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    public static function actualize($request, $flow_id)
    {
        try {
            $result = DB::transaction(function () use ($request, $flow_id) {
                $result = flow::find($flow_id);
                $result->flow_next = $request->flow_next;
                $result->flow_description = $request->flow_description;
                $result->flow_type_id = $request->flow_type_id;
                $result->flow_roles_id = $request->flow_roles_id;
                
                $result->push();
                
                DB::commit();

                return $result;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    /**
     *  Change status
     * @param  [type] $case_id [description]
     * @return [type]          [description]
     */
    public static function destroy($flow_id)
    {
        try {
            $result = DB::transaction(function () use ($flow_id) {
                $result = Flow::find($flow_id);
                $result->flow_status = ($result->flow_status == 1)? 0:1;
                
                $result->push();
                
                DB::commit();

                return $result;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }
}
