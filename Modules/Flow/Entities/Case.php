<?php

namespace Modules\Flow\Entities;

use Illuminate\Database\Eloquent\Model;

class Case extends Model
{
    protected $fillable = [
    	'case_access_control_id',
    	'case_description',
    	'case_is_readed',
    	'case_status'
    ];

    /**
	 * Database connection
	 * @var string
	 */
	protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'case';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $result = new Case;
                $result->case_access_control_id = $request->ud_run;
                $result->case_description = $request->ud_first_name;
                
                $result->push();
                
                DB::commit();

                return $result;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }
}
