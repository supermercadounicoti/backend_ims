<?php

namespace Modules\Flow\Entities;

use DB;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class FlowType extends Model
{
    use UtilsFromTraits;
    protected $fillable = [
    	'f_type_name',
    	'f_type_description',
    	'f_type_status'
    ];

    /**
	 * Database connection
	 * @var string
	 */
	//protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'flow_type';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * Date format
     * @var string
     */
    public $timestamps = true;
    //protected $dateFormat = 'Y-m-d H:i:00';

    /**
     * Get the flow for the flow type.
     */
    public function flow()
    {
        return $this->hasMany('Modules\Flow\Entities\FlowType');
    }

    /**
     * Store new records
     * @return [type] [description]
     */
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $result = new flowType;
                $result->f_type_name = $request->f_type_name;
                $result->f_type_description = $request->f_type_description;
                
                $result->push();
                
                DB::commit();

                return $result;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

     /**
     * Change status
     * @param  [type] $access_control [description]
     * @return [type]                 [description]
     */
    public static function changeStatus($model)
    {
        try {
            $model->f_type_status = ($model->f_type_status == 1)? 0:1;
            $model->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $model;
    }

    /**
     * list data
     * @return [object]
     */
    public static function listData($where)
    {
        try {
            $result = static::where($where)
                ->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    public static function updateFlowType($request, $flowType)
    {
        try {
            $flowType->f_type_name = $request->f_type_name;
            $flowType->f_type_description = $request->f_type_description;
            $flowType->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $flowType;
    }
}
