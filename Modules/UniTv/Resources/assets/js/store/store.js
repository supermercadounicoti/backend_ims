import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


const state = {
  count: 0,
  fresh_id : 0,
  statuss : 0

}

const mutations = {
  increment (state) {
    state.count++
  },
  decrement (state) {
    state.count--
  },
  cleanFreshId(state) {
    state.fresh_id = 0
  },
  setFreshId(state, id) {
    state.fresh_id = id
  },
  setStatuss(state){
    state.statuss = 1
  },
  cleanStatuss(state) {
    state.statuss = 0
  },


}

const actions = {
  cleanFreshId: ({ commit }) => commit('cleanFreshId'),
  cleanStatuss: ({ commit }) => commit('cleanStatuss'),
  setFreshId: ({ commit }, id) => commit('setFreshId', id),
  setStatuss: ({ commit }) => commit('setStatuss'),
  increment: ({ commit }) => commit('increment'),
  decrement: ({ commit }) => commit('decrement'),
  incrementIfOdd ({ commit, state }) {
    if ((state.count + 1) % 2 === 0) {
      commit('increment')

    }
  },
  incrementAsync ({ commit }) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        commit('increment')
        resolve()
      }, 1000)
    })
  }
}

const getters = {
  // evenOrOdd: state => state.count % 2 === 0 ? 'even' : 'odd',
  getid: state => state.fresh_id,
  getstatus: state => state.statuss

}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
