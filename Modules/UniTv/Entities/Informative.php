<?php

namespace Modules\UniTv\Entities;

use DB;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;
use Modules\UniTv\Entities\SupermarketHasInformative;

class Informative extends Model
{
	use UtilsFromTraits;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'path',
        'description',
        'init_date',
        'end_date',
        'status',
        'force_disable',
        'type_id'
    ];
    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'informatives';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the supermarket for offer.
     */
    public function supermarkets()
    {
        return $this->belongsToMany('Modules\UniTv\Entities\Supermarket',
                'supermarket_has_informatives');
    }

    /**
     * Get the type of Offer
     */
    public function type()
    {
        return $this->belongsTo('Modules\UniTv\Entities\Type');
    }

    /**
     * Create a new offer
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function store($request)
    {
		try {
			$result = DB::transaction(function () use ($request) {
				$informative = new Informative;
				$informative->path = $request->path;
				$informative->description = $request->description;
                $informative->init_date = $request->init_date;
                $informative->end_date = $request->end_date;
                $informative->status = self::validateStatus($request->init_date, $request->end_date);
				$informative->type_id = $request->type_id;

				$informative->push();

                $supermarketWithOffer = SupermarketHasInformative::createSupermarketWithOffer($request->supermarkets, $informative->id);

				DB::commit();

                return $informative;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}

        return $result;
    }

    /**
     * List Only Offer by id
     * @param  [Object] $offer
     * @return [Json] Message
     */
    public static function listDetails($informative)
    {
    	try {
    		$result = DB::transaction(function () use ($informative) {
				$result = Informative::where('id',$informative->id)
					->get()[0];

				return $result;

			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
        return $result;
    }


    /**
     * List Only informative by id
     * @param  [Object] $informative_id
     * @return [Json] Message
     */
    public static function filterById($informative_id)
    {
        try {
            $result = DB::transaction(function () use ($informative_id) {
                $result = Informative::where('id',$informative_id)
                    ->get()[0];

                return $result;

            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Filter offer by end date
     * @param  [Datetime] $endDate
     * @return [type] [description]
     */
    public static function filterByEndDate()
    {
        $currentDate = Carbon::now()->subHours(3)->subDay()->toDateTimeString();
        echo("initializing the process: $currentDate\n\n");
        try {
            $result = DB::transaction(function () use ($currentDate) {
                $result = Informative::where([
                        [
                            'end_date', '>=',$currentDate
                        ],[
                           'force_disable', '<>', 1
                        ]
                    ])
                    ->get();
                return $result;

            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Update Offer
     * @param  [Object] Offer   [Model]
     * @param  [Request] $request
     * @return [Json] Message
     */
    public static function updateInformative($informative, $request)
    {
    	try {
			$result = DB::transaction(function () use ($informative, $request) {
				$informative = Informative::find($informative->id);
				$informative->init_date = $request->init_date;
				$informative->end_date = $request->end_date;

				$informative->push();

                 /*$supermarketWithOffer = SupermarketHasInformative::updateSupermarketWithInformative($request->supermarkets, $request->id);*/
				DB::commit();
			});
			return self::responseMessage(1, $result);
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
    }

    /**
     * Update Status Offer
     * @param  [Object] Offer   [Model]
     * @param  [Request] $request
     * @return [Json] Message
     */
    public static function updateStatus($offer, $status)
    {
        try {
            $result = DB::transaction(function () use ($offer, $status) {
                $offer = Informative::find($offer->id);
                $offer->status = $status;

                $offer->push();

                DB::commit();
            });
            return self::responseMessage(1, $result);
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
    }

    /**
     * Destroy offer
     * @param  [Object] Offer
     * @return [Json] Message
     */
    public static function destroy($informative)
    {
    	try {
			$result = DB::transaction(function () use ($informative) {
				$informative = Informative::find($informative->id);
                $informative->status = ($informative->status == 1)? 0:1;
                $informative->push();

                DB::commit();
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
        return self::responseMessage(1, $result);
    }

    /**
     * List All offer
     * @return [Json] Message
     */
    public static function listAllInformatives($type_id)
    {
    	try {
    		$result = DB::transaction(function () use ($type_id) {
				$result = Informative::with('supermarkets')
                    ->where('type_id', '=',$type_id)
                    ->get(['id', 'path', 'description', 'type_id', 'init_date', 'end_date']);
				return $result;

			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}

        return $result;
    }


    /**
     * Validate if employee exists
     * in the month in the supermarket
     * @return [Object] $request
     */
    public static function existsEmployee($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $result = Informative::whereHas('supermarkets', function ($query) use($request) {
                        $query->where('supermarket_id', '=', $request->supermarkets[0]);
                    })->where([
                        [
                            'init_date', '=', $request->init_date
                        ], [
                            'end_date', '=', $request->end_date
                        ], [
                            'type_id', '=', 3
                        ], [
                            'force_disable', '<>', 1
                        ]
                    ])->exists();
                return $result;

            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Validate if employee is activate
     * in the month in the supermarket
     * @return [Object] $request
     */
    public static function verifyChangeStatus($supermarket_id, $informative)
    {
        try {
            $result = DB::transaction(function () use ($supermarket_id, $informative) {
                $result = Informative::whereHas('supermarkets', function ($query) use($supermarket_id ,$informative) {
                        $query->where('supermarket_id', '=', $supermarket_id);
                    })->where([
                        [
                            'init_date', '=', $informative->init_date
                        ], [
                            'end_date', '=', $informative->end_date
                        ], [
                            'type_id', '=', 3
                        ], [
                            'force_disable', '=', 0
                        ]
                    ])->exists();
                return $result;

            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Validate if is necessary send offer to TV
     * @param  [string] $initDate
     * @param  [string] $endDate
     * @return [int] status and message to Node JS
     */
    public static function validateStatus($initDate, $endDate)
    {
        $currentDate = Carbon::now()->subHours(3);
        $initDate = Carbon::parse($initDate);
        $endDate = Carbon::parse($endDate);
        $status = false;

        if ($initDate <= $currentDate and $endDate >= $currentDate) {
            $status = true;
        } elseif ($initDate <= $currentDate and $endDate <= $currentDate) {
            $status = false;
        } elseif ($initDate >= $currentDate and $endDate >= $currentDate) {
            $status = false;
        } elseif ($initDate >= $currentDate and $endDate <= $currentDate) {
            $status = false;
        }
        return $status;
    }

    /**
     * Update Force change status
     * @return [Int] $informative_id
     */
    public static function forceChangeStatus($informative_id)
    {
        try {
            $result = DB::transaction(function () use ($informative_id) {
                $informatives = Informative::find($informative_id);
                $informatives->force_disable = ($informatives->force_disable == 1)? 0:1;
                $informatives->status = 0;

                $informatives->push();

                DB::commit();

                return $informatives;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }


		public static function listAllInformativesStatus($type_id)
    {
    	try {
    		$result = DB::transaction(function () use ($type_id) {
				$result = Informative::with('supermarkets')
                    ->where('type_id', '=',$type_id)
										->where('status','=',"1")
                    ->get();
				return $result;

			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}

        return $result;
    }

		public static function listCounterActive($type_id, $supermarket_id, $status)
    {
    	try {
    		$result = DB::transaction(function () use ($type_id, $supermarket_id, $status) {
				$result = Informative::whereHas('supermarkets', function ($query) use($type_id, $supermarket_id, $status) {
								$query->where('supermarket_id', '=', $supermarket_id);
						})->where('type_id', '=',$type_id)->where('status','=',$status)->count();
				return $result;

			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}

        return $result;
    }



}
