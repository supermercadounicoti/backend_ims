<?php

namespace Modules\UniTv\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class SupermarketHasInformative extends Model
{
    use UtilsFromTraits;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'supermarket_id',
        'informative_id'
    ];
    
    protected $dateFormat = 'Y-m-d H:i:00';
    
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'supermarket_has_informatives';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * create supermarket association with offer
     * @param  [type] $supermarkets
     * @param  [type] $offerId
     */
    public static function createSupermarketWithOffer($supermarkets, $offerId)
    {
    	try {
			$result = DB::transaction(function () use ($supermarkets, $offerId) {
				foreach ($supermarkets as $id) {
					$supermarketWihtInfo = new SupermarketHasInformative;
					$supermarketWihtInfo->supermarket_id = $id;
					$supermarketWihtInfo->informative_id = $offerId;

					$supermarketWihtInfo->push();
				}

				DB::commit();
			});
			return self::responseMessage(1, $result);
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
    }

    public static function updateSupermarketWithInformative($supermarkets, $informative_id)
    {
        try {
            $result = DB::transaction(function () use ($supermarkets, $offerId) {
                foreach ($supermarkets as $id) {
                    $supermarketWihtInfo = SupermarketHasInformative::where([
                            [
                                'supermarket_id', $id
                            ], [
                                'informative_id', $informative_id
                            ]
                        ])->first();
                    $supermarketWihtInfo->supermarket_id = $id;
                    $supermarketWihtInfo->informative_id = $offerId;

                    $supermarketWihtInfo->push();
                }

                DB::commit();
            });
            return self::responseMessage(1, $result);
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
    }
}
