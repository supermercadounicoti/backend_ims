<?php

namespace Modules\UniTv\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class ContacBook extends Model
{
	use UtilsFromTraits;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'second_name',
        'father_surname',
        'mother_surname',
        'department',
        'phone',
        'cellphone',
    ];
    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'contact_book';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * get content book
     * @return Response
     */
    public static function getContactBook()
    {
        try {
            $result = DB::transaction(function () {
                $result = static::get();

                return $result;

            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }
}
