<?php

namespace Modules\UniTv\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;
use Modules\UniTv\Entities\SupermarketHasVideo;

class Video extends Model
{
    use UtilsFromTraits;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'url',
        'status'
    ];
    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'videos';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the supermarkets for videos.
     */
    public function supermarkets()
    {
        return $this->belongsToMany(
                'Modules\UniTv\Entities\Supermarket', 
                'supermarket_has_videos');
    }

    /**
     * Create a new video
     * @param  [type] $request 
     * @return [type] JSON and Slack message
     */
    public static function create($request)
    {
    	try {
            $result = DB::transaction(function () use ($request) {
                $video = new Video;
                $video->name = $request->name;
                $video->url = $request->url;
                $video->status = 1;

                $video->push();

                $supermarketWithVideo = SupermarketHasVideo::createSupermarketWithVideo($request->supermarkets, $video->id);

                DB::commit();
            });
            return self::responseMessage(1, $result);
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
    }

    /**
     * List Only video by id
     * @param  [Object] $video
     * @return [Json] Message
     */
    public static function show($model)
    {
        try {
            $result = DB::transaction(function () use ($model) {
                $result = Video::where('id',$model->id)
                    ->get()[0];

                return $result;

            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Update video
     * @param  [Object] Offer   [Model]
     * @param  [Request] $request
     * @return [Json] Message
     */
    public static function updateVideo($model, $request)
    {
        try {
            $result = DB::transaction(function () use ($model, $request) {
                $video = Video::find($model->id);
                $video->name = $request->name;
                $video->url = $request->url;

                $video->push();

                DB::commit();
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1, $result);
    }


    /**
     * Change Status
     * @param  [Object] Offer
     * @return [Json] Message
     */
    public static function destroy($model)
    {
        try {
            $result = DB::transaction(function () use ($model) {
                $result = Video::find($model->id);
                $result->status = ($result->status == 1)? 0:1;
                $result->push();

                DB::commit();

            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1, $result);
    }

    /**
     * List All videos by supermarket 
     * @return [Json] Message
     */
    public static function listBySupermarket($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $result = Video::with(['supermarkets' => function($query) {
                        $query->where('supermarket_id', 1);
                    }])
                    ->where('status', 1)->get();
                return $result;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * List All videos 
     * @return [Json] Message
     */
    public static function listAll()
    {
        try {
            $result = DB::transaction(function () {
                $result = Video::with('supermarkets')
                    ->get(['id', 'name', 'url', 'status']);
                return $result;

            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }
}
