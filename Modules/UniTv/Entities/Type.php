<?php

namespace Modules\UniTv\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use UtilsFromTraits;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];

    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'types';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the offer for the supermarkets.
     */
    public function informatives()
    {
        return $this->hasMany('Modules\UniTv\Entities\Informatives');
    }

    /**
     * Create a new offer
     * @param  [type] $request 
     * @return [type] JSON and Slack message
     */
    public static function createTypeOf($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $supermarket = new Type;
                $supermarket->name = $request->name;
                $supermarket->description = $request->description;

                $supermarket->push();

                DB::commit();
            });
            return self::responseMessage(1, $result);
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
    }

    /**
     * List Only Offer by id
     * @param  [Object] $offer
     * @return [Json] Message
     */
    public static function listTypeOf($type)
    {
        try {
            $result = DB::transaction(function () use ($type) {
                $result = Type::where('id',$type->id)
                    ->get()[0];

                return $result;

            });
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
    }

    /**
     * Update Offer
     * @param  [Object] Offer   [Model]
     * @param  [Request] $request
     * @return [Json] Message
     */
    public static function updateTypeOf(Type $type)
    {
        try {
            $result = DB::transaction(function () use ($type, $request) {
                $typeOfOffer = Type::find($typeOfOffer->id);
                $typeOfOffer->name = $request->name;
                $typeOfOffer->description = $request->description;

                $typeOfOffer->push();

                DB::commit();
            });
            return self::responseMessage(1, $result);
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
    }

    /**
     * Destroy offer
     * @param  [Object] Offer
     * @return [Json] Message
     */
    public static function destroyTypeOf($type)
    {
        try {
            $result = DB::transaction(function () use ($type) {
                $typeOfOffer = Type::destroy($typeOfOffer->id);
            });
            return self::responseMessage(1, $result);
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
    }

    /**
     * List All offer 
     * @return [Json] Message
     */
    public static function listAllTypeOf()
    {
        try {
            $result = DB::transaction(function () {
                $result = Type::get();

                return $result;

            });
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
    }
}
