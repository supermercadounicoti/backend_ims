<?php

namespace Modules\UniTv\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class Supermarket extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'status'
    ];
    
    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'supermarkets';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

	/**
	 * Get the offer for the supermarkets.
     */
	public function offers()
	{
        return $this->belongsToMany(
                'Modules\UniTv\Entities\Offer',
                'supermarket_has_offers');
	}

    /**
     * Get the videos for the supermarkets.
     */
    public function videos()
    {
        return $this->belongsToMany(
                'Modules\UniTv\Entities\Videos',
                'supermarket_has_videos');
    }

    /**
     * Create a new offer
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function createSupermarket($request)
    {
		try {
			$result = DB::transaction(function () use ($request) {
				$supermarket = new Supermarket;
				$supermarket->name = $request->name;
				$supermarket->description = $request->description;
                $supermarket->status = true;

				$supermarket->push();

				DB::commit();
			});
			return self::responseMessage(1, $result);
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
    }

    /**
     * List Only Offer by id
     * @param  [Object] $offer
     * @return [Json] Message
     */
    public static function listSupermarket($supermarket)
    {
    	try {
    		$result = DB::transaction(function () use ($supermarket) {
				$result = Supermarket::where('id',$supermarket->id)
					->get()[0];

				return $result;

			});
			return $result;
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
    }

    /**
     * Update Offer
     * @param  [Object] Offer   [Model]
     * @param  [Request] $request
     * @return [Json] Message
     */
    public static function updateSupermarket($supermarket, $request)
    {
    	try {
			$result = DB::transaction(function () use ($supermarket, $request) {
				$supermarket = Supermarket::find($supermarket->id);
				$supermarket->name = $request->name;
				$supermarket->description = $request->description;

				$supermarket->push();

				DB::commit();
			});
			return self::responseMessage(1, $result);
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
    }

    /**
     * Destroy offer
     * @param  [Object] Offer
     * @return [Json] Message
     */
    public static function destroySupermarket($supermarket)
    {
    	try {
			$result = DB::transaction(function () use ($supermarket) {
				$supermarket = Supermarket::find($supermarket->id);
                $supermarket->status = ($supermarket->status == 1)? 0:1;
                $supermarket->push();

                DB::commit();
			});
			return self::responseMessage(1, $result);
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
    }

    /**
     * List All offer
     * @return [Json] Message
     */
    public static function listAllSupermarkets()
    {
    	try {
    		$result = DB::transaction(function () {
				$result = Supermarket::get();
				return $result;

			});
			return $result;
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
    }

    /**
     * Charge Checkbox
     * @return [Json] Message
     */
    public static function listActive()
    {
        try {
            $result = DB::transaction(function () {
                $result = Supermarket::where('status', true)
                    ->get();
                return $result;

            });
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
    }

}
