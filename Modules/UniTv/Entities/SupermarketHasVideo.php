<?php

namespace Modules\UniTv\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class SupermarketHasVideo extends Model
{
    use UtilsFromTraits;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'supermarket_id',
        'video_id'
    ];

    protected $dateFormat = 'Y-m-d H:i:00';
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sgi';

    /**
     * Table name
     * @var string
     */
    protected $table = 'supermarket_has_videos';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * create supermarket association with video
     * @param  [array] $supermarkets
     * @param  [type] $video_id
     */
    public static function createSupermarketWithVideo($supermarkets, $video_id)
    {
    	try {
			$result = DB::transaction(function () use ($supermarkets, $video_id) {
				foreach ($supermarkets as $id) {
					$supermarketWihtVideo = new SupermarketHasVideo;
					$supermarketWihtVideo->supermarket_id = $id;
					$supermarketWihtVideo->video_id = $video_id;

					$supermarketWihtVideo->push();
				}

				DB::commit();
			});
			return self::responseMessage(1, $result);
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
    }

}
