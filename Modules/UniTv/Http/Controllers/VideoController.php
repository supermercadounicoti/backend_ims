<?php

namespace Modules\UniTv\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\UniTv\Entities\Video;

class VideoController extends Controller
{
    use UtilsFromTraits;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('unitv::maintainers.video.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unitv::maintainers.video.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try {
            $result = Video::create($request);
            return $this->responseMessage(1, 'The record was created');
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        $result = Video::show($video);

        return $this->responseMessage(1, $result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Construction  $Construction
     * @return \Illuminate\Http\Response
     */
    public function edit(Supermarket $supermarket)
    {
        return view('unitv::maintainers.video.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function update(Video $video, Request $request)
    {
        try {  
            $result = Video::updateVideo($video, $request);
            return $this->responseMessage(1, 'The record was update');
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        try {  
            $result = Video::destroy($video);
            
            return $this->responseMessage(1, 'The record was changed status');
        
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * List all videos from supermarket
     * @return [type] [description]
     */
    public function listAll()
    {
        $result = Video::listAll();
        return datatables()->of($result)->toJson();
    }

    /**
     * List all videos from supermarket
     * @return [type] [description]
     */
    public function listBySupermarket($supermarket_id)
    {
        $result = Video::listBySupermarket($supermarket_id);

        return $result;
    }

}
