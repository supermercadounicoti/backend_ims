<?php

namespace Modules\UniTv\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\UniTv\Entities\Supermarket;

class SupermarketController extends Controller
{
    use UtilsFromTraits;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('unitv::maintainers.supermarket.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unitv::maintainers.supermarket.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try {
            $result = Supermarket::createSupermarket($request);
            return $this->responseMessage(1, 'The record was created');
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function show(Supermarket $supermarket)
    {
        $result = Supermarket::listSupermarket($supermarket);

        return $this->responseMessage(1, $result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Construction  $Construction
     * @return \Illuminate\Http\Response
     */
    public function edit(Supermarket $supermarket)
    {
        return view('unitv::maintainers.supermarket.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function update(Supermarket $supermarket, Request $request)
    {
        try {  
            $result = Supermarket::updateSupermarket($supermarket, $request);
            return $this->responseMessage(1, 'The record was update');
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supermarket $supermarket)
    {
        try {  
            $result = Supermarket::destroySupermarket($supermarket);
            
            return $this->responseMessage(1, 'The record was changed status');
        
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * List all offers that exists
     * @return [type] [description]
     */
    public function listAllSupermarkets()
    {
        $result = Supermarket::listAllSupermarkets();

        return datatables()->of($result)->toJson();
    }

    /**
     * List all supermarket that is active
     * @return [type] [description]
     */
    public function listActive()
    {
        $result = Supermarket::listActive();
        return $result;
    }

}
