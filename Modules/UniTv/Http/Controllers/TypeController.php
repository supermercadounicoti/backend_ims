<?php

namespace Modules\UniTv\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\UniTv\Entities\Type;

class TypeController extends Controller
{

    use UtilsFromTraits;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('unitv::maintainers.type_case.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unitv::maintainers.type_case.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try {
            $result = Type::createTypeOf($request);
            return $this->responseMessage(1, 'The record was created');
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        $result = Type::listTypeOf($type);

        return $this->responseMessage(1, $result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Construction  $Construction
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        return view('unitv::maintainers.type_case.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function update(Type $type, Request $request)
    {
        try {  
            $result = Type::updateTypeOf($typeOfOffer, $request);
            return $this->responseMessage(1, 'The record was update');
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        try {  
            $result = Type::destroyTypeOf($typeOfOffer);
            
            return $this->responseMessage(1, 'The record was destroyed');
        
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * List all offers that exists
     * @return [type] [description]
     */
    public function listAllTypeOf()
    {
        $result = Type::listAllTypeOf();

        return datatables()->of($result)->toJson();
    }

}
