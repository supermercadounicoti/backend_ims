<?php

namespace Modules\UniTv\Http\Controllers;

use DB;
use Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\UniTv\Entities\Informative;

class InformativeController extends Controller
{
    use UtilsFromTraits;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('unitv::maintainers.slider.index');
    }

    /**
     * Display the index page of employee
     * @return \Illuminate\Http\Response
     */
    public function employeeIndex()
    {
        return view('unitv::maintainers.employees.index');
    }

    public function productIndex()
    {
        return view('unitv::maintainers.product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('interactive.offer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try {
            $request = $this->uploadFile($request);
            $request->supermarkets = explode(',', $request->supermarkets);
            $result = Informative::store($request);
            if ($result->status) {
                $this->sendPackageNode($result);
            }
            return $this->responseMessage(1, 'The record was created');
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Upload file to server
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function uploadFile($request)
    {
        try {
            $img = $request->file('file');
            $request->description = $img->getClientOriginalName();
            if ($request->type_id == 1) {
                $request->{'path'} = Storage::disk('public')->put('Slider', $img);
            } elseif ($request->type_id == 2) {
                $request->{'path'} = Storage::disk('public')->put('Products', $img);
            } else {
                $request->{'path'} = Storage::disk('public')->put('Employee', $img);
            }
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }

        return $request;
    }

    /**
     * Store a newly created resource in storage
     * Employee of the month
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeEmployee(Request $request)
    {
        try {
            $request->init_date = Carbon::parse('01-' . $request->init_date)
                ->startOfMonth()->format('Y-m-d');
            $request->end_date = Carbon::parse('01-' . $request->end_date)
                ->endOfMonth()->format('Y-m-d');
            $request = $this->uploadFile($request);
            $request->supermarkets = explode(',', $request->supermarkets);
            if (!$this->validateEmployeeByMonth($request)) {
                $result = Informative::store($request);
                if ($result) {
                    $this->sendPackageNode($result);
                }
            } else {
                return $this->responseMessage(2, 'The record for the month entered already exists');
            }
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $this->responseMessage(1, 'The record was created');
    }

    /**
     * Validate that employee exists
     * @return [type] [description]
     */
    public function validateEmployeeByMonth($request)
    {
        try {
            $result =  Informative::existsEmployee($request);
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function show(Informative $informative)
    {
        $result = Informative::listDetails($informative);

        return $this->responseMessage(1, $result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Construction  $Construction
     * @return \Illuminate\Http\Response
     */
    public function edit(Informative $informative)
    {
        return view('interactive.offer.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function update(Informative $informative, Request $request)
    {
        try {
            $result = Informative::updateInformative($informative, $request);
            return $this->responseMessage(1, 'The record was update');
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function destroy(Informative $informative)
    {
        try {
            $result = Informative::destroy($informative);

            return $this->responseMessage(1, 'The record was changed status');

        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * List all offers that exists
     * @return [type] [description]
     */
    public function listAllInformatives($type_id)
    {
        $result = Informative::listAllInformatives($type_id);

        return datatables()->of($result)->toJson();
    }

    /**
     * Force change status
     * @return
     */
    public function forceChangeStatus($informative_id, $type_id = 0)
    {
        try {
            if ($type_id != 0) {
                $informative = Informative::filterById($informative_id);
                if ($informative->force_disable) {
                    $supermarket_id = $informative->supermarkets[0]['id'];
                    if (Informative::verifyChangeStatus($supermarket_id, $informative)) {
                        return $this->responseMessage(2, 'You can not activate 2 employees in the month');
                    }
                }
            }

            $result = Informative::forceChangeStatus($informative_id);
            if ($result->force_disable) {
                $this->sendPackageNode($result, 1);
            }
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $this->responseMessage(1, 'The record was updated');
    }


    /**
     * check if the offer must be enabled
     * @return [type] [description]
     */
    public static function checkStatusOffer()
    {
        try {
            $offers = Informative::filterByEndDate();
            foreach ($offers as $offer) {
                $status = Informative::validateStatus($offer->init_date, $offer->end_date);
                if ($status and $offer->status != true) {
                    Informative::updateStatus($offer, $status);
                    self::sendPackageNode($offer);
                } elseif ($status == false and $offer->status == true) {
                    Informative::updateStatus($offer, $status);
                    self::sendPackageNode($offer, 1);
                }
            }
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1, 'Revised offers');
    }

    public function listAllInformativesStatus($type_id)
    {
        $result = Informative::listAllInformativesStatus($type_id);

       return datatables()->of($result)->toJson();
    }

    public function listCounterActive($type_id, $supermarket_id, $status)
    {
        $result = Informative::listCounterActive($type_id, $supermarket_id, $status);

       return $result;
    }

  
}
