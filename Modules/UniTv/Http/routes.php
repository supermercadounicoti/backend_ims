<?php
Route::group(['middleware' => ['web'], 'prefix' => 'unitv', 'namespace' => 'Modules\UniTv\Http\Controllers'], function()
{
  Route::get('/', function(){
		return view('unitv::dashboard');
	});

	Route::get('/fire', function () {
	    // this fires the event
	    $data = [
	    	'event' => 'SendPackageEvent',
	    	'data' => [
	    		'id' => '2',
	    		'name' => 'Nacimiento'
	    	]
	    ];
	    Redis::publish('sendPackage', json_encode($data));
	    //Redis::publish('nacimiento', json_encode($data));

	    return 'Done';
	});

	Route::get('/test', function(){
		return view('unitv::unitv');
	});
  	Route::get('/listdasboard/{type_id}', 'InformativeController@listAllInformativesStatus');
  	Route::get('/counteractive/{type_id}/{supermarket_id}/{status}', 'InformativeController@listCounterActive');
	Route::get('/informative/list/{type_id}', 'InformativeController@listAllInformatives');
	Route::get('/informative/employee', 'InformativeController@employeeIndex');
	Route::post('/informative/employee', 'InformativeController@storeEmployee');
	Route::get('/informative/product', 'InformativeController@productIndex');
	Route::patch('/informative/forceStatus/{informative_id}/{type_id?}', 'InformativeController@forceChangeStatus');
	Route::resource('/informative','InformativeController');

	Route::get('/supermarket/list', 'SupermarketController@listAllSupermarkets');
	Route::get('/supermarket/active', 'SupermarketController@listActive');
	Route::resource('/supermarket','SupermarketController');

	Route::get('/type/list', 'TypeController@listAllTypeOf');
	Route::resource('/type','TypeController');

	Route::get('/video/list', 'VideoController@listAll');
	Route::get('/video/supermarkets/{supermarket_id}', 'VideoController@listBySupermarket');
	Route::resource('/video','VideoController');

	Route::get('/contact-book/table/list', 'ContacBookController@getContactBook');
});
