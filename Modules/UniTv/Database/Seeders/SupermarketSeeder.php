<?php
namespace Modules\UniTv\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\UniTv\Entities\Supermarket;

class SupermarketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Supermarket::insert([
			[
                'name' => 'Único Angol',
                'description' => 'Angol',
                'status' => true,
                'created_at' => '2018-02-07 14:18:00',
                'updated_at' => '2018-02-07 14:18:00',
            ],[
                'name' => 'Único Laja',
                'description' => 'Laja',
                'status' => true,
                'created_at' => '2018-02-07 14:18:00',
                'updated_at' => '2018-02-07 14:18:00',
            ],[
                'name' => 'Único Los Ángeles',
                'description' => 'Los Ángeles',
                'status' => true,
                'created_at' => '2018-02-07 14:18:00',
                'updated_at' => '2018-02-07 14:18:00',
            ],[
                'name' => 'Único Mulchén',
                'description' => 'Mulchén',
                'status' => true,
                'created_at' => '2018-02-07 14:18:00',
                'updated_at' => '2018-02-07 14:18:00',
            ]
            ,[
                'name' => 'Único Santa Bárbara',
                'description' => 'Santa Bárbara',
                'status' => true,
                'created_at' => '2018-02-07 14:18:00',
                'updated_at' => '2018-02-07 14:18:00',
            ]
		]);
    }
}
