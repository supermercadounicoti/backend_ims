<?php

namespace Modules\UniTv\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UniTvDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(SupermarketSeeder::class);
        $this->call(TypeOfSeeder::class);
        // $this->call("OthersTableSeeder");
    }
}
