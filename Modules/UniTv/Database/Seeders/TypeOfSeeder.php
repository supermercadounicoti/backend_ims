<?php

namespace Modules\UniTv\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\UniTv\Entities\Type;

class TypeOfSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Type::insert([
			[
                'name' => 'Slider - Ofertas',
                'description' => 'Slider - Ofertas',
                'created_at' => '2018-02-07 14:18:00',
                'updated_at' => '2018-02-07 14:18:00',
            ],[
                'name' => 'Productos destacados',
                'description' => 'Productos destacados',
                'created_at' => '2018-02-07 14:18:00',
                'updated_at' => '2018-02-07 14:18:00',
            ],[
                'name' => 'Empleado del mes',
                'description' => 'Empleado del mes',
                'created_at' => '2018-02-07 14:18:00',
                'updated_at' => '2018-02-07 14:18:00',
            ]
		]);
    }
}
