<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informatives', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path');
            $table->string('description');
            $table->integer('type_id')->unsigned();
            $table->dateTime('init_date');
            $table->dateTime('end_date');
            $table->boolean('status');
            $table->boolean('force_disable')->default(0);
            $table->foreign('type_id')
                ->references('id')->on('types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
      Schema::dropIfExists('informative');
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');


    }
}
