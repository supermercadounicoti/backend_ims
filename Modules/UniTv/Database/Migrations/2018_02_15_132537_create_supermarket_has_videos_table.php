<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupermarketHasVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supermarket_has_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supermarket_id')->unsigned();
            $table->integer('video_id')->unsigned();
            $table->foreign('supermarket_id')
                ->references('id')->on('supermarkets');
            $table->foreign('video_id')
                ->references('id')->on('videos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
          Schema::dropIfExists('supermarket_has_videos');
          Schema::dropIfExists('informative');
      DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
