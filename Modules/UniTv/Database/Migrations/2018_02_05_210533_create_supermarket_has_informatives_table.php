<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupermarketHasInformativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supermarket_has_informatives', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supermarket_id')->unsigned();
            $table->integer('informative_id')->unsigned();
            $table->foreign('supermarket_id')
                ->references('id')->on('supermarkets');
            $table->foreign('informative_id')
                ->references('id')->on('informatives');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('supermarket_has_informatives');
        Schema::dropIfExists('informative');
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');


    }
}
