<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module SimpleCredit</title>

       <!-- Laravel Mix - CSS File -->
       <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    </head>
    <body>
        @yield('content')

        <!-- Laravel Mix - JS File -->
        <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>
