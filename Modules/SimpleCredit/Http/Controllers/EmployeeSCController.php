<?php

namespace Modules\SimpleCredit\Http\Controllers;

use Storage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Modules\SimpleCredit\Entities\Clients;
use Modules\SimpleCredit\Entities\EmployeeSC;

class EmployeeSCController extends Controller
{
    protected $isError;

    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('simplecredit::employees.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('simplecredit::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            if ($request->file('file')) {
                $request = $this->uploadFile($request);
                if ($this->validateHeaderExcel($request)) {
                    return self::responseMessage(1068);
                }
                $request = $this->readExcel($request);
                $request->employee_data =  $request->employee_data->toArray();
                foreach ($request->employee_data as $key => $employee) {
                     $run_valid = Clients::isRunUsed($employee['run']);
                     $email_valid = Clients::isEmailUsed($employee['correo']);
                    if (!$run_valid && !$email_valid) {
                        $transaction = Clients::storeFromEmployee($request, $employee);
                        dd('hola que hace :D');
                    }
                }
            } else {
                dd('hola');
            }




            $val_run = Clients::isRunUsed($request->client['run']);
            $val_email = Clients::isEmailUsed($request->client['email']);

            if (!$val_run && !$val_email) {
                $request->{'email'} = $request->client['email'];
                $request->{'token'} = $this->getEmailToken();
                $request->{'password'} = $this->setPasswordToken($request->token);

                $transaction_client = Clients::store($request);
                $request->{'client_id'} = $transaction_client->id;

                if (!@$request->client['parent_client_id']) {
                    foreach ($request->commercial_references as $key => $value) {
                        $transaction = CommercialReferences::store($request->{'client_id'}, $value);
                    }
                    foreach ($request->banks_references as $key => $value) {
                        $transaction = BanksReferences::store($request->{'client_id'}, $value);
                    }
                    $transaction = CompaniesSC::store($request);

                    $password_reset = PasswordReset::store($request);
                    $transaction_client->notify(new SendEmailForPassword($request->token));
                }
            } else{
                return self::responseMessage(1063);
            }
            /*DB::commit();*/
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('simplecredit::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('simplecredit::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Upload excel file
     * @param  $request
     * @return Response
     */
    public function uploadFile($request)
    {
        try {
            $upload_file = $request->file('file');
            $request->description = $upload_file->getClientOriginalName();
            $request->{'path'} = Storage::disk('public')->put('SimpleCredit', $upload_file);
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }

        return $request;
    }

    /**
     * Validate Header of Excel
     * @param  $request
     * @return Response
     */
    public function validateHeaderExcel($request)
    {
        try {
            $this->isError = false;
            $status = Excel::load('storage/'.$request->path , function($reader) {

                $firstrow = $reader->first()->toArray();
                if (!isset($firstrow['run']) && 
                    !isset($firstrow['primer_nombre']) &&
                    !isset($firstrow['segundo_nombre']) &&
                    !isset($firstrow['apellido_paterno']) && 
                    !isset($firstrow['apellido_materno']) &&
                    !isset($firstrow['cumpleanos']) &&
                    !isset($firstrow['correo']) && 
                    !isset($firstrow['direccion']) &&
                    !isset($firstrow['telefono'])
                ) {
                    $this->isError = true;
                }
            });
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }

        return $this->isError; 
    }

    /**
     * Read Excel file
     * @param  $request
     * @return Response
     */
    public function readExcel($request)
    {
        try {
            $request->{'employee_data'} = Excel::load('storage/'.$request->path )->get([
                    'run', 
                    'primer_nombre',
                    'segundo_nombre',
                    'apellido_paterno', 
                    'apellido_materno',
                    'cumpleanos',
                    'correo', 
                    'direccion',
                    'telefono'
                ]);
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }

        return $request; 
    }
}
