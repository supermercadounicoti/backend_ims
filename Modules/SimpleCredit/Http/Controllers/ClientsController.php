<?php

namespace Modules\SimpleCredit\Http\Controllers;

use DB;
use Hash;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Modules\SimpleCredit\Entities\Clients;
use Illuminate\Notifications\Notification;
use App\Notifications\SendEmailForPassword;
use Modules\SimpleCredit\Entities\EmployeeSC;
use Modules\SimpleCredit\Entities\CompaniesSC;
use Modules\SimpleCredit\Entities\BanksReferences;
use Modules\Administration\Entities\PasswordReset;
use Modules\SimpleCredit\Entities\CommercialReferences;

class ClientsController extends Controller
{
    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('simplecredit::admin_clients.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('simplecredit::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $val_run = Clients::isRunUsed($request->client['run']);
            $val_email = Clients::isEmailUsed($request->client['email']);

            if (!$val_run && !$val_email) {
                $request->{'email'} = $request->client['email'];
                $request->{'token'} = $this->getEmailToken();
                $request->{'password'} = $this->setPasswordToken($request->token);

                $transaction_client = Clients::store($request);
                $request->{'client_id'} = $transaction_client->id;

                if (!@$request->client['parent_client_id']) {
                    foreach ($request->commercial_references as $key => $value) {
                        $transaction = CommercialReferences::store($request->{'client_id'}, $value);
                    }
                    foreach ($request->banks_references as $key => $value) {
                        $transaction = BanksReferences::store($request->{'client_id'}, $value);
                    }
                    $transaction = CompaniesSC::store($request);

                    $password_reset = PasswordReset::store($request);
                    $transaction_client->notify(new SendEmailForPassword($request->token));
                } /*else{
                    if (!$request->file('file')) {
                        $request = $this->uploadFile($request);
                        if ($this->validateHeaderExcel($request)) {
                            return self::responseMessage(1068);
                        }
                        $request = $this->readExcel($request);
                    }
                    $transaction = EmployeeSC::store($request);
                }*/
            } else{
                return self::responseMessage(1063);
            }
            /*DB::commit();*/
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1);
    }

    /**
     * Get token to send email
     * @return [type] [description]
     */
    public function getEmailToken(){
        
        $email_token = hash_hmac('sha256', Str::random(40), env('APP_KEY'));

        return $email_token;
    }

    /**
     * set token password
     * @param [type] $token [description]
     */
    public function setPasswordToken($token)
    {
        $hash_token = Hash::make($token);

        return $hash_token;
    }

    /**
     * Business Customer
     * @return [type] [description]
     */
    public function storeBusinessCustomer($request)
    {
        dd('hola :D');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('simplecredit::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('simplecredit::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
