<?php

namespace Modules\SimpleCredit\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Modules\SimpleCredit\Entities\AccountType;

class AccountTypeController extends Controller
{
    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('simplecredit::account_type.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('simplecredit::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
           $transaction = AccountType::store($request);
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return self::responseMessage(1);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(AccountType $account_type)
    {
        return $account_type;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('simplecredit::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(AccountType $account_type)
    {
        try {
            $transaction = AccountType::changeStatus($account_type);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1);
    }

    /**
     * List all data
     * @param  [type] $status [description]
     * @return \Illuminate\Http\Response
     */
    public function listData($status = 2, $mode = 0){
        try {
            if ($status == 0 || $status == 1) {
                $where = [
                    'status' => $status
                ];
            } elseif ($status == 2) {
                $where = [];
            } else {
                return self::responseMessage(1064);
            }
            $transaction = AccountType::listData($where);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return ($mode == 0)? $transaction : 
            datatables()->of($transaction)->toJson();
    }
}
