<?php

namespace Modules\SimpleCredit\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Modules\SimpleCredit\Entities\ResponsiblesCredit;

class ResponsiblesCreditController extends Controller
{
    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('simplecredit::responsibles.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('simplecredit::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            if (!$this->uniqueRun($request->run)) {
                $request->{'code'} = $this->generateCode();
                $transaction = ResponsiblesCredit::store($request);   
            } else {
                return self::responseMessage(1063);
            }
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return self::responseMessage(1);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(ResponsiblesCredit $responsibles_credit)
    {
        return $responsibles_credit;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('simplecredit::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, ResponsiblesCredit $responsibles_credit)
    {
        try {
            $result = ResponsiblesCredit::updateData($request, $responsibles_credit);
            return $this->responseMessage(1, 'The record was update');
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(ResponsiblesCredit $responsibles_credit)
    {
        try {
            $transaction = ResponsiblesCredit::changeStatus($responsibles_credit);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1);
    }

    /**
     * generate access code
     * @return [object]
     */
    public function generateCode()
    {
        try {
            $dt = Carbon::now();
            $dt = explode("==", base64_encode($dt))[0];
            if ($this->uniqueCode($dt)) {
                $this->generateCode();
            }
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return $dt;
    }

    /**
     * validate unique code
     * @return [object]
     */
    public function uniqueCode($code)
    {
        try {
            $where = [
                'code' => $code
            ];
            $transaction = ResponsiblesCredit::existData($where);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return $transaction; 
    }

    /**
     * Validate unique run
     * @param  $run
     * @return [object]
     */
    public function uniqueRun($run)
    {
        try {
            $where = [
                'run' => $run
            ];
            $transaction = ResponsiblesCredit::existData($where);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return $transaction; 
    }

    /**
     * List all data
     * @param  [type] $status [description]
     * @return \Illuminate\Http\Response
     */
    public function listData($client_id, $status = 2, $mode = 0){
        try {
            if ($status == 0 || $status == 1) {
                $where = [
                    'status' => $status,
                    'client_id' => $client_id
                ];
            } elseif ($status == 2) {
                $where = [
                    'client_id' => $client_id
                ];
            } else {
                return self::responseMessage(1064);
            }
            $transaction = ResponsiblesCredit::listData($where);   
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }

        return ($mode == 0)? $transaction : 
            datatables()->of($transaction)->toJson();
    }
}
