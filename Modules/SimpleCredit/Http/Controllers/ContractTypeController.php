<?php

namespace Modules\SimpleCredit\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\SimpleCredit\Entities\ContractType;

class ContractTypeController extends Controller
{   
    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('simplecredit::contract_type.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('simplecredit::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $result = ContractType::store($request);
            return $this->responseMessage(1, 'The record was created');
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('simplecredit::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('simplecredit::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function update(ContractType $contractType, Request $request)
    {
        try {  
            $result = ContractType::updateContractType($contractType, $request);
            return $this->responseMessage(1, 'The record was update');
        } catch (\Exception $e) {
            return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {  
        try {
                $result = ContractType::change($id);

                return $this->responseMessage(1, 'The record was changed status');

        } catch (\Exception $e) {
                return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * List all contract type that exists
     * @return [type] [description]
     */
    public function listAllContractType()
    {
        $result = ContractType::listAllContractType();

        return datatables()->of($result)->toJson();
    }

    public function getTypeById($id)
    {   
        try {
            $result = ContractType::getTypeById($id);
        } catch (\Exception $e) {
            DB::rollback();
                     // dd($e);
        return $this->responseMessage(0, $e->getMessage());
        }
        return $result;
    }

      public function listActiveContractType()
    {
        $result = ContractType::listActiveContractType();
        return $result;
    }
}
