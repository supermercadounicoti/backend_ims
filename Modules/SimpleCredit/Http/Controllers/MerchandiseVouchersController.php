<?php

namespace Modules\SimpleCredit\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Crypt;
use Modules\SimpleCredit\Entities\MerchandiseVouchers;

class MerchandiseVouchersController extends Controller
{
    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('simplecredit::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('simplecredit::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $request->{'bar_code'} = $this->generateCode();
            $transaction = MerchandiseVouchers::store($request);   
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return self::responseMessage(1);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('simplecredit::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('simplecredit::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(MerchandiseVouchers $responsibles_credit)
    {
        try {
            $transaction = MerchandiseVouchers::changeStatus($responsibles_credit);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1);
    }

    /**
     * generate access code
     * @return [object]
     */
    public function generateCode()
    {
        try {
            $dt = Carbon::now();
            $dt = Crypt::encryptString($dt);
            $dt = substr($dt, 0, 23);
            if ($this->uniqueCode($dt)) {
                $this->generateCode();
            }
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return $dt;
    }

    /**
     * Validate unique code
     * @param  $run
     * @return [object]
     */
    public function uniqueCode($code)
    {
        try {
            $where = [
                'bar_code' => $code
            ];
            $transaction = MerchandiseVouchers::existData($where);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return $transaction; 
    }

    /**
     * List all data
     * @param  [type] $status [description]
     * @return \Illuminate\Http\Response
     */
    public function listData($client_id, $status = 2, $mode = 0){
        try {
            if ($status == 0 || $status == 1) {
                $where = [
                    'status' => $status,
                    'client_id' => $client_id
                ];
            } elseif ($status == 2) {
                $where = [
                    'client_id' => $client_id
                ];
            } else {
                return self::responseMessage(1064);
            }
            $transaction = MerchandiseVouchers::listData($where);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return ($mode == 0)? $transaction : 
            datatables()->of($transaction)->toJson();
    }
}
