<?php

Route::group(['middleware' => 'web', 'prefix' => 'simplecredit', 'namespace' => 'Modules\SimpleCredit\Http\Controllers'], function()
{
    Route::get('/', 'SimpleCreditController@index');
    Route::resource('/clients','ClientsController');
    Route::resource('/employee','EmployeeSCController');

/*    // Routes Articles
    Route::resource('/articles','ArticlesController');

    // Routes Contract
    Route::resource('/contract','ContractController');
*/
    // Routes Account Type
    Route::get('/account-type/list/{status?}/{mode?}', 'AccountTypeController@listData');
    Route::resource('/account-type', 'AccountTypeController');

    // Routes Banks
    Route::get('/banks/list/{status?}/{mode?}', 'BanksController@listData');
    Route::resource('/banks', 'BanksController');

    // Routes ResponsiblesCredit
    Route::get('/responsibles-credit/list/{client_id}/{status?}/{mode?}', 'ResponsiblesCreditController@listData');
    Route::resource('/responsibles-credit', 'ResponsiblesCreditController');

    // Routes MerchandiseVouchers
    Route::get('/merchandise-vouchers/list/{client_id}/{status?}/{mode?}', 'MerchandiseVouchersController@listData');
    Route::resource('/merchandise-vouchers', 'MerchandiseVouchersController');


/*    //Routes ContractType
    Route::resource('/contractType','ContractTypeController');
    Route::get('/listContractType','ContractTypeController@listAllContractType');
    Route::get('/getTypeById/{contractType_id}', 'ContractTypeController@getTypeById');
    Route::get('/listActive','ContractTypeController@listActiveContractType');*/

    //views
    /*Route::get('/clients', function () {
        return view('simplecredit::admin_clients.index');
   });*/
    Route::get('/responsables', function () {
        return view('simplecredit::responsables.index');
   });
    Route::get('/employees', function () {
        return view('simplecredit::employees.index');
   });
     Route::get('/contract', function () {
        return view('simplecredit::contract.index');
   });
});
