<?php

namespace Modules\SimpleCredit\Entities;

use Illuminate\Database\Eloquent\Model;

class ContractHasArticlesHeader extends Model
{
    protected $fillable = [
    	'articles_id',
    	'contract_id',
    	'header_id'

    ];
}
