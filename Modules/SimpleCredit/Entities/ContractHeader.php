<?php

namespace Modules\SimpleCredit\Entities;

use Illuminate\Database\Eloquent\Model;

class Contract_Header extends Model
{
    protected $fillable = [
    	'name',
		'description'

    ];
}
