<?php

namespace Modules\SimpleCredit\Entities;

use DB;
use Storage;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class CommercialReferences extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone_number',
        'payment_date',
        'ammount_allocated',
        'client_id'
    ];
    /**
     * Format date when i create a new record
     * @var string
     */
    /*protected $dateFormat = 'Y-m-d H:i:00';*/
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Table name
     * @var string
     */
    protected $table = 'commercial_references';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Create
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function store($client_id, $request)
    {
        try {
            $result = DB::transaction(function () use ($client_id, $request) {
                $query = new CommercialReferences;
                $query->name = $request['name'];
		        $query->phone_number = $request['phone_number'];
		        $query->payment_date = $request['payment_date'];
		        $query->ammount_allocated = $request['ammount_allocated'];
		        $query->client_id = $client_id;

                $query->push();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }
}
