<?php

namespace Modules\SimpleCredit\Entities;
use DB;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class ContractType extends Model
{
    use UtilsFromTraits;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'description',
		'status'
	];

	protected $dateFormat = 'Y-m-d H:i:00';
	/**
	 * Database connection
	 * @var string
	 */
	protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'contract_type';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;

	public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $query = new ContractType;
                $query->name = $request->name;
                $query->description = $request->description;
                $query->status = 1;
                $query->push();

                DB::commit();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    public static function listAllContractType()
    {
        try {
            $result = static::all();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Change status
     * @return Response
     */
    public static function change($id)
    {	
    	try {
			$result = DB::transaction(function () use ($id) {
				$query = ContractType::find($id);
				$query->status = ($query->status == 1)? 0:1;

				$query->push();

                DB::commit();

                return $query;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
        return self::responseMessage(1, $result);
    }

	/**
     * Update Offer
     * @param  [Object] Offer   [Model]
     * @param  [Request] $request
     * @return [Json] Message
     */
    public static function updateContractType($contractType, $request)
    {	
    	try {
			$result = DB::transaction(function () use ($contractType, $request) {
				$contractType = ContractType::find($contractType->id);
				$contractType->name = $request->name;
				$contractType->description = $request->description;

				$contractType->push();

				DB::commit();
			});
			return self::responseMessage(1, $result);
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
    }

	/**
	 * List Only Offer by id
	 * @param  [Object] $offer
	 * @return [Json] Message
	 */
	public static function getTypeById($id)
	{
		try {
			$result = DB::transaction(function () use ($id) {
			$result = ContractType::where('id',$id)
				->get()[0];

			return $result;

		});
		return $result;
	} catch (Exception $e) {
		DB::rollback();
		return self::responseMessage(0, $e->getMessage());
	}
	}

	public static function listActiveContractType()
	{
		try {
            $result = DB::transaction(function () {
                $result = ContractType::where('status', true)
                    ->get();
                return $result;

            });
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
	}
}
