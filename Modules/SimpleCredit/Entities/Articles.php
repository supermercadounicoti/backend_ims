<?php

namespace Modules\SimpleCredit\Entities;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $fillable = [
    	'name',
    	'description'
    ];
}
