<?php

namespace Modules\SimpleCredit\Entities;

use DB;
use Storage;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class BanksReferences extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_number',
        'bank_id',
        'account_type_id',
        'client_id',
        'status'
    ];
    /**
     * Format date when i create a new record
     * @var string
     */
    /*protected $dateFormat = 'Y-m-d H:i:00';*/
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Table name
     * @var string
     */
    protected $table = 'banks_references';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Create
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function store($client_id, $request)
    {
        try {
            $result = DB::transaction(function () use ($client_id, $request) {
                $query = new BanksReferences;
                $query->account_number = $request['account_number'];
		        $query->bank_id = $request['bank_id'];
		        $query->account_type_id = $request['account_type_id'];
		        $query->client_id = $client_id;

                $query->push();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }
}
