<?php

namespace Modules\SimpleCredit\Entities;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $fillable = [
    	'name',
    	'description',
    	'status'

    ];
}
