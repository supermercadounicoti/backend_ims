<?php

namespace Modules\SimpleCredit\Entities;

use DB;
use Storage;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class EmployeeSC extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'first_name',
        'second_name',
        'father_surname',
        'mother_surname',
        'birthday',
        'status'
    ];
    /**
     * Format date when i create a new record
     * @var string
     */
    /*protected $dateFormat = 'Y-m-d H:i:00';*/
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Table name
     * @var string
     */
    protected $table = 'employee_sc';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Create
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function store($request)
    {
    	dd($request);
        try {
            $result = DB::transaction(function () use ($request) {
                $query = new EmployeeSC;
                $query->client_id = $request->client_id;
                $query->activity = $request->company_data['activity'];
                $query->legal_representative_name = $request->company_data['legal_representative_name'];
                $query->invoice_to_name_of = $request->company_data['invoice_to_name_of'];
                $query->contact_dept_payments = $request->company_data['contact_dept_payments'];
                $query->paydays = $request->company_data['paydays'];
                $query->request_amount = $request->company_data['request_amount'];

                $query->push();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }
}
