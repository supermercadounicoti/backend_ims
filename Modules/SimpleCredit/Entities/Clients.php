<?php

namespace Modules\SimpleCredit\Entities;

use DB;
use Storage;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Clients extends Authenticatable
{
    use HasApiTokens, Notifiable, UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'run',
        'address',
        'phone',
        'email',
        'password',
        'city_id',
        'parent_client_id',
        'status',
        'approved',
        'contract_path'
    ];
    /**
     * Format date when i create a new record
     * @var string
     */
    /*protected $dateFormat = 'Y-m-d H:i:00';*/
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Table name
     * @var string
     */
    protected $table = 'clients';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Create
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function store($request)
    {   dd($request);
        try {
            $result = DB::transaction(function () use ($request) {
                $query = new Clients;
                $query->run = $request->client['run'];
		        $query->address = $request->client['address'];
		        $query->phone = $request->client['phone'];
		        $query->email = $request->client['email'];
		        $query->password = $request->password;
		        $query->city_id = $request->client['city_id'];
                $query->parent_client_id = @$request->client['parent_client_id'];

                $query->push();

                /*DB::commit();*/

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Create
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function storeFromEmployee($request, $data)
    {
        try {
            $result = DB::transaction(function () use ($request, $data) {
                $query = new Clients;
                $query->run = $request['run'];
                $query->address = $request['direccion'];
                $query->phone = $request['telefono'];
                $query->email = $request['correo'];
                $query->password = $request->password;
                $query->city_id = $request['city_id'];
                $query->parent_client_id = @$request->parent_client_id;

                $query->push();

                /*DB::commit();*/

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Validate if RUN exists on the database
     * @param  [integer]  $value
     * @return boolean
     */
    public static function isRunUsed($value)
    {
        try {
            $result = static::where('run', $value)->exists();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    /**
     * Validate if RUN exists on the database
     * @param  [integer]  $value
     * @return boolean
     */
    public static function isEmailUsed($value)
    {
        try {
            $result = static::where('email', $value)->exists();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

}
