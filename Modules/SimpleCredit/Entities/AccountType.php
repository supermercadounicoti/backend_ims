<?php

namespace Modules\SimpleCredit\Entities;

use DB;
use Storage;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'status'
    ];
    /**
     * Format date when i create a new record
     * @var string
     */
    /*protected $dateFormat = 'Y-m-d H:i:00';*/
    /**
     * Database connection
     * @var string
     */
   // protected $connection = 'mysql';

    /**
     * Table name
     * @var string
     */
    protected $table = 'accounttype';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Create
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $query = new AccountType;
                $query->name = $request->name;
		        $query->description = $request->description;

                $query->push();

                DB::commit();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Change status
     * @param  [type] $access_control [description]
     * @return [type]                 [description]
     */
    public static function changeStatus($model)
    {
        try {
            $model->status = ($model->status == 1)? 0:1;
            $model->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $model;
    }

    /**
     * list data
     * @return [object]
     */
    public static function listData($where)
    {
        try {
            $result = static::where($where)
            	->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }
}
