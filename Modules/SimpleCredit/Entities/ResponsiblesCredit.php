<?php

namespace Modules\SimpleCredit\Entities;

use DB;
use Storage;
use Carbon\Carbon;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class ResponsiblesCredit extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'run',
        'first_name',
        'second_name',
        'father_surname',
        'mother_surname',
        'code',
        'status',
        'client_id'
    ];
    /**
     * Format date when i create a new record
     * @var string
     */
    /*protected $dateFormat = 'Y-m-d H:i:00';*/
    /**
     * Database connection
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Table name
     * @var string
     */
    protected $table = 'responsibles_credit';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Create
     * @param  [type] $request
     * @return [type] JSON and Slack message
     */
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $query = new ResponsiblesCredit;
                $query->run = $request->run;
		        $query->first_name = $request->first_name;
		        $query->second_name = $request->second_name;
		        $query->father_surname = $request->father_surname;
		        $query->mother_surname = $request->mother_surname;
		        $query->code = $request->code;
		        $query->client_id = $request->client_id;

                $query->push();

                DB::commit();

                return $query;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * update
     * @return Response
     */
    public static function updateData($request, $model)
    {
        try {
            $model->first_name = $request->first_name;
            $model->second_name = $request->second_name;
            $model->father_surname = $request->father_surname;
            $model->mother_surname = $request->mother_surname;
            $model->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $model;
    }

    /**
     * Change status
     * @param  [type] $access_control [description]
     * @return [type]                 [description]
     */
    public static function changeStatus($model)
    {
        try {
            $model->status = ($model->status == 1)? 0:1;
            $model->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $model;
    }

    /**
     * list data
     * @return [object]
     */
    public static function listData($where)
    {
        try {
            $result = static::where($where)
            	->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    /**
     * Exists data
     * @return [object]
     */
    public static function existData($where)
    {
        try {
            $result = static::where($where)
            	->exists();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }
}
