<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesScTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies_sc', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')
                ->unsigned();
            $table->string('activity');
            $table->string('legal_representative_name');
            $table->string('invoice_to_name_of');
            $table->string('contact_dept_payments');
            $table->integer('paydays');
            $table->integer('request_amount')
                ->nullable();
            $table->integer('amount_approved')
                ->nullable();

            $table->foreign('client_id')
                ->references('id')->on('clients');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_sc');
    }
}
