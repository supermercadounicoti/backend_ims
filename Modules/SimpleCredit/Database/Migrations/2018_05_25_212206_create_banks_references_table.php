<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks_references', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_number');
            $table->integer('bank_id')
                ->unsigned();
            $table->integer('account_type_id')
                ->unsigned();
            $table->integer('client_id')
                ->unsigned();
            $table->boolean('status')
                ->default(1);

            $table->foreign('bank_id')
                ->references('id')->on('banks');
            $table->foreign('client_id')
                ->references('id')->on('clients');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks_references');
    }
}
