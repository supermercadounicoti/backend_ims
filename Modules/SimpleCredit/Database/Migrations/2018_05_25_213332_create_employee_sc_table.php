<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeScTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_sc', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')
                ->unsigned();
            $table->string('first_name');
            $table->string('second_name');
            $table->string('father_surname');
            $table->string('mother_surname');
            $table->date('birthday');
            $table->boolean('status')
                ->default(1);

            $table->foreign('client_id')
                ->references('id')->on('clients');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_sc');
    }
}
