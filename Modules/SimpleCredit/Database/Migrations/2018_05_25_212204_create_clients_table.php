<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('run');
            $table->string('address');
            $table->string('phone');
            $table->string('email');
            $table->string('password');
            $table->integer('city_id')
                ->unsigned();
            $table->integer('parent_client_id')
                ->nullable()
                ->unsigned();
            $table->boolean('status')
                ->default(1);
            $table->boolean('approved')
                ->default(0);
            $table->string('contract_path')
                ->nullable();

            $table->foreign('city_id')
                ->references('id')->on('cities');
            $table->foreign('parent_client_id')
                ->references('id')->on('clients');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
