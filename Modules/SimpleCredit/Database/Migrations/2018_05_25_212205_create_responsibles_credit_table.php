<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsiblesCreditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responsibles_credit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('run')
                ->unique();
            $table->string('first_name');
            $table->string('second_name');
            $table->string('father_surname');
            $table->string('mother_surname');
            $table->string('code');
            $table->boolean('status')
                ->default(1);
            $table->integer('client_id')
                ->unsigned();

            $table->timestamps();

            $table->foreign('client_id')
                ->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responsibles_credit');
    }
}
