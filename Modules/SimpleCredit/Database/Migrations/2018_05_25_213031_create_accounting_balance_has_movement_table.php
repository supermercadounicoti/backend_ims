<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingBalanceHasMovementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_balance_has_movement', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('movement_type')
                ->unsigned();
            $table->integer('accounting_balanace_id')
                ->unsigned();
            $table->integer('doc_number');

            $table->foreign('accounting_balanace_id')
                ->references('id')->on('accounting_balance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting_balance_has_movement');
    }
}
