<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_balance', function (Blueprint $table) {
            $table->increments('id');
            $table->booleaN('status');
            $table->integer('client_id')
                ->unsigned();
            $table->integer('amount_id')
                ->unsigned();
            $table->dateTime('expired_date');

            $table->foreign('client_id')
                ->references('id')->on('clients');
            $table->foreign('amount_id')
                ->references('id')->on('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting_balance');
    }
}
