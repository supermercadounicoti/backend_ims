<?php

namespace Modules\SimpleCredit\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Modules\SimpleCredit\Entities\Banks::insert([
            [
                'name' => 'Banco 1',
                'description' => 'Banco 1',
                'status' => 1,
                'created_at' => '2018-06-12 19:32',
                'updated_at' => '2018-06-12 19:32'
            ],[
                'name' => 'banco 2',
                'description' => 'banco 2',
                'status' => 1,
                'created_at' => '2018-06-12 19:32',
                'updated_at' => '2018-06-12 19:32'
            ]
        ]);
    }
}
