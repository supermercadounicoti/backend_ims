<?php

namespace Modules\SimpleCredit\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AccountTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Modules\SimpleCredit\Entities\AccountType::insert([
            [
                'name' => 'Cuenta Corriente',
                'description' => 'Cuenta Corriente',
                'status' => 1,
                'created_at' => '2018-06-12 19:32',
                'updated_at' => '2018-06-12 19:32'
            ],[
                'name' => 'Cuenta Vista',
                'description' => 'Cuenta Vista',
                'status' => 1,
                'created_at' => '2018-06-12 19:32',
                'updated_at' => '2018-06-12 19:32'
            ]
        ]);
    }
}
