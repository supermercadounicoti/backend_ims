<?php

namespace Modules\SimpleCredit\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SimpleCreditDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AccountTypeTableSeeder::class);
        $this->call(BankTableSeeder::class);
    }
}
