<?php

namespace Modules\SimpleCredit\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\SimpleCredit\Entities\ContractType;

class ContractTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();

        ContractType::insert([
            [
                'name' => 'First contract type',
                'description' => 'First contract type'
               
            ],
            [
                'name' => 'Second contract type',
                'description' => 'Second contract type'
               
            ]
        ]);
    }
}
