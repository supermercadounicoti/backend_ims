<?php

namespace Modules\Reception\Entities;

use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrderHeader extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sqlsrv';

    /**
     * Table name
     * @var string
     */
    protected $table = 'COMPRAS_OCOMPRA_E';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * validate if purchase order exists
     * @param  [type]  $local
     * @param  [type]  $purchaseOrder
     * @return Response
     */
    public static function isValid($local, $purchaseOrder)
    {
        try {
            $result = static::where([
                ['numero', $purchaseOrder],
                ['codigo_local', $local]
            ])->whereIn('estado', ['016', '017'])
            ->exists();
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    /**
     * get details from purchase order
     * @param  [type] $local
     * @param  [type] $purchaseOrder
     * @return Response
     */
    public static function getDetails($local, $purchaseOrder)
    {
        try {
            $result = static::where([
                ['numero', $purchaseOrder],
                ['codigo_local', $local]
            ])->get(['rut', 'nombre', 'fecha'])[0];
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }
}
