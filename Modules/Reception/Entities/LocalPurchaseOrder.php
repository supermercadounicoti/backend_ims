<?php

namespace Modules\Reception\Entities;

use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class LocalPurchaseOrder extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sqlsrv';

    /**
     * Table name
     * @var string
     */
    protected $table = 'TABLA_LOCALES';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * get all locals
     * @return Response
     */
    public static function getLocals()
    {
    	try {
    		$result = static::where('estado', '001')
                ->orderBy('descripcion')
                ->get(['codigo_local', 'descripcion']);
	    } catch (Exception $e) {
	      	return self::responseMessage(0, $e->getMessage());
	    }

	    return $result;
    }


}
