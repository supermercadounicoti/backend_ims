<?php

namespace Modules\Reception\Entities;

use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDetails extends Model
{
    use UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Database connection
     * @var string
     */
    protected $connection = 'sqlsrv';

    /**
     * Table name
     * @var string
     */
    protected $table = 'COMPRAS_OCOMPRA_D';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * get details of purchase order
     * @param  [type] $local
     * @param  [type] $purchaseOrder
     * @return Response
     */
    public static function getDetails($local, $purchaseOrder)
    {
    	try {
    		$result = static::where([
	            ['numero', $purchaseOrder],
	            ['codigo_local', $local]
	        ])->get(['codigo', 'glosa', 'cantidad', 'recepcion']);
	    } catch (Exception $e) {
	      	return self::responseMessage(0, $e->getMessage());
	    }

	    return $result;
    }

    /**
     * Get details of product
     * @param  [type] $request 
     * @return Response
     */
    public static function getDetailsProduct($request)
    {
        try {
            $result = static::where([
                ['numero', $request->purchase_order],
                ['codigo_local', $request->supermarket_id],
                ['codigo', $request->product_code]
            ])->first();
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * update the record
     * @param  [type] $request
     * @return Response
     */
    public static function modify($request)
    {
        try {
            $result = static::where([
                ['numero', $request->purchase_order],
                ['codigo_local', $request->supermarket_id],
                ['codigo', $request->product_code]
            ])->update(['recepcion' => $request->received]);;

        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }
}
