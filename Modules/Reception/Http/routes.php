<?php

Route::group(['middleware' => 'web', 'prefix' => 'reception', 'namespace' => 'Modules\Reception\Http\Controllers'], function()
{
    Route::get('/', 'ReceptionController@index');
    //Route::get('/find-purchase-order/{local}/{purchaseOrder}', 'PurchaseOrderHeaderController@findByPurchaseOrder');
    //Route::get('/list-purchase-order/{local}/{purchaseOrder}', 'PurchaseOrderHeaderController@listDetails');

    //Route::post('/update-purchase-order', 'PurchaseOrderHeaderController@update');
});
