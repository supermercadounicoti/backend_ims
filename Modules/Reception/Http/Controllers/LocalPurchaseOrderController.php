<?php

namespace Modules\Reception\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Modules\Reception\Entities\LocalPurchaseOrder;

class LocalPurchaseOrderController extends Controller
{
    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('reception::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('reception::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('reception::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('reception::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Get locals
     * @return [type] [description]
     */
    public function getLocals()
    {
        try {
            $transaction = LocalPurchaseOrder::getLocals();
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1, $transaction);
    }
}
