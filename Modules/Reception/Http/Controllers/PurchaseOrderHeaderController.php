<?php

namespace Modules\Reception\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\UtilsFromTraits;
use Illuminate\Routing\Controller;
use Modules\Reception\Entities\PurchaseOrderHeader;
use Modules\Reception\Entities\PurchaseOrderDetails;

class PurchaseOrderHeaderController extends Controller
{
    use UtilsFromTraits;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('reception::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('reception::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('reception::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('reception::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        try {
            $transaction = PurchaseOrderDetails::getDetailsProduct($request);
            $request->received += $transaction->Recepcion;
            if ($request->received > $transaction->Cantidad) {
                return self::responseMessage(1069);
            }
            $transaction = PurchaseOrderDetails::modify($request);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage($transaction);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * function to validate that PO exists
     * @param  [type] $local
     * @param  [type] $purchaseOrder
     * @return Response
     */
    public function findByPurchaseOrder($local, $purchaseOrder)
    {
        try {
            $transaction = PurchaseOrderHeader::isValid($local, $purchaseOrder);
            //$transaction = PurchaseOrderHeader::getDetails($local, $purchaseOrder);
            //$collection->push($transaction);

            //$transaction = PurchaseOrderDetails::getDetails($local, $purchaseOrder);
            //$collection->push($transaction);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return self::responseMessage(1, $transaction);
    }

    /**
     * Get details of PO
     * @param  [type] $local
     * @param  [type] $purchaseOrder
     * @return Response
     */
    public function listDetails($local, $purchaseOrder)
    {
        try {
            $collection = collect();
            $transaction = PurchaseOrderHeader::getDetails($local, $purchaseOrder);
            $collection->push($transaction);

            $transaction = PurchaseOrderDetails::getDetails($local, $purchaseOrder);
            $collection->push($transaction);
        } catch (Exception $e) {
            return self::responseMessage(0, $e->getMessage());
        }
        return $collection;
    }
}
