<?php

namespace Modules\Administration\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Administration\Entities\Modules;

class RedirectIfAuthorized
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        if ($request->method() == 'GET') {
            $authorized = Modules::isAuthorized(Auth::user()->id, 
                $request->path());
            if (!$authorized) {
                abort(404);
            }
            return $next($request);
        }
        return $next($request);
    }
}
