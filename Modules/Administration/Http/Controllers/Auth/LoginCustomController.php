<?php

namespace Modules\Administration\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Traits\LoginFromTrait;

class LoginCustomController extends Controller
{
    use LoginFromTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $redirectTo = '/helpdesk/tablero';

    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }
}