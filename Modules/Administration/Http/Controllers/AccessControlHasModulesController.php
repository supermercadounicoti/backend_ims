<?php

namespace App\Http\Controllers;

use App\AccessControlHasModules;
use Illuminate\Http\Request;

class AccessControlHasModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AccessControlHasModules  $accessControlHasModules
     * @return \Illuminate\Http\Response
     */
    public function show(AccessControlHasModules $accessControlHasModules)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AccessControlHasModules  $accessControlHasModules
     * @return \Illuminate\Http\Response
     */
    public function edit(AccessControlHasModules $accessControlHasModules)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AccessControlHasModules  $accessControlHasModules
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccessControlHasModules $accessControlHasModules)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AccessControlHasModules  $accessControlHasModules
     * @return \Illuminate\Http\Response
     */
    public function destroy(AccessControlHasModules $accessControlHasModules)
    {
        //
    }
}
