<?php

namespace Modules\Administration\Http\Controllers;

use DB;
use Hash;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notification;
use App\Notifications\SendEmailForPassword;
use Modules\Administration\Entities\Modules;
use Modules\Administration\Entities\UserData;
use Modules\Administration\Entities\AccessControl;
use Modules\Administration\Entities\PasswordReset;
use Modules\Administration\Entities\ApplicationsMobile;
use Modules\Administration\Entities\AccessControlHasModules;

class AccessControlController extends Controller
{
	use UtilsFromTraits;
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('administration::permission.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('administration::user.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{	
		try {
			$request = $this->orderRequest($request);
			$val_run = UserData::isValid($request->ud_run);
			$val_email = AccessControl::isValid($request->email);
			if (!$val_run && !$val_email) {
				$request->{'token'} = $this->getEmailToken();
				$request->{'password'} = $this->setPasswordToken($request->token);

				$request->{'user_data_id'} = UserData::store($request)->id;
				$transaction = AccessControl::store($request);

				$password_reset = PasswordReset::store($request);
				$transaction->notify(new SendEmailForPassword($request->token));
			} else{
				return self::responseMessage(1063);
			}
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}

		return self::responseMessage(1);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\AccessControl  $accessControl
	 * @return \Illuminate\Http\Response
	 */
	public function show(AccessControl $accessControl)
	{
		return $accessControl;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\AccessControl  $accessControl
	 * @return \Illuminate\Http\Response
	 */
	public function edit(AccessControl $accessControl)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\AccessControl  $accessControl
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, AccessControl $accessControl)
	{
		try {
			$transaction = AccessControl::changeStatus($accessControl);
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\AccessControl  $accessControl
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(AccessControl $accessControl)
	{
		//
	}


	/**
	 * Order request 
	 * @param \Illuminate\Http\Response
	 * @return \Illuminate\Http\Response
	 */
	public function orderRequest($request)
	{
		try {
			$request->{'ud_run'} = $request->user_data['ud_run'];
			$request->{'ud_first_name'} = $request->user_data['ud_first_name'];
			$request->{'ud_second_name'} = $request->user_data['ud_second_name'];
			$request->{'ud_father_surname'} = $request->user_data['ud_father_surname'];
			$request->{'ud_mother_surname'} = $request->user_data['ud_mother_surname'];

		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}

		return $request;
	}

	/**
	 * Get token to send email
	 * @return [type] [description]
	 */
	public function getEmailToken(){
		
		$email_token = hash_hmac('sha256', Str::random(40), env('APP_KEY'));

		return $email_token;
	}

	/**
	 * set token password
	 * @param [type] $token [description]
	 */
	public function setPasswordToken($token)
	{
		$hash_token = Hash::make($token);

		return $hash_token;
	}

	/**
	 * Show form to create password
	 * @param  [string] $token
	 * @return view
	 */
	public function showPassword($token)
	{
		// Se debe cambiar ruta acá
		return view('auth.passwords.reset', compact('token'));
	}


	/**
	 * Store password
	 * @param  Request $request [description]
	 * @return \Illuminate\Http\Response
	 */
	public function storePassword(Request $request)
	{
		$transaction = PasswordReset::where('email', $request->email)->first();
		if (Hash::check($request->token, $transaction->token)) {
			$transaction =  AccessControl::updatePassword($request->email, Hash::make($request->password));
			return redirect()->route('login');
		}
		return self::responseMessage(0, 'Email or hash it does not correspond');

	}

	/**
	 * List user by status
	 * @param  [type] $status [description]
	 * @return \Illuminate\Http\Response
	 */
	public function listUsers($status){
		try {
			$transaction = AccessControl::listUsers($status);   
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return $transaction;
	}

	/**
	 * List all user
	 * @param  [type] $status [description]
	 * @return \Illuminate\Http\Response
	 */
	public function listAllUsers(){
		try {
			$transaction = AccessControl::listAllUsers();   
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}

		return datatables()->of($transaction)->toJson();
	}

	/**
	 *  Get menu to user
	 * @param  [type] $user_id [description]
	 * @return \Illuminate\Http\Response
	 */
	public function menu($user_id)
	{
		try {
			$parent_modules = Modules::getModules(Auth::user()->id);
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return $this->sortedMenu($parent_modules);
	}

	/**
	 *  Sort menu with child and grandchild
	 * @param  [model] $menu
	 * @return array
	 */
	public function sortedMenu($menu)
	{
		try {
			$collect = collect();
			$collectChildren = collect();
			$menuGroup = $menu->groupBy('md_parent_id');
			foreach ($menuGroup as $key => $menuParent) {
				foreach ($menuParent as $key => $parent) {
					$submenu = $menu->where('md_parent_id', '=', $parent->id);
					if ($submenu->count() > 0) {
						foreach ($submenu as $key => $children) {
							$grandChild = $menu->where('md_parent_id', '=',$children->id);
							$data = [
									'id' => $children->id,
									'name' => $children->md_name,
									'path' => $children->md_path,
									'icon' => $children->md_icon,
									'grandChild' => $grandChild
							];
							$collectChildren->push($data);
						}
						$data = $this->formatToMenu($parent, $collectChildren);
						$collectChildren = collect();
						$collect->push($data);
					} else {
						$data = $this->formatToMenu($parent, $collectChildren);
						$collect->push($data);
					}
				}
				break;
			}
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return $collect;
	}

	/**
	 *  Format array with menu
	 * @param  $parent
	 * @param  $children
	 * @param  $grandChild
	 * @return array
	 */
	public function formatToMenu($parent, $children = null, $grandChild = null)
	{
		$data = [
				'id' => $parent->id,
				'name' => $parent->md_name,
				'path' => $parent->md_path,
				'icon' => $parent->md_icon,
				'children' => $children
		];
		return $data;
	}

	/**
	 * Get menu mobile
	 * @return \Illuminate\Http\Response
	 */
	public function getMenuMobile(Request $request)
	{
		try {
			$getMenuMobile = ApplicationsMobile::getAppMobile($request->user()->id);
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return $getMenuMobile;
	}

	/**
	 * List user by status
	 * @param  [type] $status [description]
	 * @return \Illuminate\Http\Response
	 */
	public function listUsersById($id)
	{
		try {
			$transaction = AccessControl::listUsersById($id);
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return $transaction;
	}

	/**
	 * enter access to modules from user
	 * @param  Request $request [description]
	 * @return \Illuminate\Http\Response
	 */
	public function storeAccessModule(Request $request)
	{
		try {
			$notCreated = collect();
			$modules = Modules::getModules($request->user_id);
			$items = collect($request->description);
			foreach ($items as $module_id) {
				$transaction = Modules::getParent($module_id);
				if ($transaction->md_parent_id != 1) {
					if (!$items->contains($transaction->md_parent_id)) {
						$items->push($transaction->md_parent_id);
					}
				}
			}
			foreach ($items as $module_id) {
				
				if ($modules->contains('id', $module_id)) {
					$modules->where('id', $module_id)->map(function ($modules) {
				    	$modules['status'] = true;
			    		return $modules;
					});
				} else {
					$notCreated->push($module_id);
				}
			}
			
			if ($notCreated->count() == 0  && $modules->where('status', false)->count() == 0) {
				return self::responseMessage(1523, 'No action was taken');
			}
			// entering new modules to the user
			if ($notCreated->count() != 0) {
				$transaction = AccessControlHasModules::storeUserModule($request->user_id, $notCreated);
			}

			if ($modules->where('status', false)->count() != 0) {
				foreach ($modules->where('status', false) as $module) {
					$transaction = AccessControlHasModules::changeStatus($request->user_id, $module->id, false);
				}
			}
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return self::responseMessage(1, 'Record was updated');
	}
}
