<?php

namespace Modules\Administration\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Traits\UtilsFromTraits;
use App\Http\Controllers\Controller;
use Modules\Administration\Entities\Modules;

class ModulesController extends Controller
{
	use UtilsFromTraits;
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Modules  $modules
	 * @return \Illuminate\Http\Response
	 */
	public function show(Modules $modules)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Modules  $modules
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Modules $modules)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Modules  $modules
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Modules $modules)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Modules  $modules
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Modules $modules)
	{
		//
	}

	/**
	 * List modules
	 * @return \Illuminate\Http\Response
	 */
	public function listModule()
	{
		try {
			$menu = Modules::listModules();
			$collect = collect();
			$collectChildren = collect();
			$menuGroup = $menu->groupBy('md_parent_id');
			foreach ($menuGroup as $key => $menuParent) {
				foreach ($menuParent as $key => $parent) {
					$submenu = $menu->where('md_parent_id', '=', $parent->id);
					if ($submenu->count() > 0) {
						foreach ($submenu as $key => $children) {
							$grandChild = $menu->where('md_parent_id', '=',$children->id);
							$data = [
									'id' => $children->id,
									'name' => $children->md_name,
									'path' => $children->md_path,
									'icon' => $children->md_icon,
									'grandChild' => $grandChild
							];
							$collectChildren->push($data);
						}
						$data = $this->formatToMenu($parent, $collectChildren);
						$collectChildren = collect();
						$collect->push($data);
					} else {
						$data = $this->formatToMenu($parent, $collectChildren);
						$collect->push($data);
					}
				}
				break;
			}
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return $collect;
	}

	/**
	 *  Format array with menu
	 * @param  $parent
	 * @param  $children
	 * @param  $grandChild
	 * @return array
	 */
	public function formatToMenu($parent, $children = null, $grandChild = null)
	{
		$data = [
				'id' => $parent->id,
				'name' => $parent->md_name,
				'path' => $parent->md_path,
				'icon' => $parent->md_icon,
				'children' => $children
		];
		return $data;
	}

	/**
	 * get active modules from a user
	 * @param  $user_id
	 * @return \Illuminate\Http\Response
	 */
	public function activeModule($user_id)
	{
		try {
			$transaction = Modules::getModules($user_id);
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return $transaction->pluck('id')->toArray();
	}
}
