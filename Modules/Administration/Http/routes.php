<?php

Route::group(['middleware' => ['web'], 'prefix' => 'administration', 'namespace' => 'Modules\Administration\Http\Controllers'], function()
{
    Route::get('/', function () {
        return view('menu');
    })->middleware(['auth']);

    Route::get('/create', 'AccessControlController@create')->middleware('auth');
	/*Route::get('/', 'Auth\LoginCustomController@showLoginForm');*/
    Route::post('/signIn', 'Auth\LoginCustomController@Login');
	Route::get('/signOut', 'Auth\LoginCustomController@logout')->name('logout');

	Route::post('/register', 'Auth\LoginCustomController@Login')->name('register');

    Route::get('/password/reset/{token}', 'AccessControlController@showPassword');

    Route::post('/password/reset', 'AccessControlController@storePassword')->name('password.request');

    Route::get('/access-control/menu/{user_id}', 'AccessControlController@menu')->middleware('auth');
    Route::get('/access-control/list/{status}', 'AccessControlController@listUsers');
    Route::get('/access-control/list', 'AccessControlController@listAllUsers');
    Route::get('/access-control/{id}', 'AccessControlController@listUsersById')->middleware('auth');
    Route::post('/access-control/module', 'AccessControlController@storeAccessModule')->middleware('auth');
    Route::resource('/access-control','AccessControlController');
    Route::resource('/user-data','UserDataController')->middleware('auth');
    Route::get('/module/list','ModulesController@listModule')->middleware('auth');
    Route::get('/module/active/list/{user_id}','ModulesController@activeModule')->middleware('auth');
});
