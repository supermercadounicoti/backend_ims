<?php

namespace Modules\Administration\Entities;

use DB;
use Hash;
use App\Traits\UtilsFromTraits;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AccessControl extends Authenticatable
{
    use HasApiTokens, Notifiable, UtilsFromTraits;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'user_data_id', 'status'
    ];

    protected $dateFormat = 'Y-m-d H:i:00';

    /**
	 * Table name
	 * @var string
	 */
	protected $table = 'access_controls';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * Modules relationships
     */
    public function modules()
    {
        return $this->belongsToMany(
                'Modules\Administration\Entities\Modules', 
                'access_control_has_modules',
                'access_control_id',
                'module_id'
            )
        ->withPivot('patern_module_id');
    }

    /**
     * Applications Mobile relationships
     */
    public function appsMobile()
    {
        return $this->belongsToMany(
                'Modules\Administration\Entities\ApplicationsMobile', 
                'access_controls_has_app_mobile',
                'access_control_id',
                'application_mobile_id'
            );
    }

    /**
     * UserData relationships
     */
    public function userData(){
        return $this->belongsTo('Modules\Administration\Entities\UserData')
            ->select(array('id', 
                'ud_run', 
                'ud_first_name', 
                'ud_second_name', 
                'ud_father_surname',
                'ud_mother_surname'));
    }


    /**
     * insert record
     */
    
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $result = new AccessControl;
                $result->email = $request->email;
                $result->password = $request->password;
                $result->user_data_id = $request->user_data_id;

                $result->push();

                DB::commit();

                return $result;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    /**
     * Validate if email exists
     * @param  [type]  $value
     * @return object
     */
    public static function isValid($value)
    {
        try {
            $result = static::where('email', $value)->exists();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    /**
     * List user by status
     * @param  [type] $status
     * @return object
     */
    public static function listUsers($status)
    {
        try {
            $result = static::with('userData')
                ->where('status', $status)->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    /**
     * List user by status
     * @param  [type] $status
     * @return object
     */
    public static function listUsersById($id)
    {   
        
        try {
            $result = AccessControl::with('userData')->where('id', $id)
                ->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }
        return $result;
    }

    /**
     * Password Update
     */
    public static function updatePassword($email, $password)
    {
        try {
            $transaction = static::where('email', $email)
                ->update(['password' => $password]);
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $transaction;
    }

    public static function listAllUsers()
    {
        try {
            $result = static::with('userData')->get();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }

    public static function changeStatus($access_control)
    {
        try {
            $access_control->status = ($access_control->status == 1)? 0:1;
            $access_control->save();
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $access_control;
    }
}
