<?php

namespace Modules\Administration\Entities;

use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class ApplicationsMobile extends Model
{
	use UtilsFromTraits;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'am_name',
		'am_path',
		'am_image_path',
		'am_status'
	];

	/**
	 * Database connection
	 * @var string
	 */
	protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'applications_mobile';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * Many to many access control
     * @return [type] [description]
     */
    public function access_control()
    {
        return $this->belongsToMany(
                'Modules\Administration\Entities\AccessControl', 
                'access_controls_has_app_mobile',
                'application_mobile_id',
                'access_control_id'
            );
    }

    /**
     * get modules of apps mobile
     * @param  $user_id
	 * @return \Illuminate\Http\Response
     */
    public static function getAppMobile($user_id)
    {
    	try {
    		$appMobile = static::where('am_status', 1)
    			->whereHas('access_control', function ($query) use ($user_id) {
					$query->where([
						[
							'access_controls.id', '=', $user_id
						]
					]);
			})
			->get();
    	} catch (Exception $e) {
    		return self::responseMessage(0, $e->getMessage());
    	}
    	return $appMobile;
    }

}
