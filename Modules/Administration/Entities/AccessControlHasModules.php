<?php

namespace Modules\Administration\Entities;

use DB;
use Illuminate\Database\Eloquent\Model;

class AccessControlHasModules extends Model
{
	/*use UtilsFromTraits;*/

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'access_control_id',
		'modules_id'
	];

	/**
	 * Database connection
	 * @var string
	 */
	#protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'access_control_has_modules';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	public static function existsRecord($user_id, $module_id)
	{
		try {
			$transaction = static::where([
					['access_control_id', $user_id],
					['module_id', $module_id]
				])
			->exists();
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return $transaction;
	}

	public static function store($user_id, $module_id)
	{
		try {
			$result = DB::transaction(function () use ($user_id, $module_id) {
                $result = new AccessControlHasModules;
                $result->access_control_id = $user_id;
                $result->module_id = $module_id;

                $result->push();

                DB::commit();

                return $result;
            });
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}

		return $result;
	}

	public static function changeStatus($user_id, $module_id, $status=true)
	{
    	try {
			$result = DB::transaction(function () use ($user_id, $module_id, $status) {
				$transaction = static::where([
						['access_control_id', $user_id],
						['module_id', $module_id]
					])->first();
				$transaction->status = $status;

				$transaction->push();

				DB::commit();

				return $transaction;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}

		return $result;
	}

	public static function storeUserModule($user_id, $modules)
	{
		try {
			foreach ($modules as $module) {
				$transaction = static::existsRecord($user_id, $module);
				if ($transaction) {
					$transaction = static::changeStatus($user_id, $module);
				} else {
					$transaction = static::store($user_id, $module);
				}
			}
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}
		return $transaction;
	}
}
