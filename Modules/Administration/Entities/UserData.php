<?php

namespace Modules\Administration\Entities;

use DB;
use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
	use UtilsFromTraits;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'ud_run',
		'ud_first_name',
		'ud_second_name',
		'ud_father_surname',
		'ud_mother_surname',
	];

	/**
	 * Database connection
	 * @var string
	 */
	#protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'users_data';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;

    protected $dateFormat = 'Y-m-d H:i:00';
	/**
	 * insert record
	 */
	
	public static function store($request)
	{
		try {
			$result = DB::transaction(function () use ($request) {
				$result = new UserData;
				$result->ud_run = $request->ud_run;
				$result->ud_first_name = $request->ud_first_name;
				$result->ud_second_name = $request->ud_second_name;
				$result->ud_father_surname = $request->ud_father_surname;
				$result->ud_mother_surname = $request->ud_mother_surname;

				$result->push();
				
				DB::commit();

				return $result;
			});
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}

		return $result;
	}

	/**
	 * Validate if RUN exists on the database
	 * @param  [integer]  $value
	 * @return boolean
	 */
	public static function isValid($value)
	{
		try {
			$result = static::where('ud_run', $value)->exists();
		} catch (Exception $e) {
			DB::rollback();
			return self::responseMessage(0, $e->getMessage());
		}

		return $result;
	}
}
