<?php

namespace Modules\Administration\Entities;

use DB;
use Hash;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'email',
    	'token',
    	'created_at'
    ];

    /**
	 * Table name
	 * @var string
	 */
	protected $table = 'password_resets';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;


    /**
     * insert record
     */
    
    public static function store($request)
    {
        try {
            $result = DB::transaction(function () use ($request) {
                $result = new PasswordReset;
                $result->email = $request->email;
                $result->token = Hash::make($request->token);
                $result->created_at = Carbon::now()->toDateTimeString();

                $result->push();

                DB::commit();

                return $result;
            });
        } catch (Exception $e) {
            DB::rollback();
            return self::responseMessage(0, $e->getMessage());
        }

        return $result;
    }
}
