<?php

namespace Modules\Administration\Entities;

use App\Traits\UtilsFromTraits;
use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
	use UtilsFromTraits;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'md_name',
		'md_path',
		'md_icon',
		'md_parent_id'
	];

	/**
	 * Database connection
	 * @var string
	 */
	#protected $connection = 'sgi';

	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'modules';

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'created_at', 'updated_at'
	];


	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;

	/**
	 * get children of parent
	 * @return object
	 */
	public function children(){
		return $this->hasMany( 'Modules\Administration\Entities\Modules', 'md_parent_id', 'id' );
	}

	/**
	 * get parent of children
	 * @return object
	 */
	public function parent(){
		return $this->hasOne( 'Modules\Administration\Entities\Modules', 'id', 'md_parent_id' );
	}

	/**
	 * Modules that own of user
	 * @return object
	 */
	public function access_control()
	{
		return $this->belongsToMany(
				'Modules\Administration\Entities\AccessControl', 
				'access_control_has_modules',
				'module_id',
				'access_control_id'
			);
	}


	/**
	 * get modules that own of user
	 * @param  [type] $user_id [description]
	 * @return object
	 */
	public static function getModules($user_id)
	{
		try {
			$parent_modules = Modules::
				whereHas('access_control', function ($query) use ($user_id) {
					$query->where([
						[
							'access_controls.id', '=', $user_id
						],[
							'access_control_has_modules.status', '=', true
						]
					]);
				})
				->get();
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return $parent_modules;
	}

	/**
	 * list module
	 * @return object
	 */
	public static function listModules()
	{
		try {
			$parent_modules = Modules::whereNotNull('md_parent_id')
				->get();
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return $parent_modules;
	}

	/**
	 * Check if the actual route is available to the user
	 * @return boolean [description]
	 */
	public static function isAuthorized($user_id, $path)
	{
		try {
			$parent_modules = Modules::
				where('md_path', '/'. $path)
				->whereHas('access_control', function ($query) use ($user_id) {
					$query->where([
						[
							'access_controls.id', '=', $user_id
						],[
							'access_control_has_modules.status', '=', true
						]
					]);
				})
				->exists();
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return $parent_modules;
	}

	public static function getParent($module_id)
	{
		try {
			$parent = Modules::find($module_id);
		} catch (Exception $e) {
			return self::responseMessage(0, $e->getMessage());
		}
		return $parent;
	}
}
