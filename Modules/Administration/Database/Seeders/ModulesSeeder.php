<?php

namespace Modules\Administration\Database\Seeders;

use Illuminate\Database\Seeder;

class ModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		\Modules\Administration\Entities\Modules::insert([
			[
				'md_name' => 'Menu', 
				'md_path' => '/',
				'md_icon' => '/',
				'md_parent_id' => null,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'Comercial', 
				'md_path' => '/comercial',
				'md_icon' => '/icono1',
				'md_parent_id' => 1,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'UniTv', 
				'md_path' => '/unitv',
				'md_icon' => 'fa fa-television',
				'md_parent_id' => 2,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'Home', 
				'md_path' => '/unitv',
				'md_icon' => 'fa fa-picture-o',
				'md_parent_id' => 3,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'Slider Informativo', 
				'md_path' => '/unitv/informative',
				'md_icon' => 'fa fa-picture-o',
				'md_parent_id' => 3,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'Videos', 
				'md_path' => '/unitv/video',
				'md_icon' => 'fa fa-video-camera',
				'md_parent_id' => 3,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'Trabajador del mes', 
				'md_path' => '/unitv/informative/employee',
				'md_icon' => 'fa fa-user',
				'md_parent_id' => 3,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'Producto del mes', 
				'md_path' => '/unitv/informative/product',
				'md_icon' => 'fa fa-shopping-cart',
				'md_parent_id' => 3,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'Supermercado', 
				'md_path' => '/unitv/supermarket',
				'md_icon' => 'fa fa-home',
				'md_parent_id' => 3,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'Informática', 
				'md_path' => '/informatica',
				'md_icon' => 'fa fa-question-circle',
				'md_parent_id' => 1,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'HelpDesk', 
				'md_path' => '/helpdesk',
				'md_icon' => 'fa fa-home',
				'md_parent_id' => 10,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'Tablero admin', 
				'md_path' => '/helpdesk',
				'md_icon' => 'fa fa-plus',
				'md_parent_id' => 11,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'Crear Ticket', 
				'md_path' => '/helpdesk/dashboard',
				'md_icon' => 'fa fa-plus',
				'md_parent_id' => 11,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'Problemas', 
				'md_path' => '/helpdesk/problem',
				'md_icon' => 'fa fa-question-circle',
				'md_parent_id' => 11,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			],[
				'md_name' => 'Tipo problemas', 
				'md_path' => '/helpdesk/type-problem',
				'md_icon' => 'fa fa-question-circle',
				'md_parent_id' => 11,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			]
		]);
    }
}
