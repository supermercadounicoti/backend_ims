<?php
namespace Modules\Administration\Database\Seeders;

use Illuminate\Database\Seeder;

class UserDataSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		\Modules\Administration\Entities\UserData::insert([
			[
				'ud_run' => 18215358,
				'ud_first_name' => 'Carlos',
				'ud_second_name' => 'Alberto',
				'ud_father_surname' => 'Saez',
				'ud_mother_surname' => 'Lara',
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			]
		]);
	}
}
