<?php

namespace Modules\Administration\Database\Seeders;

use Illuminate\Database\Seeder;

class AccessControlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		\Modules\Administration\Entities\AccessControl::insert([
			[
				'email' => 'cs@su.cl', 
				'password' => \Hash::make('123456'),
				'user_data_id' => 3,
				'rol_id' => 1,
				'created_at' => '2018-02-07 14:18:00',
				'updated_at' => '2018-02-07 14:18:00',
			]
		]);
    }
}
