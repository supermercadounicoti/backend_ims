<?php

namespace Modules\Administration\Database\Seeders;

use Illuminate\Database\Seeder;

class AccessControlHasModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		\Modules\Administration\Entities\AccessControlHasModules::insert([
			[
				'access_control_id' => 1,
				'module_id' => 1,
			],[
				'access_control_id' => 1,
				'module_id' => 2,
			],[
				'access_control_id' => 1,
				'module_id' => 3,
			],[
				'access_control_id' => 1,
				'module_id' => 4,
			],[
				'access_control_id' => 1,
				'module_id' => 5,
			],[
				'access_control_id' => 1,
				'module_id' => 6,
			],[
				'access_control_id' => 1,
				'module_id' => 7,
			],[
				'access_control_id' => 1,
				'module_id' => 8,
			],[
				'access_control_id' => 1,
				'module_id' => 9,
			],[
				'access_control_id' => 1,
				'module_id' => 10,
			],[
				'access_control_id' => 1,
				'module_id' => 11,
			],[
				'access_control_id' => 1,
				'module_id' => 12,
			],[
				'access_control_id' => 1,
				'module_id' => 13,
			],[
				'access_control_id' => 1,
				'module_id' => 14,
			],[
				'access_control_id' => 1,
				'module_id' => 15,
			]
		]);
    }
}
