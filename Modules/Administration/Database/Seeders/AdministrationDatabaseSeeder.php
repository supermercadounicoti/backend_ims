<?php

namespace Modules\Administration\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AdministrationDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserDataSeeder::class);
        $this->call(AccessControlSeeder::class);
        $this->call(ModulesSeeder::class);
        $this->call(AccessControlHasModulesSeeder::class);
        // $this->call("OthersTableSeeder");
    }
}
