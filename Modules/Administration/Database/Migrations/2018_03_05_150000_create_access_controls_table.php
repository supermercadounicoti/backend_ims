<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('access_controls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')
                ->unique();
            $table->string('password');
            $table->integer('user_data_id')
                ->unsigned();
            $table->integer('rol_id')
                ->unsigned();
            $table->boolean('status')->default(1);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('user_data_id')
                ->references('id')->on('users_data');
            $table->foreign('rol_id')
                ->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_controls');
    }
}
