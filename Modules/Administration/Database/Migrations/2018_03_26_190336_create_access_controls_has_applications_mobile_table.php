<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessControlsHasApplicationsMobileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_controls_has_app_mobile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('access_control_id')
                ->unsigned();
            $table->integer('application_mobile_id')
                ->unsigned();
            $table->boolean('status');

            $table->foreign('access_control_id')
                ->references('id')->on('access_controls');
            $table->foreign('application_mobile_id')
                ->references('id')->on('applications_mobile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_controls_has_app_mobile');
    }
}
