<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessControlHasModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_control_has_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('access_control_id')->unsigned();
            $table->integer('module_id')->unsigned();
            $table->boolean('status')->default(1);

            $table->foreign('access_control_id')->references('id')->on('access_controls');
            $table->foreign('module_id')->references('id')->on('modules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_control_has_modules');
    }
}
