<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //$this->call(UserDataSeeder::class);
        //$this->call(AccessControlSeeder::class);
        $this->call(ModulesSeeder::class);
        //$this->call(AccessControlHasModulesSeeder::class);
    }
}
